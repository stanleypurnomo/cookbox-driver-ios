import React, { Component, useState } from 'react';
import { View, Text, StyleSheet, AsyncStorage, ActivityIndicator, RefreshControl, Switch } from 'react-native';
import { ScrollView, TouchableHighlight, TouchableOpacity } from 'react-native-gesture-handler';
import { withNavigation, SafeAreaView, NavigationActions, StackActions } from 'react-navigation';
import ButtonApproveDriver from '../approve/ButtonApproveDriver';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import TestingEachOrderList from './testing/TestingEachOrderList';
import SwitchOnOff from '../screens/SwitchOnOff';
import ScrollViewOrderActive from '../screens/ScrollViewOrderActive';
// const {driverID} = this.props;

class ActiveOrderListDriver extends React.Component{
    constructor(props) {
        super(props);
        // const [isEnabled, setIsEnabled] = useState(false);
        // const toggleSwitch = () => setIsEnabled(previousState => !previousState);
        //True to show the loader
        this.state = { 
            // loaded: null,
            id:props.driverID,
            data: [],
            refreshing: false, 
            // refreshingButton: false
        };
        this.url = 'http://seafoodau.herokuapp.com/api/v1/order/list/'+this.props.driverID
        
        // alert(this.url);// console.log(this.driverID+' abcdef');
        this.componentWillMount = this.componentWillMount.bind(this);
    }

    componentWillMount(){
        // const { navigation } = this.props;
        // this.focusListener = navigation.addListener('didFocus', () => {
        //     this.setState({ data: [] });
        //     var This = this;
        //     var reader = new FileReader();
        //     var url = this.url;
        //     var req = new XMLHttpRequest();
        //     req.responseType = 'json';
        //     req.open('GET', url, true);
        //     // req.onload(this.setState({loading:true}));
        //     req.onload  = function(This) {
        //         var jsonResponse = JSON.stringify(req.response);
        //         var jsonData = JSON.parse(jsonResponse);
                
        //         // console.log(jsonData[1]["body"]);
        //         // AsyncStorage.setItem('testjson',jsonResponse);
        //     // do something with jsonResponse
        //         data = jsonData;
        //         // console.log(data);
        //         AsyncStorage.setItem('testjson',JSON.stringify(jsonData));
        //     };
        //     // jsonData = this.jsonData;
        //     req.send(null);
        //     this.fetchJSON();
        //     // this.updateData();
        // });
        // alert(this.props.driverID);
        var This = this;
        var reader = new FileReader();
        var url = 'http://seafoodau.herokuapp.com/api/v1/order/list/'+this.props.driverID;
        var req = new XMLHttpRequest();
        req.responseType = 'json';
        req.open('GET', url, true);
        // req.onload(this.setState({loading:true}));
        req.onreadystatechange  = function(This) {
            var jsonResponse = JSON.stringify(req.response);
            var jsonData = JSON.parse(jsonResponse);
            
            // console.log(jsonData[1]["body"]);
            // AsyncStorage.setItem('testjson',jsonResponse);
        // do something with jsonResponse
            data = jsonData;
            // console.log(data);
            AsyncStorage.setItem('testjson',JSON.stringify(jsonData));
        };
        // jsonData = this.jsonData;
        req.send(null);
        // this.fetchJSON();
        this.onRefresh();
        this.onRefreshAgain();
        // alert(this.props.getStatusOTW);
        var thisVar = this;
        setTimeout(function(){thisVar.onRefresh();},1000);
        // var idDriver = this.getDriverID;
        // console.log(idDriver);
        // console.log(this.state.id+'  hello')
    }

    // componentWillUnmount() {
    //     // Remove the event listener before removing the screen from the stack
    //     this.focusListener.remove();
    // }

    ListViewItemSeparator = () => {
        return (
          //returning the listview item saparator view
          <View
            style={{
              height: 0.2,
              width: '100%',
              backgroundColor: '#808080',
            }}
          />
        );
      };

    fetchJSON = async () => {
        try {
            const json = await AsyncStorage.getItem('testjson');
            const jsonVal = JSON.parse(json);
            this.setState({
                data: jsonVal
            });
            console.log(this.state.data);
            console.log('true');
        } catch (error) {
            console.log(error);
        }
    }

    updateData(){
        var url = 'http://seafoodau.herokuapp.com/api/v1/order/list/'+this.props.driverID;
        var req = new XMLHttpRequest();
        req.responseType = 'json';
        req.open('GET', url, true);
        req.onreadystatechange  = function(This) {
            var jsonResponse = JSON.stringify(req.response);
            var jsonData = JSON.parse(jsonResponse);
            
            // console.log(jsonData[1]["body"]);
            // AsyncStorage.setItem('testjson',jsonResponse);
        // do something with jsonResponse
            data = jsonData;
            // console.log(data);
            AsyncStorage.setItem('testjson',JSON.stringify(jsonData));
        };
        // jsonData = this.jsonData;
        this.fetchJSON();
        req.send(null);
    }

    onCancel(id){
        var req = new XMLHttpRequest();
        var url = 'http://seafoodau.herokuapp.com/api/v1/order/out_of_order/'+id;
        req.responseType = 'json';
        req.open('GET', url, true);
        var ThisState = this;
        this.setState({
            refreshing: true,
        });
        // req.onload(this.setState({loading:true}));
        req.onload  = function(This) {
            console.log('refreshing '+ThisState.state.refreshing);
            var jsonResponse = req.response;
            console.log(jsonResponse.status);
            ThisState.setState({
                data:[]
            });
            if(jsonResponse.status === 'success'){
                setTimeout(function(){ThisState.setState({refreshing: false,})},1500);
                console.log(ThisState.state.refreshing+' result');
                console.log('refreshing success'+ThisState.state.refreshing);
                ThisState.onRefresh();
            }
            setTimeout(function(){ThisState.onDoneAgain();},500);
        };
        this.props.navigation.reset({
            index: 0,
            routes: [
                {   name: 'Order Out of Stock',
                    params:{id: id},
                }
            ]
        });
        req.send(null)
    }

    onChange(id){
        var req = new XMLHttpRequest();
        var url = 'http://seafoodau.herokuapp.com/api/v1/order/sending/'+id;
        req.responseType = 'json';
        req.open('GET', url, true);
        var ThisState = this;
        this.setState({
            refreshing: true,
        });
        // req.onload(this.setState({loading:true}));
        req.onload  = function(This) {
            console.log('refreshing '+ThisState.state.refreshing);
            var jsonResponse = req.response;
            console.log(jsonResponse.status);
            ThisState.setState({
                data:[]
            });
            // console.log(JSON.stringify(jsonResponse));
            if(jsonResponse.status === 'success'){
                setTimeout(function(){ThisState.setState({refreshing: false,})},1500);
                console.log(ThisState.state.refreshing+' result');
                console.log('refreshing success'+ThisState.state.refreshing);
                ThisState.onRefresh();
            }
            // ThisState.setState({refreshing: false,});
            setTimeout(function(){ThisState.onDoneAgain();},500);
            // var jsonData = JSON.parse(jsonResponse);
            
            // console.log(jsonData[1]["body"]);
            // AsyncStorage.setItem('testjson',jsonResponse);
        // do something with jsonResponse
            // data = jsonData;
            // console.log(data);
            // AsyncStorage.setItem('testjson',JSON.stringify(jsonData));
        };
        req.send(null)
        // jsonData = this.jsonData;
    }

    onRefresh() {
        //Clear old data of the list
        this.setState({ data: [], refreshing: true });
        //Call the Service to get the latest data
        // this.updateData();
        var ThisState = this;
        setTimeout(function(){ThisState.updateData();},5000);
        setTimeout(function(){ThisState.setState({ refreshing:false });},5000);
        this.onRefreshAgain();
        // setTimeout(function(){alert('Refresh Done');},5000);
    }

    onRefreshAgain() {
        this.setState({ data: [], refreshing: true });
        this.updateData();
    }

    onDoneAgain(){
        this.onRefresh();
        // break;
    }

    onDone(id){
        var req = new XMLHttpRequest();
        var url = 'http://seafoodau.herokuapp.com/api/v1/order/completed/'+id;
        req.responseType = 'json';
        req.open('GET', url, true);
        var ThisState = this;
        this.setState({
            refreshing: true,
        });
        // req.onload(this.setState({loading:true}));
        req.onload  = function(This) {
            console.log('refreshing '+ThisState.state.refreshing);
            var jsonResponse = req.response;
            console.log(jsonResponse.status);
            ThisState.setState({
                data:[]
            });
            // console.log(JSON.stringify(jsonResponse));
            if(jsonResponse.status === 'success'){
                setTimeout(function(){ThisState.setState({refreshing: false,})},1500);
                console.log(ThisState.state.refreshing+' result');
                console.log('refreshing success'+ThisState.state.refreshing);
                ThisState.onRefresh();
            }
            // ThisState.setState({refreshing: false,});
            setTimeout(function(){ThisState.onDoneAgain();},500);
            // var jsonData = JSON.parse(jsonResponse);
            
            // console.log(jsonData[1]["body"]);
            // AsyncStorage.setItem('testjson',jsonResponse);
        // do something with jsonResponse
            // data = jsonData;
            // console.log(data);
            // AsyncStorage.setItem('testjson',JSON.stringify(jsonData));
        };
        req.send(null)
        // jsonData = this.jsonData;
    }

    async getStatusOrder(id){
    //    const getStatus = await AsyncStorage.getItem('statusOrderID-'+id);
       const getStatus = await AsyncStorage.getItem('statusOrderID-'+id);
       this.setState({ status:getStatus })
       return <Text></Text>;
    //     if(getStatus === 'done'){
    //     //    console.log(getStatus+' STatus');
    //           return 'false';
    //     } else {
    //         // console.log(getStatus+' Statius');
    //        return 'true';
    //    }
    }

    // renderOrderList(){
    //     return(
    //     <View style={{ marginTop:10 }}>
    //         {/* <Text>{this.state.data.order}</Text> */}
    //         {
    //             this.state.data.map((item, key) =>
    //             (
    //                 <View style={{ marginBottom:10 }}>
    //                 <Text>{item.order[0]["id_transaction"]}</Text>
    //                 <Text>{item.order[0]["total_price"]}</Text>
    //                 <Text>{item.order[0]["recipient_address"]}</Text>
    //                 <Text>{item.order[0]["status"]}</Text>
    //                 <Text>{item.order[0]["fullname"]}</Text>
    //                 {
    //                     item.order_items.map((item2, key) =>(
    //                     <View style={{ flexDirection:'row', marginBottom:10 }}>
    //                         <Text style={{ flex:0.25 }}>{item2[0]["qty"]}</Text>
    //                         <Text style={{ flex:0.5 }}>{item2[0]["name"]}</Text>
    //                     </View>
    //                     ))
    //                 }
    //                 </View>
    //             ))
    //         }
    //         {/* <Text>{this.state.data["body"]}</Text> */}
    //                 {/* <FlatList   style={{ }}
    //                     data={this.state.data}
    //                     keyExtractor={(item, index) => index.toString()}
    //                     // ItemSeparatorComponent={this.ListViewItemSeparator}
    //                     renderItem={({item}) => (
    //                     <View style={{ borderWidth:1, borderColor:'#ccc', marginBottom:10 }}>
    //                         <Text
    //                         style={{}}
    //                         onPress={() => alert(item.order.id_transaction)}>
    //                             {item[0].order.fullname}
    //                         </Text>
    //                     </View>
    //                     )}
    //                     refreshControl={
    //                         <RefreshControl
    //                           //refresh control used for the Pull to Refresh
    //                           refreshing={this.state.refreshing}
    //                           onRefresh={this.onRefresh.bind(this)}
    //                         />
    //                     }
    //                 > */}
    //             {/* <Text>
    //                 {testJson[0]}
    //             </Text> */}
    //         {/* </FlatList> */}
    //     </View>
    //     )
    // }


    // getStatus(key){
    //     return
    // }
    // getKey(key)

    renderOrderList(){
        if(this.state.refreshing === false && this.state.data!=null){
            return(
                <View style={{ marginBottom:15 }}>
                {   
                    this.state.data.map(( item, key ) =>
                    (   
                        
                        // this.getStatusOrder(item.order[0]["id_transaction"]) === 'true'
                    // item.status == 'true'
                    // ?
                    <View key={key} ref={key} style={{ height:320, marginBottom:60 }}>
                        {/* <Text>{Object.keys(this.state.count).length}</Text> */}
                        {/* {this.getStatusOrder(item.order[0]["id_transaction"])} */}
                        <View style={{ marginBottom:15, borderColor:'#ccc', borderWidth:0.5, borderRadius:5, padding:15,
                                        backgroundColor:'white', shadowColor: "#000",
                                        shadowOffset: {width: 0,height: 2,},
                                        shadowOpacity: 0.25,shadowRadius: 2.25,
                                        elevation: 5, }}>
                            <View style={{ borderRadius:5, marginLeft:15, marginRight:15, position:'absolute', zIndex:1, top:15, left:10 }}>
                                <TouchableOpacity 
                                    onPress={() => alert('Pressed Track')} 
                                    style={{ zIndex:1, backgroundColor:'purple', borderRadius:5, borderTopRightRadius:5, width:'100%', marginTop:10, paddingLeft:15, paddingRight:15, paddingTop:5, paddingBottom:5 }}>
                                    <Text style={{ fontFamily:'Asap-Bold', color:'white' }}>Track</Text>
                                </TouchableOpacity>
                            </View>
                            <TouchableOpacity onPress={() => alert('Pressed Maps')} style={{ width:'100%', height:150, marginBottom:5 }}>
                                <View style={{position: 'absolute', top: 0, right: 0, bottom: 0, left: 0}} />
                                <MapView
                                    style={styles.map }
                                    provider={PROVIDER_GOOGLE}
                                    scrollEnabled={false}
                                    initialRegion={{
                                    latitude: -27.3810232,
                                    longitude: 152.4326816,
                                    latitudeDelta: 1,
                                    longitudeDelta: 1,
                                    }}
                                />
                            </TouchableOpacity>
                            <Text style={[styles.fontFamilyRegular, { fontSize:14, color:'red', marginBottom:5 }]}>{item.order[0]["fullname"]}</Text>
                            <Text style={[styles.fontFamilyRegular, { fontSize:14, color:'red', marginBottom:20 }]}>+61-123-456-789</Text>
                            <View style={{ marginBottom:40, height:25 }}>
                                {
                                    item.order_items.map((item2, key) =>(
                                    <View style={{ flexDirection:'row', marginBottom:10 }}>
                                        <Text style={[styles.fontFamilyRegular, { fontSize:15, flex:0.1 }]}>{item2[0]["qty"]}</Text>
                                        <Text style={[styles.fontFamilyRegular, { fontSize:15, flex:0.5 }]}>{item2[0]["name"]}</Text>
                                    </View>
                                    ))
                                }
                                {/* <Text style={[styles.fontFamilyRegular, { fontSize:15 }]}>1x SeaBox Prawn</Text> */}
                                {/* <Text style={[styles.fontFamilyRegular, { fontSize:15 }]}>1x Dimsim Seafood</Text> */}
                            </View>
                            <View style={{ flexDirection:'row' }}>
                                <Text style={[styles.fontFamilyRegular, { color:'blue', fontWeight:'700' }]}>Destination:</Text>
                                <Text style={[styles.fw400, styles.fontFamilyRegular, {paddingLeft:5, paddingRight:5}]}>{item.order[0]["recipient_address"]}</Text>
                                {/* <Text>{key+1}</Text> */}
                            </View>
                            <View>
                                {
                                    this.state.refresh
                                }
                                <ButtonApproveDriver navigation={this.props.navigation} orderId={item.order[0]["id_transaction"]} statusOrder={item.order[0]["status"]}  onDone={this.onDone.bind(this, item.order[0]["id_transaction"])} onCancel={this.onCancel.bind(this, item.order[0]["id_transaction"])} onRefresh={this.onChange.bind(this, item.order[0]["id_transaction"])}
                                // whileDone={this.deleteOrder.bind(this, item.order[0]["id_transaction"])}
                                />
                            </View>
                        </View>
                    </View>
                    // :
                    // null
                    ))
                }
            </View>
            )
        } else if(this.state.refreshing === true) {
            return(
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <View style={{ backgroundColor:'white', borderWidth:1, borderColor:'#ccc', 
                        alignSelf:'center', padding:20, shadowColor: "#000",
                        shadowOffset: {width: 0,height: 2,},
                        shadowOpacity: 0.25,shadowRadius: 3.84,elevation: 5, top:'100%'}}>
                        <ActivityIndicator size="large" color="black" />
                    </View>
                </View>
            );
        } else if(this.state.data === null){
            return(
                // <View>
                //     <TestingEachOrderList navigation={this.props.navigation}/>
                //     <TestingEachOrderList navigation={this.props.navigation}/>
                //     <TestingEachOrderList navigation={this.props.navigation}/>
                //     <TestingEachOrderList navigation={this.props.navigation}/>
                // </View>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={[styles.fontFamilyRegular, { top:'750%', fontSize:15 }]}>Your order will be shown here</Text>
                </View>
            );
        } else {
            
        }
    }
    render(){
        const { getStatusOTW } = this.props;
        return(
            <View style={{ backgroundColor:'white', flexDirection:'column', flex:1, paddingTop:0}}>
                <ScrollView style={{ flex:0.1 }}
                            refreshControl={ <RefreshControl onRefresh={this.props.checkStatus}/> }>
                    <View style={{ flexDirection:'row', flex:0.05, marginBottom:5 }}>
                        <View style={{ flex:0.175 }}>
                            <Text style={[styles.fontFamilyRegular, { fontSize:17, lineHeight:40 }]}>Status:</Text>
                        </View>
                        <View style={{ flex:0.35, alignSelf:'center' }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Testing')} style={{ paddingBottom:2.5, paddingTop:2.5, backgroundColor:'#4FCA55', 
                                            alignItems:'center', borderRadius:5, alignSelf:'flex-start', paddingLeft:15, paddingRight:15 }}>
                                <Text style={[styles.fontFamilyRegular, { fontSize:16, color:'white', fontWeight:'600' }]}>Active</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ flex:0.5, alignItems:'center', justifyContent:'flex-start', flexDirection:'row' }}>
                            <View style={{ flex:0.9 }}>
                                <Text style={[styles.fontFamilyRegular, { lineHeight:15 }]}>Switch to Off</Text>
                            </View>
                            <View style={{ flex:0.1, marginBottom:10, alignItems:'flex-end' }}>
                                <SwitchOnOff getStatus={this.props.statusDriver} onSwitch={this.props.onSwitch}/>
                            </View>
                        </View>
                    </View>
                    <View style={{ flexDirection:'row', flex:0.05, marginBottom:5 }}>
                        <View style={{ flex:0.175 }}></View>
                        <View style={{ flex:0.375 }}>
                            {
                                getStatusOTW === 'on restaurant'
                                ?
                                <View style={{ paddingBottom:2.5, paddingTop:2.5, backgroundColor:'#009ce5', 
                                                alignItems:'center', borderRadius:5 }}>
                                    <Text style={[styles.fontFamilyRegular, { fontSize:16, color:'white', fontWeight:'600' }]}>On Restaurant</Text>
                                </View>
                                :
                                null
                            }
                        </View>
                    </View>
                </ScrollView>
                <View style={{ flex:5 }}>
                    <ScrollViewOrderActive idDriver={this.props.driverID} navigation={this.props.navigation}/>
                </View>
                {/* <View style={{ flexDirection:'row', flex:0.05, marginTop:10 }}>
                    <View style={{ flex:0.5}}>
                        {
                            this.state.data != null
                            ?
                            <Text style={styles.fontFamilyRegular}>You have { Object.keys(this.state.data).length ? <Text style={{ color:'red' }}>{Object.keys(this.state.data).length}</Text> : <Text style={{ color:'red' }}>0</Text> } order</Text>
                            :
                            <Text style={styles.fontFamilyRegular}>You have <Text style={{ color:'red' }}>no</Text> order</Text>
                        }
                    </View>
                </View> */}
                {/* <View>
                    <View style={{ flex:0.5 }}>
                        
                    </View>
                </View> */}
                {/* {
                    this.state.countOrders > 0
                    ? */}
                    {/* <View style={{ flex:0.7, marginLeft:-5, marginRight:-5 }}> */}
                        {/* <ScrollView style={{ width:'100%', marginBottom:30, height:'75%'}} 
                                refreshControl={ <RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefresh.bind(this)}/> }
                        > */}
                            {/* <Text>{this.state.data.order[0]["id_transaction"]}</Text> */}
                            {/* {this.renderOrderList()}
                        </ScrollView> */}
                    {/* </View> */}
                    {/* :
                    null
                } */}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    fontFamilyRegular:{
        fontFamily:'Asap-Regular'
    },
    fw400:{
        fontWeight:'400'
    },
    map: {
        ...StyleSheet.absoluteFillObject,
        height:150,
        zIndex:-1,
    },
});

export default withNavigation(ActiveOrderListDriver);