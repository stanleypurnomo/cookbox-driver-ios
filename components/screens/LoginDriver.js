import React, { Component } from 'react';
import { View, Image, Text, Dimensions, StyleSheet, Linking, StatusBar, 
        KeyboardAvoidingView, Keyboard, TouchableWithoutFeedback, 
        AsyncStorage, ActivityIndicator, Alert } from 'react-native';
import { withNavigation, SafeAreaView, NavigationActions } from 'react-navigation';
import { TouchableOpacity, ScrollView, TextInput } from 'react-native-gesture-handler';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { CommonActions } from '@react-navigation/native';
import { KeyboardAccessoryView, KeyboardAccessoryNavigation } from 'react-native-keyboard-accessory';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import DoneButton from 'react-native-keyboard-done-button';
import { asin } from 'react-native-reanimated';
import DropdownAlert from 'react-native-dropdownalert';

var statusBarHeight = getStatusBarHeight();
var fullName = '';
const DismissKeyboard = ({ children }) => (
    <TouchableWithoutFeedback 
    onPress={() => Keyboard.dismiss()}>
        {children}
    </TouchableWithoutFeedback>
);
class LoginDriver extends Component{
    constructor(){
        super();
        this.state = {
            username:'',
            password:'',
            isLogin: false,
            isLoading: false,
            token:'',
        };
        this.login = this.login.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
        // this.state.isLogin = this.state.isLogin.bind(this);
    }       
      componentDidMount(){
        if(this.state.isLogin === true){
            this.props.navigation.navigate('Home');
        }
      }

      fetchAuthData = async () =>{
        try {
            const tokenVal = await AsyncStorage.getItem('token');
            console.log(tokenVal);
            this.setState({
                token:tokenVal
            });
        } catch (error) {
            console.log(error);
        }
        var request = {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Authorization': this.state.token
            },
        };
        fetch('http://seafoodau.herokuapp.com/api/v1/auth/data', request)
        .then((response) => response.json())
        .then((responseJson)=>
        {
            console.log(responseJson);
            if(responseJson.status == '200'){
                console.log(responseJson.data.fullname);
                AsyncStorage.setItem('username',responseJson.data.username);
                fullName = responseJson.data.fullname;
                AsyncStorage.setItem('fullname',responseJson.data.fullname);
                AsyncStorage.setItem('id',responseJson.data.id);
                // Alert.alert(
                //     "Welcome, "+fullName,
                //     "Have a nice day",
                //     [
                //         { text: "OK" }
                //     ],
                // { cancelable: true });
                // const navigateAction = this.props.navigation.navigate({
                //     routeName: 'Home',
                //     params: {id: 2}
                //   });
                // this.props.navigation.dispatch(navigateAction);
                this.props.navigation.reset({
                    index: 0,
                    routes: [
                    { name: 'Home',
                        params:{ id: 2 }
                    },
                    // {
                    //   name: 'Profile',
                    //   params: { user: 'jane' },
                    // },
                    ],
                });
                AsyncStorage.setItem('isLogin','true');
                AsyncStorage.setItem('welcomeMessage','true');
            } else {
                console.log('Error getting data');
                // alert("Wrong Username or Password");
                // this.setState({
                //     isLoading: false,
                // });
            }
        })
        .catch((error)=>{
            console.error(error);
        });
      }

      IsJsonString(str) {
        try {
            str;
        } catch (e) {
            return false;
        }
        return true;
    }

    // function setHeaders(headers){
    //     for(let key in headers){
    //       xhr.setRequestHeader(key, header[key]) 
    //     }
    //   }
    //   setHeaders({"Host":"api.domain.com","X-Requested-With":"XMLHttpRequest","contentType":"application/json"})

      login = () =>{
        const {username, password} = this.state;
        // alert(username+','+password);
        // this.props.navigation.navigate('Home');
        //  API Login Function
        var params = {
            username: username,
            password: password,
        };
        
        var formData = new FormData();
        
        for (var k in params) {
            formData.append(k, params[k]);
        }

        console.log(formData);

        
        var request = {
            method: 'POST',
            headers: {
                'Content-type': 'application/x-www-form-urlencoded',
            },
            body: formData
        };

        this.setState({
            isLoading: true,
        });
        
        fetch('http://seafoodau.herokuapp.com/api/v1/auth/login', request)
        .then((response) => response.json())
        .then((responseJson)=>
        {
            console.log(responseJson);
            if(responseJson.status == '200'){
                AsyncStorage.setItem('token',responseJson.token);
                this.fetchAuthData();
                // console.log(fullName);
            } else {
                // alert("Wrong Username or Password");
                this.dropDownAlertRef.alertWithType('error', "Wrong Username or Password");
                this.setState({
                    isLoading: false,
                });
            }
        })
        .catch((error)=>{
            console.log(error);
            Alert.alert(
                "Login Error",
                "Something went wrong",
                [
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ],
                { cancelable: false }
            );
            this.setState({
                isLoading: false,
            });
        });

        // fetch('http://seafoodau.herokuapp.com/api/v1/auth/login',{
        //     method:'post',
        //     header:{
        //         'Accept': '*/*',
        //         'Content-type': 'application/json',
        //         'Connection': 'keep-alive',
        //         'User-Agent': 'PostmanRuntime/7.25.0'

        //     },
        //     body:formData,
        // })
        // .then((response) => response.json())
        //     .then((responseJson)=>{
        //         console.log(responseJson.msg);
        //         if(responseJson == "ok"){
        //             alert("Login successful");
        //             this.props.navigation.dispatch(
        //                 CommonActions.reset({
        //                   index: 0,
        //                   routes: [
        //                     { name: 'Home' },
        //                     // {
        //                     //   name: 'Profile',
        //                     //   params: { user: 'jane' },
        //                     // },
        //                   ],
        //                 })
        //             );
        //             AsyncStorage.setItem('isLogin','true');
        //         } else {
        //             alert("Wrong Username or Password");
        //         }
        //     })
        //     .catch((error)=>{
        //         console.error(error);
        //     });
        // Keyboard.dismiss();


    //   this.state.isLogin = true;
      // if(this.state.isLogin===true){
      //     this.setState({
      //       isLogin: false,
      //     });
      //     console.log(this.state.isLogin);
      // }
    //   if(this.state.isLogin === true){
    //       this.props.navigation.navigate('Home');
    //     }
      }
      render(){
        return(
        <DismissKeyboard>
        <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : null}>
            {
                this.state.isLoading === true
                ?
                <View style={{ position:'absolute', top:'50%', zIndex:999, alignSelf:'center' }}>
                    <View style={{ backgroundColor:'white', borderWidth:1, borderColor:'#ccc', 
                    alignSelf:'center', padding:20, shadowColor: "#000",
                    shadowOffset: {width: 0,height: 2,},
                    shadowOpacity: 0.25,shadowRadius: 3.84,elevation: 5, }}>
                        <ActivityIndicator size="large" color="black" />
                    </View>
                    {/* <Text>Is Loading: true</Text> */}
                    {/* show loading spinner */}
                </View>
                :
                null
            }
          <View style={{ height:'100%', backgroundColor:'white' }}>
            {/* <StatusBar barStyle="dark-content" backgroundColor="black"/> */}
            <View style={{ flexDirection:'column', flex:1.5}}>
                <View style={{ flex:0.5 }}>
                    <Image  source={require('../../components/images/logo-banner/background1new.png')}
                    style={{ height:640, width:'100%', zIndex:1, resizeMode:'cover' }}/>
                </View>
                <View style={{ alignSelf:'center', position:'absolute', zIndex:9, top:statusBarHeight+20 }}>
                {/* <Text>AAA</Text> */}
                    <View style={{ alignSelf:'center' }}>
                        <Image source={require('../images/logo-banner/Transparent_background_white.png')} 
                            style={{ width:200, height:150, resizeMode:'contain', marginBottom:25 }}
                        />
                    </View>
                {/* <Image source={require('../../components/images/logo-banner/White.png')}
                        style={{ zIndex:9, height:175, width:175, resizeMode:'contain' }}/> */}
                </View>
            </View>
            <View style={{ top:'-12.5%' }}>
                <DropdownAlert ref={ref => this.dropDownAlertRef = ref} errorColor="#f54f29"/>
            </View>
            <View style={{ flexDirection:'column', alignItems:'center', flex:1, backgroundColor:'white', paddingTop:15, paddingBottom:15 }}>
                <View style={{ flexDirection:'row', marginBottom:20, paddingLeft:5, paddingRight:5 }}>
                    <View style={{ flex:0.3, alignItems:'center' }}>
                        <Text style={{ lineHeight:36, fontFamily:'Asap-Regular', fontSize:18 }}>Username</Text>
                    </View>
                    <View style={{ paddingLeft:15, paddingRight:15, flex:0.55, alignItems:'flex-start' }}>
                            {/* <DoneButton
                            title="Done!"   //not required, default value = `Done`
                            style={{ backgroundColor: 'red' }}  //not required
                            doneStyle={{ color: 'green' }}  //not required
                            /> */}
                            <TextInput style={{ fontFamily:'Asap-Regular', padding:7.5, borderColor:'#FF2D00', borderRadius:5, width:'100%', borderWidth:1, height:36 }}
                            returnKeyType={ 'next' } autoCapitalize='none' onSubmitEditing={Keyboard.dismiss} autoCorrect={false} keyboardAppearance="light" onChangeText={username => this.setState({username})}/>
                    </View>
                </View>
                <View style={{ flexDirection:'row', marginBottom:20, paddingLeft:5, paddingRight:5 }}>
                    <View style={{ flex:0.3, alignItems:'center' }}>
                        <Text style={{ lineHeight:36, fontFamily:'Asap-Regular', fontSize:18 }}>Password</Text>
                    </View>
                    <View style={{ paddingLeft:15, paddingRight:15, flex:0.55, alignItems:'flex-start' }}>
                        {/* <KeyboardAccessoryView style={{ width:'100%' }}> */}
                        <TextInput style={{ fontFamily:'Asap-Regular', padding:7.5, borderColor:'#FF2D00', borderRadius:5, width:'100%', borderWidth:1, height:36 }}
                        secureTextEntry={true} autoCapitalize='none' keyboardAppearance="light" returnKeyType={ 'done' } onSubmitEditing={Keyboard.dismiss} onChangeText={password => this.setState({password})}/>
                        {/* <KeyboardAccessoryNavigation/> */}
                    </View>
                </View> 
                <TouchableOpacity onPress={this.login} style={{ borderRadius:5, width:200, backgroundColor:'#f54f29', padding:10, alignItems:'center' }}>
                    <Text style={{ fontFamily:'Asap-Regular', color:'white',fontSize:18, fontWeight:'600' }}>Go</Text>
                </TouchableOpacity>
            </View>    
            {/* <Button title="Click here Log out" onPress={this.logout}></Button>
            <Text>{String(this.state.isLogin)}</Text> */}
          </View>
        </KeyboardAvoidingView>
        </DismissKeyboard>
        )
    }
}

export default withNavigation(LoginDriver);