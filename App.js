import * as React from 'react';
import { Button, View, Text, TextInput, Image, StyleSheet, SafeAreaView, TouchableOpacity, StatusBar, Dimensions, AsyncStorage, ActivityIndicator, KeyboardAvoidingView, Keyboard } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { ScrollView, TouchableWithoutFeedback } from 'react-native-gesture-handler';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Header } from 'react-native-elements';
import Footer from './components/screens/Footer';
// import TestPushNotif from './components/notifications/TestPushNotif';
import ReviewOrder from './components/screens/ReviewOrder';
import ConfirmOrdersonDriver from './components/screens/ConfirmOrdersonDriver';
import DriverCallout from './components/floatingbuttons/Driver';
import WebViewComponents from './components/screens/WebViewComponents';
import HomeAppDriver from './components/screens/HomeAppDriver';
import AcceptOrder from './components/screens/AcceptOrderDriver';
import OnRestaurant from './components/screens/OnRestaurantDriver';
import OrderInProgress from './components/screens/OrderInProgressDriver';
import TrackingMapsDriver from './components/screens/TrackingMapsDriver';
import ProfileDriver from './components/screens/ProfileDriver';
import AchievementDriver from './components/screens/AchievementDriver';
import IsLogin from './components/screens/IsLogin';
import LoginDriver from './components/screens/LoginDriver';
import TrackOrderDriver from './components/screens/TrackOrderDriver';
import HomeDriverInactive from './components/screens/HomeDriverInactive';
import WelcomeDriver from './components/screens/WelcomeDriver';
import ChatDriver from './components/screens/ChatDriver';
import TestingRefresh from './components/screens/testing/TestingRefresh';
import TestResponseJSON from './components/screens/testing/TestResponseJSON';
import ForgotPasswordDriver from './components/screens/ForgotPasswordDriver';
import OutOfStock from './components/screens/OutOfStock';
import OutOfStockNew from './components/screens/OutOfStockNew';
import TextareaAutosize from 'react-autosize-textarea';
//Screens Components

import PrivacyPolicyScreens from './components/screens/PrivacyPolicy';
import DiscoverRestaurants from './components/screens/DiscoverRestaurants';
import OrderDetails from './components/screens/OrderDetails';
import Callout from 'react-native-maps';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faArrowLeft, faTimesCircle, aChevronUp, faChevronDown, faChevronCircleLeft, faChevronLeft, faChevronCircleRight, faTrash, faTrashAlt, faHome, faUserAlt, faPiggyBank } from '@fortawesome/free-solid-svg-icons';
import OrderInProgressDriver from './components/screens/OrderInProgressDriver';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import Textarea from 'react-native-textarea';
import { SafeAreaProvider } from 'react-native-safe-area-context';
// import Ionicons from '@expo/vector-icons/Ionicons';
// import { HeaderLoginRegister } from './components/headers/LoginRegister';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
var statusBarHeight = getStatusBarHeight();
console.log(windowWidth);

function HomeScreen({ navigation }) {
  return (
    <SafeAreaView>
      <StatusBar barStyle="dark-content" />
      <Header
        placement="right"
        // leftComponent={<TouchableOpacity onPress={() => navigation.openDrawer()}><Image source={require('./components/images/logo-banner/baseline_menu_black_18dp.png')}/></TouchableOpacity>}
        leftComponent={<View style={{flexDirection:'column', top:-25, zIndex:99}}
        onPress={() => navigation.navigate('Login')}><TouchableOpacity onPress={() => navigation.navigate('Login')} style={{ zIndex:99 }}><Text style={windowWidth === 320 ? styles.fontSm : styles.fontMed}>Select Location</Text><Text style={{ fontSize:15, alignSelf:'flex-start' }}>Brisbane</Text></TouchableOpacity></View>}
        centerComponent={
        <View style={{ alignSelf:'center', flexDirection:'column', flex:1 }}>
          <View style={{ flexDirection:'row', flex:1, alignSelf:'center' }}>
            {/* <TouchableOpacity onPress={PushNotificationIOS.requestPermissions()}> */}
              <Image style={{ width:120, alignSelf:'center' }} source={require('./components/images/logo-banner/cookbox-logo.png')}/>
            {/* </TouchableOpacity> */}
          </View>
          <View style={{ flexDirection:'row', flex:1, alignSelf:'center' }}>
            <View style={{ width:'100%', flex:1, left:0}}>
              <TouchableOpacity 
              onPress={() =>  navigation.navigate('Search Restaurant')}
              style={{}}><View style={windowWidth === 320 ? styles.searchBarHeaderSm : windowWidth === 375 ? styles.searchBarHeaderMed : styles.searchBarHeader}><View style={{ flexDirection:'row', flex:1, alignSelf:'center' }}><Text style={{ alignSelf:'center', lineHeight:28 }}>🔎 Search for restaurant</Text></View></View>
              </TouchableOpacity>
            </View>
          </View>
        </View>}

        containerStyle={{height: 150, paddingTop: 50, backgroundColor:"white", top:-50, shadowColor: "#000",shadowOffset: {width: 100,height: 50,},shadowOpacity: 1.0,shadowRadius: 4,elevation: 10, borderBottomColor:'#ccc', borderBottomWidth:0.5}}

        rightComponent={<View style={{ width:85, top:-25 }}><TouchableOpacity onPress={() =>  navigation.navigate('Order Details')}><Image style={{ alignSelf:'flex-end' }} source={require('./components/images/icons/baseline_menu_black_18dp.png')}/></TouchableOpacity></View>}
      />
      <View style={{ position:'absolute', zIndex:1000, bottom:hp('40%'), right:15 }}>
        <DriverCallout/>
      </View>
      <View style={{ backgroundColor:'white', bottom:hp('35%'), position:'absolute', zIndex:999, right:15, shadowColor: "#000",shadowOffset: {width: 0,height: 2,},shadowOpacity: 0.25,shadowRadius: 3.84,elevation: 5 }}>
        <TouchableOpacity onPress={() =>  navigation.navigate('Driver', { count:4 })}>
          <Image style={{ height:50, width:50, borderRadius:10, resizeMode:'contain' }} source={require('./components/images/logo-banner/icon-driver.png')}/>
        </TouchableOpacity>
      </View>
        <ScrollView style={{ backgroundColor:'white', height:hp('100%'), top:-50, paddingTop:15 }}>
            <View style={{ flexDirection:'column', flex:1, paddingLeft:10, paddingRight:10, height:300 }}>
              {/* <View style></View> */}
              <View style={{ flexDirection:'row'}}>
                <View style={{ flex:0.5, paddingBottom:15 }}>
                  <Text style={{ alignSelf:'flex-start', fontSize:18 }}>Browse by cuisine</Text>
                </View>
                <View style={{ flex:0.5, paddingBottom:15 }}>
                  <TouchableOpacity
                    onPress={() => navigation.navigate('Discover Restaurants', { category: 'All'})}>
                    <Text style={{ alignSelf:'flex-end', fontSize:14, lineHeight:24, color:'#ff0018' }}>See all restaurant</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={{ flexDirection:'row', flex:1, paddingBottom:15 }}>
                <View style={{ flex:0.33 }}>
                  <TouchableOpacity
                    onPress={() => navigation.navigate('Discover Restaurants', { category: 'Singapore'})}>
                    <Image source={{uri:'http://cookbox.com.au/data_upload/food_category/singapore.jpg'}} style={windowWidth === 320 ? styles.imgXs : styles.imgMed}/>
                    <Text style={{ alignSelf:'center' }}>Singapore</Text>
                  </TouchableOpacity>
                </View>
                <View style={{ flex:0.33 }}>
                  <TouchableOpacity
                  onPress={() => navigation.navigate('Discover Restaurants', { category: 'Hongkong'})}>
                    <Image source={{uri:'http://cookbox.com.au/data_upload/food_category/hongkong.jpg'}} style={windowWidth === 320 ? styles.imgXs : styles.imgMed}/>
                    <Text style={{ alignSelf:'center' }}>Hongkong</Text>
                  </TouchableOpacity>
                </View>
                <View style={{ flex:0.33 }}>
                  <TouchableOpacity
                    onPress={() => navigation.navigate('Discover Restaurants', { category: 'Thailand'})}>
                    <Image source={{uri:'http://cookbox.com.au/data_upload/food_category/thailand.jpg'}} style={windowWidth === 320 ? styles.imgXs : styles.imgMed}/>
                    <Text style={{ alignSelf:'center' }}>Thailand</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={{ flexDirection:'row', flex:1 }}>
                <View style={{ flex:0.33 }}>
                  <TouchableOpacity
                    onPress={() => navigation.navigate('Discover Restaurants', { category: 'Dimsim'})}>
                    <Image source={{uri:'http://cookbox.com.au/data_upload/food_category/dimsim.jpg'}} style={windowWidth === 320 ? styles.imgXs : styles.imgMed}/>
                    <Text style={{ alignSelf:'center' }}>DimSim</Text>
                  </TouchableOpacity>
                </View>
                <View style={{ flex:0.33 }}>
                  <TouchableOpacity
                    onPress={() => navigation.navigate('Discover Restaurants', { category: 'Seafood'})}>
                    <Image source={{uri:'http://cookbox.com.au/data_upload/food_category/seafood.jpg'}} style={windowWidth === 320 ? styles.imgXs : styles.imgMed}/>
                    <Text style={{ alignSelf:'center' }}>Seafood</Text>
                  </TouchableOpacity>
                </View>
                <View style={{ flex:0.33 }}>
                  <TouchableOpacity
                    onPress={() => navigation.navigate('Discover Restaurants', { category: 'Vegetarian'})}>
                    <Image source={{uri:'http://cookbox.com.au/data_upload/food_category/vegetarian.jpg'}} style={windowWidth === 320 ? styles.imgXs : styles.imgMed}/>
                    <Text style={{ alignSelf:'center' }}>Vegetarian</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
            <View style={{ flexDirection:'column', flex:1, paddingTop:50, paddingLeft:15, paddingRight:15, paddingBottom:25 }}>
              <Text style={{ fontSize:18, paddingBottom:15 }}>Explore our promo</Text>
              <TouchableOpacity>  
                <Image source={{uri: "http://cookbox.com.au/data_upload/promo_banner/1.jpg"}} style={{width:'100%', height:200, marginBottom:20}}/>
                <View style={{ padding:10, backgroundColor:'#ffffff94', position:'absolute', bottom:'12.5%', left:'2.5%', borderRadius:5 }}>
                  <Text style={{ fontSize:22, color:'black' }}>Click Here</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>  
                <Image source={{uri: "http://cookbox.com.au/data_upload/promo_banner/2.jpg"}} style={{width:'100%', height:200, marginBottom:20}}/>
                <View style={{ padding:10, backgroundColor:'#ffffff94', position:'absolute', bottom:'12.5%', left:'2.5%', borderRadius:5 }}>
                  <Text style={{ fontSize:22, color:'black' }}>Click Here</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <Image source={{uri: "http://cookbox.com.au/data_upload/promo_banner/3.jpg"}} style={{width:'100%', height:200, marginBottom:20}}/>
                <View style={{ padding:10, backgroundColor:'#ffffff94', position:'absolute', bottom:'12.5%', left:'2.5%', borderRadius:5 }}>
                  <Text style={{ fontSize:22, color:'black' }}>Click Here</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity>
                <Image source={{uri: "http://cookbox.com.au/data_upload/promo_banner/4.jpg"}} style={{width:'100%', height:200, marginBottom:20}}/>
                <View style={{ padding:10, backgroundColor:'#ffffff94', position:'absolute', bottom:'12.5%', left:'2.5%', borderRadius:5 }}>
                  <Text style={{ fontSize:22, color:'black' }}>Click Here</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{ marginBottom:hp('25%') }}>
              <Footer/>
            </View>
          </ScrollView>
    </SafeAreaView>
  );
}

function PrivacyPolicyScreen({ navigation }) {
  return(
    <View>
      <PrivacyPolicyScreens navigation={navigation}/>
    </View>
  );
}

function SearchLocationScreen({ navigation }) {
  return(
    <View><Text>Search Location Here</Text></View>
  );
}

function ProfileScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Button
        title="Go to Notifications"
        onPress={() => navigation.navigate('Notifications')}
      />
      <Button title="Go back" onPress={() => navigation.goBack()} />
    </View>
  );
}

function NotificationsScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Button
        title="Go to Settings"
        onPress={() => navigation.navigate('Settings')}
      />
      <Button title="Go back" onPress={() => navigation.goBack()} />
    </View>
  );
}

function LoginScreen({ navigation }) {
  return (
    <View style={styles.container}>
      <View style={{ flex:1 }}>
        <Image style={{position:'relative', height:hp('25%')}} source={require('./components/images/logo-banner/banner-login.jpg')}/>
        <TouchableOpacity 
            style={windowWidth <= 375 ? styles.buttonBackLoginSm : styles.buttonBackLogin}
            onPress={() => navigation.goBack()}>
          <Image style={{ height:30, width:30 }}
          source={require('./components/images/icons/baseline_arrow_back_ios_black_18dp.png')}/>
        </TouchableOpacity>
        <Image style={windowWidth <= 375 ? styles.imgBannerLoginSm : styles.imgBannerLogin} source={require('./components/images/logo-banner/Inverse-background.png')}/>
      </View>
      <ScrollView style={{marginTop:15, width:'100%', alignContent:'center', height:300}}>
        <View style={{ flexDirection:'row', flex:0.5, marginBottom:20, justifyContent:'center' }}>
          <Text style={{textAlign:'left', paddingLeft:20, paddingRight:45, 
                        justifyContent:'flex-start', fontSize:24, 
                        alignSelf:'flex-end'}}>
                  Sign in with your CookBox account
          </Text>
        </View>
        <View style={{flexDirection:'column', flex:1, alignItems:'center'}}>
          <Text style={styles.label}>Email or Phone Number</Text>
          <TextInput style={styles.inputBox}
          // underlineColorAndroid='rgba(0,0,0,0)'
          placeholder="Email or Phone Number"
          // placeholderTextColor = "#ffffff"
          // selectionColor="#fff"
          keyboardType="email-address"
          onSubmitEditing={()=> this.password.focus()}
          />
          <Text style={styles.label}>Password</Text>
          <TextInput style={styles.inputBox}
          // underlineColorAndroid='rgba(0,0,0,0)'
          placeholder="Password"
          secureTextEntry={true}
          // placeholderTextColor = "#ffffff"
          ref={(input) => this.password = input}
          />
          <TouchableOpacity style={styles.button}>
            <Text style={styles.buttonText}>Sign In</Text>
          </TouchableOpacity>
          <Text style={{alignSelf:'center'}}>or</Text>
          <TouchableOpacity style={styles.fbButton}>
            <Text style={styles.buttonText}>Continue with Facebook</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.gButton}>
            <Text style={styles.buttonText}>Continue with Google</Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity onPress={() => navigation.navigate('Register')}>
            <Text style={{color:'#ff0018', textAlign:'center', paddingTop:10}}>Create your account</Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
}

function SettingsScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Button title="Go back" onPress={() => navigation.goBack()} />
    </View>
  );
}

function RegisterScreen({ navigation }) {
  return (
    <View style={styles.container}>
      {/* <HeaderLoginRegister/> */}
      <View style={{ flex:1 }}>
        <Image style={{position:'relative', height:hp('25%')}} source={require('./components/images/logo-banner/banner-login.jpg')}/>
        <TouchableOpacity 
            style={{ position:'absolute', borderRadius:50, paddingLeft:'30%', paddingTop:'18.5%', backgroundColor:'white', shadowColor: "#000",shadowOffset: {width: 0,height: 2,},shadowOpacity: 0.25,shadowRadius: 3.84,elevation: 5, top:'15%', left:'70%', height:50, width:50 }}
            onPress={() => navigation.goBack()}>
          <Image style={{ height:30, width:30 }}  source={require('./components/images/icons/baseline_arrow_back_ios_black_18dp.png')}/>
        </TouchableOpacity>
        <Image style={{position:'absolute',top:'15%', right:'70%', height:100, width:100}} source={require('./components/images/logo-banner/Inverse-background.png')}/>
      </View>
      <ScrollView style={{marginBottom:30, width:'100%', alignContent:'center', height:200}}>
        <View style={{ flexDirection:'row', flex:0.5, marginBottom:20, justifyContent:'flex-start', paddingLeft:25, paddingRight:25 }}>
          <Text style={{textAlign:'left', paddingRight:45, 
                        justifyContent:'flex-start', fontSize:24, 
                        alignSelf:'flex-end'}}>
                  Create your account
          </Text>
        </View>
        <View style={{flexDirection:'column', flex:1, alignItems:'center'}}>
          <View style={{ flexDirection:'row', flex:1, paddingLeft:25, paddingRight:25 }}>
            <View style={{ flex:0.5, paddingRight:15 }}>
              <Text style={styles.labelInline}>First Name</Text>
              <TextInput style={styles.inputBoxInline}
              // underlineColorAndroid='rgba(0,0,0,0)'
              placeholder="First Name"
              // placeholderTextColor = "#ffffff"
              // selectionColor="#fff"
              keyboardType="email-address"
              onSubmitEditing={()=> this.password.focus()}
              />
            </View>
            <View style={{flex:0.5}}>
              <Text style={styles.labelInline}>Last Name</Text>
              <TextInput style={styles.inputBoxInline}
              // underlineColorAndroid='rgba(0,0,0,0)'
              placeholder="Last Name"
              // keyboardType="email-address"
              secureTextEntry={false}
              // placeholderTextColor = "#ffffff"
              // ref={(input) => this.password = input}
              />
            </View>
          </View>
          <View style={{ flexDirection:'column', flex:1, paddingLeft:25, paddingRight:25 }}>
            <View style={{ flex:1 }}>
              <Text style={styles.labelRegister}>
                Email
              </Text>
              <TextInput style={styles.inputBoxRegister} placeholder="Email"/>
            </View>
            <View style={{ flex:1 }}>
              <Text style={styles.labelRegister}>
                Address
              </Text>
              <TextInput style={styles.inputBoxRegister} placeholder="Address"/>
            </View>
            <View style={{ flex:1 }}>
              <Text style={styles.labelRegister}>
                Phone Number
              </Text>
              <TextInput style={styles.inputBoxRegister} placeholder="Phone Number"/>
            </View>
          </View>
          <TouchableOpacity style={styles.button}>
            <Text style={styles.buttonText}>Create your account</Text>
          </TouchableOpacity>
          <Text style={{alignSelf:'center'}}>or</Text>
          <TouchableOpacity style={styles.fbButton}>
            <Text style={styles.buttonText}>Continue with Facebook</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.gButton}>
            <Text style={styles.buttonText}>Continue with Google</Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity onPress={() => navigation.navigate('Register')}>
            <Text style={{color:'#ff0018', textAlign:'center', paddingTop:10}}>Create your account</Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
}

function HeaderLeft(props) {
  // const count = this.props.count;
  return (
    // <View style={{ padding:10 }}>
    //   <TouchableOpacity onPress={() => navigation.navigate('Home')}>
      <View style={{ flexDirection:'column', flex:1, alignItems:'center', paddingLeft:5, paddingRight:5 }}>
        <View style={{ flexDirection:'row', flex:1, alignItems:'center' }}>
          <View style={{ flex:0.9 }}>
            <FontAwesomeIcon icon={faChevronLeft} size="25x"/>
            {/* <Image source={ require('./components/images/icons/baseline_clear_black_18dp.png') } style={{ width:24, height:24,marginLeft:12.5 }}/> */}
          </View>
          <View style={{ flex:0.1 }}>
            <Text style={{ fontSize:21 }}>{props.count}</Text>
          </View>
        </View>
      </View>
  );
}

function DiscoverRestaurantsScreen({ route, navigation }){
  const { category }  = route.params;
  const { title }     = route.params;
  // const { title }     = 'Discover '.categor÷y;
  return (
    <View style={{ flexDirection:'column', flex:1 }}> 
      <DiscoverRestaurants navigation={navigation} category={category}/>
    </View>
  )
}

const Stack   = createStackNavigator();
const Tab     = createBottomTabNavigator();
const TopTab  = createMaterialTopTabNavigator();
const Drawer  = createDrawerNavigator(); 

// function TestPushNotifScreen(){
//   return (
//     <View style={{ flexDirection:'column', flex:1 }}>
//       <TestPushNotif/>
//     </View>
//   )
// }

function ReviewOrderScreen(){
  return(
    <SafeAreaView>
      <ReviewOrder/>
    </SafeAreaView>
  );
}

function AcceptOrderScreen({ navigation }){
  return(
    <View>
      <StatusBar barStyle="light-content"/>
      <AcceptOrder navigation={navigation}/>
    </View>
  );
}

function OnRestaurantScreen({ navigation }){
  return(
    <View>
      <StatusBar barStyle="light-content"/>
      <OnRestaurant navigation={navigation}/>
    </View>
  )
}

function OrderInProgressScreen({ navigation }){
  return(
    <View>
      <StatusBar barStyle="light-content"/>
      <OrderInProgress navigation={navigation}/>
    </View>
  )
}

function TrackingMapsScreen({ navigation }){
    return(
        <View>
            <TrackingMapsDriver navigation={navigation}/>
        </View>
    )
}

function AchievementDriverScreen({ navigation }){
  return(
    <View style={{ backgroundColor:'#f2f2f2', height:'100%' }}>
      <AchievementDriver navigation={navigation}/>
    </View>
  );
}

function ProfileDriverScreen({ navigation, isLogin }){
  return(
    <View style={{ backgroundColor:'#f2f2f2', height:'100%' }}>
      <ProfileDriver navigation={navigation} isLogin={<IsLogin navigation={navigation}/>}/>
    </View>
  )
}

function DriverStack({route, navigation}){
  // const { count }   = route.params;
  // var cnt           = count;
  // console.log(cnt);
  return(
      <Stack.Navigator initialRouteName="Home Driver">
        <Stack.Screen name="Home Driver" component={HomeDriverScreen}/>
        <Stack.Screen
          name="Confirm Order"
          component={ConfirmOrdersonDriverScreen}
          // options={({ navigation, route }) => ({
          //   headerLeft: count => <TouchableOpacity onPress={() => navigation.goBack()}><HeaderLeft count={cnt} /></TouchableOpacity>,
          //   headerLeftTitle: () => (<Text>{count}</Text>),
          //   headerTitle: () => (<Text style={{ fontSize:21 }}>Confirm Order</Text>),
          // })}
        />
        <Stack.Screen name="Accept Order" component={AcceptOrderScreen} options={{headerShown: false}} navigationOptions={{tabBarVisible:false}}/>
      </Stack.Navigator>
  );
}

function HomeStack({route, navigation}) {
  // const { category }  = route.params;
  var enableGesture     =   null;
  var confirmed         =   true;
  if(confirmed === true){
    var enableGesture   = false;
  } else if(confirmed === false) {
    var enableGesture   = true;
  }
  return (
    // <MainTab/>
    <Stack.Navigator initialRouteName="Driver">
      {/* <Stack.Screen name="Tab" component={MainTab} options={{ headerShown:false }}/> */}
      {/* <Stack.Screen name="Test Push Notif" component={TestPushNotifScreen}/> */}
      <Stack.Screen name="Web View" component={WebViewScreen} options={{headerShown: false}}/>
      <Stack.Screen name="Driver" component={HomeDriverScreen} options={{ headerShown: true, headerTitle:'Driver Screen', tabBarVisible: false}}/>
      <Stack.Screen name="Accept Order" component={AcceptOrderScreen} options={{headerShown: false}}/>
      <Stack.Screen name="Go to Restaurant" component={OnRestaurantScreen} 
                        options={({ navigation, route }) => 
                        ({
                          headerStyle: { 
                            backgroundColor:'#2c2c2c',
                            shadowColor: "#000",
                            shadowOffset: {
                              width: 0,
                              height: 2,},
                            shadowOpacity: 0.25,
                            shadowRadius: 3.84,
                            elevation: 5,
                          },
                          headerTintColor:'white',
                          headerTitleStyle:{fontSize:20,
                          fontWeight:'500'},
                          headerLeft: () => (
                              <View style={{ padding:10 }}>
                                <TouchableOpacity onPress={() => navigation.navigate('Confirm Order')} style={{ }}>
                                { Platform.OS === 'ios'
                                ?
                                    <FontAwesomeIcon icon={faTimesCircle} size="27x" color="white"/>
                                :
                                    <FontAwesomeIcon icon={faTimesCircle} size="27" color="white"/>
                                }
                                </TouchableOpacity>
                              </View>),
                          gestureEnabled: enableGesture,
                        })}
      />
      <Stack.Screen name="Order In Progress" component={OrderInProgressScreen}
                      options={({ navigation, route }) => 
                      ({
                        headerStyle: { 
                          backgroundColor:'#2c2c2c',
                          shadowColor: "#000",
                          shadowOffset: {
                            width: 0,
                            height: 2,},
                          shadowOpacity: 0.25,
                          shadowRadius: 3.84,
                          elevation: 5,
                        },
                        headerTintColor:'white',
                        headerTitleStyle:{fontSize:20,
                        fontWeight:'500'},
                        headerLeft: () => (
                            <View style={{ padding:10 }}>
                              <TouchableOpacity onPress={() => navigation.navigate('Confirm Order')} style={{ }}>
                                { Platform.OS === 'ios'
                                ?
                                    <FontAwesomeIcon icon={faTimesCircle} size="27x" color="white"/>
                                :
                                    <FontAwesomeIcon icon={faTimesCircle} size="27" color="white"/>
                                }
                              </TouchableOpacity>
                            </View>),
                        gestureEnabled: enableGesture,
                      })}
      />
      <Stack.Screen name="Order Details" component={OrderDetailsScreen}
      options={{
        headerBackImage: ()=>(<HeaderLeft/>), headerBackTitle: "  ",
      }}
      />
      <Stack.Screen name="Track Order" component={TrackingMapsScreen} options={{headerShown: false}}/>
      <Stack.Screen name="Home" component={HomeScreen} options={{headerShown: false}}/>
      <Stack.Screen name="Privacy Policy" component={PrivacyPolicyScreen}
      options={{
        headerTitle: 'Privacy Policy', headerBackImage: ()=>(<HeaderLeft/>),headerBackTitle: "  ",}}
      />
      <Stack.Screen name="Discover Restaurants" component={DiscoverRestaurantsScreen}
      // initialParams={{ category: {category} }}
      options={({ route }) => 
              ({ 
                headerTitle: 'Discover '+route.params.category+' Restaurants',
                headerBackImage:()=>(<HeaderLeft/>),
                headerBackTitle: "  ",
              })}
      />
      <Stack.Screen name="Search Restaurant" component={SearchLocationScreen} 
      options={{
        headerTitle: 'Change Your Address', headerBackImage: ()=>(<HeaderLeft/>),headerBackTitle: "  ",}}
      />
      <Stack.Screen name="Review Order" component={ReviewOrderScreen}/>
      <Stack.Screen name="Confirm Order" component={ConfirmOrdersonDriverScreen} options={{ headerTitle:'Confirm Your Order' }}/>
      <Stack.Screen name="Login" component={LoginScreen} options={{headerShown: false}}/>
      <Stack.Screen name="Register" component={RegisterScreen} options={{headerShown: false}}/>
      <Stack.Screen name="Notifications" component={NotificationsScreen} />
      <Stack.Screen name="Profile" component={ProfileScreen} />
      <Stack.Screen name="Settings" component={SettingsScreen} />
    </Stack.Navigator>
  );
}


function OrderDetailsScreen({navigation}){
  return(
    <SafeAreaView style={{ flexDirection:'column', flex:1, backgroundColor:'#fbfbfc' }}>
      <OrderDetails navigation={navigation}/>
    </SafeAreaView>
  );
}

function ConfirmOrdersonDriverScreen({ navigation, route }){
  const count  = route.params;
  return(
    <SafeAreaView style={{ flexDirection:'column', flex:1, backgroundColor:'#fbfbfc' }}>
      <ConfirmOrdersonDriver navigation={navigation} count={count}/>
    </SafeAreaView>
  )
}

function WebViewScreen(){
  return(
    <View>
      <WebViewComponents/>
    </View>
  )
}

function HomeDriverScreen({navigation}){
  return(
    <SafeAreaView style={{ backgroundColor:'white', height:hp('100%'), color:'black' }}>
      {/* <StatusBar barStyle="dark-content" backgroundColor="black"/> */}
      {/* <Text>AAA</Text> */}
      {/* <StatusBar barStyle="light-content"/> */}
      <HomeAppDriver navigation={navigation}/>
    </SafeAreaView>
  )
}

class DriverTab extends React.Component {
  constructor(){
    super();
    this.state = {
      isLogin: true,
    }
  }
  // static navigationOptions = {
  //   header: null
  // }
  render(){
    return (
        <SafeAreaProvider>
        <View style={{ height:statusBarHeight, backgroundColor:'#f54f29', position:'relative', top:0 }}></View>
        <StatusBar barStyle="light-content"/>
          <TopTab.Navigator 
            style={{ borderBottomWidth:0, color:'black' }}
            tabBarOptions={{
              activeTintColor: 'white', 
              inactiveTintColor: '#ccc',
              showIcon: true,  
              showLabel: true,
              upperCaseLabel: false,
              indicatorStyle: {
                display:'none',
              },
              labelStyle: { 
                textTransform: 'none',
                fontWeight:'600',
                fontFamily:'Asap-Bold',
                fontSize:16,
                left:'5%',
              },
              style: {  
                  backgroundColor:'#f54f29',
                  color:'black',
                  borderBottomWidth:0,
                  // marginTop:-20, 
              }  
            }}
            swipeEnabled={false}
            screenOptions={({ route }) => ({
              tabBarIcon: ({ focused, color, size }) => {
                let iconName;
                let iconURL;
                let iconColor;
                let iconSize = '30';
                if (route.name === 'Home') {
                  // iconName = focused
                  //   ? 'ios-information-circle'
                  //   : 'ios-information-circle-outline';
                  iconName = focused
                    ? require('./components/images/logo-banner/home-white.png')
                    : require('./components/images/logo-banner/home-grey.png');
                  iconColor = focused
                    ? 'white'
                    : '#ccc';
                } else if (route.name === 'Point') {
                  // iconName = focused ? 'ios-list-box' : 'ios-list';
                  iconName = focused
                    ? require('./components/images/logo-banner/point-white.png')
                    : require('./components/images/logo-banner/point-grey.png');
                  iconColor = focused
                    ? 'white'
                    : '#ccc';
                } else if (route.name === 'Account') {
                  // iconName = focused ? 'ios-list-box' : 'ios-list';
                  iconName = focused
                    ? require('./components/images/logo-banner/account-white.png')
                    : require('./components/images/logo-banner/account-grey.png');
                  iconColor = focused
                    ? 'white'
                    : '#ccc';
                }
                // You can return any component that you like here!
                // return <FontAwesomeIcon icon={iconName} color={iconColor} size={iconSize}/>;
                return <Image source={iconName} style={{ width:30, height:30 }} />;
              },
            })}>
            {/* <Tab.Screen name="Login"/> */}
            <TopTab.Screen name="Home" component={HomeDriverStack}/>
            <TopTab.Screen name="Point" component={AchievementDriverScreen}/>
            <TopTab.Screen name="Account" component={ProfileDriverScreen}/>
          </TopTab.Navigator>
        </SafeAreaProvider>
    );
  }
}

function OrderOutOfStockScreen({navigation, route}){
  // const {navigation} = props;
  const { id } = route.params;
  return(
    <View>
      <OutOfStockNew navigation={navigation} id={id}/>
    </View>
  );
}

function TrackDriverScreen({navigation}){
  return(
    <View style={{ backgroundColor:'white', height:hp('100%') }}>
      <TrackOrderDriver navigation={navigation}/>
    </View>
  );
}

function HomeDriverInactiveScreen({navigation}){
  return(
    <View style={{ height:'100%' }}>
      <HomeDriverInactive navigation={navigation}/>
    </View>
  )
}

function HomeDriverStack(){
  return(
    <SafeAreaProvider>
      <Stack.Navigator initialRouteName="Home" onNavigationStateChange={null} screenOptions={{ headerShown:false }}>
        <Stack.Screen name="Home" component={HomeDriverScreen}/>
        <Stack.Screen name="Inactive" component={HomeDriverInactiveScreen}/>
        <Stack.Screen name="Order Out of Stock" component={OrderOutOfStockScreen}/>
        <Stack.Screen name="Track Order" component={TrackDriverScreen}/>
        {/* <Stack.Screen name="Login" component={LoginDriverScreen}/> */}
        {/* <Stack.Screen name="Home" component={DriverTab} options={{ gestureEnabled: false }}/> */}
      </Stack.Navigator>
    </SafeAreaProvider>
  );
}

function MainTab({route, navigation}) {
  // if(navigation.state.routeName === "Accept Order"){
  //   return null;
  // } else {
    return (
      <Tab.Navigator
            screenOptions={({ route }) => ({
              tabBarIcon: ({ focused, color, size }) => {
                let iconName;
                let iconURL;
                if (route.name === 'Home') {
                  // iconName = focused
                  //   ? 'ios-information-circle'
                  //   : 'ios-information-circle-outline';
                  iconName = focused
                    ? require('./components/images/icons/baseline_home_black_24dp.png')
                    : require('./components/images/icons/outline_home_black_24dp.png');
                } else if (route.name === 'Profiles') {
                  // iconName = focused ? 'ios-list-box' : 'ios-list';
                  iconName = focused
                    ? require('./components/images/icons/baseline_person_black_24dp.png')
                    : require('./components/images/icons/outline_person_black_24dp.png');
                } else if (route.name === 'Order') {
                  // iconName = focused ? 'ios-list-box' : 'ios-list';
                  iconName = focused
                    ? require('./components/images/icons/baseline_add_shopping_cart_black_24dp.png')
                    : require('./components/images/icons/baseline_add_shopping_cart_black_24dp.png');
                } else if (route.name === 'Menu') {
                  // iconName = focused ? 'ios-list-box' : 'ios-list';
                  iconName = focused
                    ? require('./components/images/icons/outline_restaurant_menu_black_24dp.png')
                    : require('./components/images/icons/outline_restaurant_menu_black_24dp.png');
                } else if (route.name === 'Notifications') {
                  // iconName = focused ? 'ios-list-box' : 'ios-list';
                  iconName = focused
                    ? require('./components/images/icons/baseline_notifications_black_24dp.png')
                    : require('./components/images/icons/outline_notifications_black_24dp.png');
                }
  
                // You can return any component that you like here!
                return <Image source={iconName} style={{ width:30, height:30 }} />;
              },
            })}
            tabBarOptions={{
              activeTintColor: 'tomato',
              inactiveTintColor: 'gray',
              tabBarPosition: 'top',
            }}>
          <Tab.Screen name="Home" component={HomeStack} />
          <Tab.Screen name="Menu"
          component={ProfileScreen}/>
          <Tab.Screen name="Order"
          component={OrderDetailsScreen}/>
          {/* <Tab.Screen name="Notifications"
          component={TestPushNotifScreen}/> */}
          <Tab.Screen name="Profiles"
          component={ProfileScreen}/>
        </Tab.Navigator>
    );
  // }
}

function WelcomeDriverScreen({navigation}){
  return(
    <View>
      <WelcomeDriver navigation={navigation}/>
    </View>
  );
}

function LoginDriverScreen({navigation}){
  return(
    <View>
      <LoginDriver navigation={navigation}/>
    </View>
  )
}

function ChatDriverScreen({navigation}){
  // const { id } = route.params;
  return(
    <View>
      <ChatDriver navigation={navigation}/>
    </View>
  )
}

function BeforeLoginStack(){
  return(
    <Stack.Navigator screenOptions={{ headerShown:false }} initialRouteName="Welcome">
      <Stack.Screen name="Welcome" component={WelcomeDriverScreen}/>
      <Stack.Screen name="Forgot Password" component={ForgotPasswordDriverScreen}/>
      <Stack.Screen name="Chat CS" component={ChatDriverScreen} 
        options={({navigation, route}) =>
        ({ 
          headerShown: true,
          headerStyle: {
            backgroundColor:'#ff0018',
          },
          headerTintColor:'white',
          headerBackTitle:'',
          headerLeft: () => (
            <View style={{ padding:10 }}>
              <TouchableOpacity onPress={() => navigation.goBack()}>
                <FontAwesomeIcon icon={faArrowLeft} size="27" color="white"/>
              </TouchableOpacity>
            </View>
          ),
          headerTitle: () => (
            <View style={{ flexDirection:'column' }}>
              <Text style={{ color:'white', fontFamily:'Asap-Bold' }}>CUSTOMER SERVICE</Text>
              <Text style={{ color:'white', fontFamily:'Asap-Bold' }}>Typically replies whiting a minute</Text>
            </View>
          ),
        })}
      />
      <Stack.Screen name="Login" component={LoginDriverScreen}/>
      <Stack.Screen name="Home" component={DriverTab} options={{ gestureEnabled: true }}/>
    </Stack.Navigator>
  );
}

function TestingRefreshScreen({ navigation }){
  return(
    <View>
      <TestingRefresh navigation={navigation}/>
    </View>
  )
}

function TestingResponseJSONScreen({ navigation }){
  return(
    <View>
      <TestResponseJSON navigation={navigation}/>
    </View>
  )
}

function ForgotPasswordDriverScreen({ navigation }){
  return(
    <View>
      <ForgotPasswordDriver navigation={navigation}/>
    </View>
  )
}

function AfterLoginStack(){
  return(
    <Stack.Navigator screenOptions={{ headerShown:false }} initialRouteName="Home">
      <Stack.Screen name="Welcome" component={WelcomeDriverScreen}/>
      <Stack.Screen name="Forgot Password" component={ForgotPasswordDriverScreen}/>
      <Stack.Screen name="Chat CS" component={ChatDriverScreen}
        options={({navigation, route}) =>
        ({ 
          headerShown: true,
          headerStyle: {
            backgroundColor:'#ff0018',
            height:hp('10%'),
            marginTop:statusBarHeight+25
          },
          headerTintColor:'white',
          headerBackTitle:'',
          headerLeft: () => (
            <View style={{ padding:10 }}>
              <TouchableOpacity onPress={() => navigation.goBack()} style={{ marginTop:statusBarHeight, marginBottom:hp('1%') }}>
                <FontAwesomeIcon icon={faArrowLeft} size="27" color="white"/>
              </TouchableOpacity>
            </View>
          ),
          headerTitle: () => (
            <View style={{ flexDirection:'column', paddingTop:statusBarHeight, marginBottom:hp('1%') }}>
              <Text style={{ color:'white', fontFamily:'Asap-Bold' }}>CUSTOMER SERVICE</Text>
              <Text style={{ color:'white', fontFamily:'Asap-Bold' }}>Typically replies whiting a minute</Text>
            </View>
          ),
        })}
      />
      {/* <Stack.Screen name="Testing" component={TestingRefreshScreen}/> */}
      <Stack.Screen name="Testing" component={TestingResponseJSONScreen}/>
      <Stack.Screen name="Login" component={LoginDriverScreen}/>
      <Stack.Screen name="Home" component={DriverTab} options={{ gestureEnabled: true }}/>
    </Stack.Navigator>
  );
}

const styles = StyleSheet.create({
  container : {
    flexGrow: 1,
    justifyContent:'center',
    alignItems: 'center',
    backgroundColor: 'white'
  },
  orderDetailsFont:{
    fontSize:16,
  },
  fontFamilyRegular:{
    fontFamily:'Asap-Regular'
  },
  fw400:{
      fontWeight:'400'
  },
  TriangleShapeCSS: {
    width: 0,
    height: 0,
    borderLeftWidth: 0,
    borderRightWidth: 15,
    borderTopWidth: 15,
    borderBottomWidth: 0,
    borderStyle: 'solid',
    left:100,
    top:0,
    backgroundColor: 'transparent',
    borderLeftColor: 'transparent',
    borderRightColor: 'transparent',
    borderBottomColor: '#ff0018cc',
    borderTopColor: '#ff0018cc',
  },
  buttonBackLogin: {
    position:'absolute', 
    borderRadius:50, 
    paddingLeft:'30%', 
    paddingTop:'18.5%', 
    backgroundColor:'white', 
    shadowColor: "#000",
    shadowOffset: {width: 0,height: 2,},
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5, 
    top:'15%', 
    left:'70%', 
    height:50, 
    width:50
  },
  buttonBackLoginSm: {
    position:'absolute', 
    borderRadius:50, 
    paddingLeft:'30%', 
    paddingTop:'18.5%', 
    backgroundColor:'white', 
    shadowColor: "#000",
    shadowOffset: {width: 0,height: 2,},
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5, 
    top:'15%', 
    left:'80%', 
    height:50, 
    width:50
  },
  imgBannerLogin: {
    position:'absolute',
    top:'15%', 
    right:'70%', 
    height:100, 
    width:100
  },
  imgBannerLoginSm: {
    position:'absolute',
    top:'15%', 
    right:'80%', 
    height:100, 
    width:100
  },
  searchBarHeader:{
    alignSelf:'center', 
    width:wp('95%'), 
    left:-7.5, 
    borderWidth:1, 
    borderColor:'#ccc', 
    height:40, 
    borderRadius:10, 
    paddingTop:0, 
    paddingLeft:10
  },
  searchBarHeaderMed:{
    alignSelf:'center', 
    width:wp('95%'), 
    left:-2.5, 
    borderWidth:1, 
    borderColor:'#ccc', 
    height:40, 
    borderRadius:10, 
    paddingTop:0, 
    paddingLeft:10
  },
  searchBarHeaderSm:{
    alignSelf:'center', 
    width:wp('95%'), 
    left:2.5, 
    borderWidth:1, 
    borderColor:'#ccc', 
    height:40, 
    borderRadius:10, 
    paddingTop:0, 
    paddingLeft:10
  },
  imgXs: {
    width: 75,
    height:75,
    alignSelf:'center',
    borderRadius:50
  },
  imgMed: {
    width: 100,
    height:100,
    alignSelf:'center',
    borderRadius:50
  },
  flexSm: {
    flex:0.33,
    paddingLeft:0,
    paddingRight:0,
  },
  flexMed: {
    flex:0.33,
  },
  fontXs: {
    fontSize: 8,
  },
  fontSm: {
    fontSize: 10,
  },
  fontMed: {
    fontSize: 12,
  },
  fontLg: {
    fontSize: 14,
  },
  inputBox: {
    width:300,
    height:50,
    borderColor:'#ccc',
    borderWidth:0.5,
    backgroundColor:'rgba(255, 255,255,0.2)',
    borderRadius: 10,
    paddingHorizontal:16,
    fontSize:16,
    color:'black',
    marginVertical: 10
  },
  inputBoxRegister: {
    width:wp('90%'),
    height:50,
    borderColor:'#ccc',
    borderWidth:0.5,
    backgroundColor:'rgba(255, 255,255,0.2)',
    borderRadius: 10,
    paddingHorizontal:16,
    fontSize:16,
    color:'black',
    marginVertical: 10
  },
  inputBoxInline: {
    // width:300,
    height:50,
    borderColor:'#ccc',
    // marginLeft: 50,
    borderWidth:0.5,
    backgroundColor:'rgba(255, 255,255,0.2)',
    borderRadius: 10,
    paddingHorizontal:16,
    fontSize:16,
    color:'black',
    marginVertical: 10
  },
  label: {
    alignSelf: 'flex-start',
    paddingLeft: '15%',
    paddingRight: '15%',
  },
  labelRegister: {
    textAlign: 'left',
    paddingLeft: 5,
  },
  labelInline: {
    textAlign: 'left',
    paddingLeft: 5,
  },
  button: {
    width:300,
    backgroundColor:'#ff0018',
    borderRadius: 4,
    marginVertical: 10,
    paddingVertical: 13
  },
  fbButton: {
    width:300,
    backgroundColor:'#3b5998',
    borderRadius: 4,
    marginVertical: 10,
    paddingVertical: 13
  },
  gButton:{
    width:300,
    backgroundColor:'#4285f4',
    borderRadius: 4,
    marginVertical: 10,
    paddingVertical: 13
  },
  buttonText: {
    fontSize:16,
    fontWeight:'500',
    color:'#ffffff',
    textAlign:'center'
  }
});

export default class App extends React.Component {
  constructor(){
    super();
    this.state = {
        loggedIn : null,
        isLoading: true,
        // loaded: false,
    }
  }

  componentDidMount(){
    this.fetchLoggedIn();
  }

  fetchLoggedIn = async () => {
    try {
        const value = await AsyncStorage.getItem('isLogin');
        console.log(value);
        if (value == 'true') {
          // We have data!!
          // console.log(value);
          this.setState({
              loggedIn : true,
              // loaded: false,
          });
        } else if(value == 'false' || value == null) {
            this.setState({
                loggedIn: false,
                // loaded: true,
            });
        }
      } catch (error) {
        // Error retrieving data
    }
  }

  render(){
    // console.log(this.state.loggedIn);
    if(this.state.loggedIn === false){
      return (
        // <SafeAreaProvider>
        //   <View style={{ backgroundColor:'black', height:statusBarHeight }}></View>
        //   <StatusBar barStyle="dark-content" backgroundColor="black"/>
          <NavigationContainer>
            {/* <MyStack /> */}
            {/* <Drawer.Navigator>
              <Drawer.Screen name="  " component={MainTab}
              drawerStyle={{
                opacity: 0,
                width: 240,
              }}/>
          </Drawer.Navigator> */}
            <BeforeLoginStack/>
            {/* <MainTab/> */}
            {/* <DriverTab/> */}
          </NavigationContainer>
        // </SafeAreaProvider>
      );
    } else if(this.state.loggedIn === true) {
      return(
        // <SafeAreaProvider>
        // <View style={{ backgroundColor:'#FF2D00', height:statusBarHeight }}></View>
          // <StatusBar barStyle="light-content" backgroundColor="black"/>
          <NavigationContainer>
            <AfterLoginStack/>
          </NavigationContainer>
        // </SafeAreaProvider>
      )
    } else {
      return(
        null
        // <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        //   <ActivityIndicator size="large" color="black" style={{ alignSelf:'center' }} />
        // </View>
      )
    }
  }
}