import React, { Component } from 'react';
import { View, Text, StyleSheet, AsyncStorage } from 'react-native';
import { ScrollView, TouchableHighlight, TouchableOpacity } from 'react-native-gesture-handler';
import { withNavigation, SafeAreaView } from 'react-navigation';
import ButtonApproveDriver from '../approve/ButtonApproveDriver';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';

class ActiveOrderListDriver extends React.Component{
    constructor(){
        super();
        this.count = [{id:'1',status:'true'},{id:'2',status:'true'},{id:'3',status:'true'},{id:'4', status:'true'}]
        this.state = {
            countOrders: null,
            count: [{id:'1',status:'true'},{id:'2',status:'true'},{id:'3',status:'true'},{id:'4', status:'true'}],
        }
        // var length = Object.keys(this.state.count).length;
        // console.log(length+' test');
    }

    countSumVal(countVal){
        this.setState({
            countOrders: countVal,
        });
    }

    componentDidMount(){
        this.setState({
            countOrders: Object.keys(this.state.count).length,
        });
        // console.log(this.state.count+' AAA');
        // var length = Object.keys(this.state.count).length;
        // this.getOrderSum();
    }

    deleteOrder(key){
        var length = Object.keys(this.state.count).length;
        var keyVal = key;
        var hasChanges = null;
        // this.setState({
        //     count:[]
        // });
        var arr = [];
        for(var i = 0; i < length; i++){
            // console.log(this.state.count[i].id);
            var thisID = this.state.count[i].id
            var thisStatus = this.state.count[i].status
            console.log(keyVal+' , '+thisID);
            if( thisID == keyVal){
                // this.count[i].splice(i, 1);
                // this.getStatus(this.count[i], false);
                // arr.push({id:this.state.count[i].id,status:'false'},)
                // this.setState({
                //     count:[
                //         {id:this.state.count[i].id,status:'false'},
                //     ]
                // })
                // var functionName
                // this.getStatus()
                arr.push({id:this.state.count[i].id,status:'false'},);
            } 
            else if(thisID != keyVal && thisStatus == 'false') {
                arr.push({id:this.state.count[i].id,status:'false'},);
                // this.setStatus()
            } else if(thisID != keyVal && thisStatus == 'true') {
                arr.push({id:this.state.count[i].id,status:'true'},);
            }
            // console.log(arr);
            this.setState({
                count: arr
            });
        }
        var count = null;
        for(var j = 0; j < length; j++){
            if(arr[j].status == 'true'){
                count++;
            }
        }
        this.countSumVal(count);
        console.log(count+' orders remaining');
        console.log(this.state.count);
        console.log(arr);
    }


    // getStatus(key){
    //     return
    // }
    // getKey(key)

    renderOrderList(){
        return(
            <View>
            {
                this.state.count.map(( item, key ) =>
                (
                item.status == 'true'
                ?
                <View key={key} ref={key} style={{ height:320, marginBottom:20 }}>
                    {/* <Text>{Object.keys(this.state.count).length}</Text>
                    <Text>{item.id}</Text> */}
                    <View style={{ marginBottom:15, borderColor:'#ccc', borderWidth:0.5, borderRadius:5, padding:15,
                                    backgroundColor:'white', shadowColor: "#000",
                                    shadowOffset: {width: 0,height: 2,},
                                    shadowOpacity: 0.25,shadowRadius: 2.25,
                                    elevation: 5, }}>
                        <View style={{ borderRadius:5, marginLeft:15, marginRight:15, position:'absolute', zIndex:1, top:15, left:10 }}>
                            <TouchableOpacity 
                                onPress={() => alert('Pressed Track')} 
                                style={{ zIndex:1, backgroundColor:'purple', borderRadius:5, borderTopRightRadius:5, width:'100%', marginTop:10, paddingLeft:15, paddingRight:15, paddingTop:5, paddingBottom:5 }}>
                                <Text style={{ fontFamily:'Asap-Bold', color:'white' }}>Track</Text>
                            </TouchableOpacity>
                        </View>
                        <TouchableOpacity onPress={() => alert('Pressed Maps')} style={{ width:'100%', height:150, marginBottom:5 }}>
                            <View style={{position: 'absolute', top: 0, right: 0, bottom: 0, left: 0}} />
                            <MapView
                                style={styles.map }
                                provider={PROVIDER_GOOGLE}
                                scrollEnabled={false}
                                initialRegion={{
                                latitude: -27.3810232,
                                longitude: 152.4326816,
                                latitudeDelta: 1,
                                longitudeDelta: 1,
                                }}
                            />
                        </TouchableOpacity>
                        <Text style={[styles.fontFamilyRegular, { fontSize:12, color:'red', marginBottom:5 }]}>Rynaldo Wahyudi</Text>
                        <Text style={[styles.fontFamilyRegular, { fontSize:12, color:'red', marginBottom:5 }]}>+61-123-456-789</Text>
                        <View style={{ marginBottom:25, height:25 }}>
                            <Text style={[styles.fontFamilyRegular, { fontSize:15 }]}>1x SeaBox Prawn</Text>
                            <Text style={[styles.fontFamilyRegular, { fontSize:15 }]}>1x Dimsim Seafood</Text>
                        </View>
                        <View style={{ flexDirection:'row' }}>
                            <Text style={[styles.fontFamilyRegular, { color:'blue', fontWeight:'700' }]}>Destination:</Text>
                            <Text style={[styles.fw400, styles.fontFamilyRegular, {paddingLeft:5, paddingRight:5}]}>333 Swann Road</Text>
                            {/* <Text>{key+1}</Text> */}
                        </View>
                        <View>
                            <ButtonApproveDriver navigation={this.props.navigation} orderId={key+1} whileDone={this.deleteOrder.bind(this, key+1)}/>
                        </View>
                    </View>
                </View>
                :
                null
                ))
            }
        </View>
        )
    }
    render(){
        return(
            <View style={{ backgroundColor:'white', flexDirection:'column', flex:1, paddingTop:0}}>
                <View style={{ flexDirection:'row', flex:0.05, marginBottom:5 }}>
                    <View style={{ flex:0.175 }}>
                        <Text style={[styles.fontFamilyRegular, { fontSize:17, lineHeight:25 }]}>Status:</Text>
                    </View>
                    <View style={{ flex:0.375, alignSelf:'center' }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Testing')} style={{ paddingBottom:2.5, paddingTop:2.5, backgroundColor:'#4FCA55', 
                                        alignItems:'center', borderRadius:5, alignSelf:'flex-start', paddingLeft:15, paddingRight:15 }}>
                            <Text style={[styles.fontFamilyRegular, { fontSize:16, color:'white', fontWeight:'600' }]}>Active</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ flexDirection:'row', flex:0.05, marginBottom:5 }}>
                    <View style={{ flex:0.175 }}></View>
                    <View style={{ flex:0.375 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('Inactive')} style={{ paddingBottom:2.5, paddingTop:2.5, backgroundColor:'#009ce5', 
                                        alignItems:'center', borderRadius:5 }}>
                            <Text style={[styles.fontFamilyRegular, { fontSize:16, color:'white', fontWeight:'600' }]}>On Restaurant</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ flexDirection:'row', flex:0.05, marginTop:10 }}>
                    <View style={{ flex:1 }}>
                        <Text style={styles.fontFamilyRegular}>You have { this.state.countOrders > 0 ? <Text style={{ color:'red' }}>{this.state.countOrders}</Text> :  <Text style={{ color:'red' }}>0</Text>} order</Text>
                    </View>
                </View>
                {
                    this.state.countOrders > 0
                    ?
                    <View style={{ flex:0.7, marginLeft:-5, marginRight:-5 }}>
                        <ScrollView style={{ width:'100%', marginBottom:30}} 
                                    // refreshControl={ <RefreshControl onRefresh={onRefresh} refreshing={onRefresh}/> }
                        >
                            {this.renderOrderList()}
                        </ScrollView>
                    </View>
                    :
                    null
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    fontFamilyRegular:{
        fontFamily:'Asap-Regular'
    },
    fw400:{
        fontWeight:'400'
    },
    map: {
        ...StyleSheet.absoluteFillObject,
        height:150,
        zIndex:-1,
    },
});

export default withNavigation(ActiveOrderListDriver);