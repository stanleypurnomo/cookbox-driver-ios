import React, { Component } from 'react';
import { View, Text, StyleSheet, AsyncStorage, ActivityIndicator, RefreshControl } from 'react-native';
import { ScrollView, TouchableHighlight, TouchableOpacity } from 'react-native-gesture-handler';
import { withNavigation, SafeAreaView, NavigationActions, StackActions } from 'react-navigation';
import ButtonApproveDriver from '../../approve/ButtonApproveDriver';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';

class TestingEachOrderList extends React.Component{
    constructor(){
        super();
        this.state = {
            isLoading: false
        }
    }

    onClickLoadingTest(){
        var thisState = this;
        this.setState({
            isLoading: true,
        });
        setTimeout(function(){thisState.setState({ isLoading: false });},1000);
    }

    render(){
        if(this.state.isLoading === false){
            return(
                <View>
                    <View style={{ height:320, marginBottom:20 }}>
                        {/* <Text>{Object.keys(this.state.count).length}</Text>
                        <Text>{item.id}</Text> */}
                        <View style={{ marginBottom:15, borderColor:'#ccc', borderWidth:0.5, borderRadius:5, padding:15,
                                        backgroundColor:'white', shadowColor: "#000",
                                        shadowOffset: {width: 0,height: 2,},
                                        shadowOpacity: 0.25,shadowRadius: 2.25,
                                        elevation: 5, }}>
                            <View style={{ borderRadius:5, marginLeft:15, marginRight:15, position:'absolute', zIndex:1, top:15, left:10 }}>
                                <TouchableOpacity 
                                    onPress={() => alert('Pressed Track')} 
                                    style={{ zIndex:1, backgroundColor:'purple', borderRadius:5, borderTopRightRadius:5, width:'100%', marginTop:10, paddingLeft:15, paddingRight:15, paddingTop:5, paddingBottom:5 }}>
                                    <Text style={{ fontFamily:'Asap-Bold', color:'white' }}>Track</Text>
                                </TouchableOpacity>
                            </View>
                            <TouchableOpacity onPress={() => alert('Pressed Maps')} style={{ width:'100%', height:150, marginBottom:5 }}>
                                <View style={{position: 'absolute', top: 0, right: 0, bottom: 0, left: 0}} />
                                <MapView
                                    style={styles.map }
                                    provider={PROVIDER_GOOGLE}
                                    scrollEnabled={false}
                                    initialRegion={{
                                    latitude: -27.3810232,
                                    longitude: 152.4326816,
                                    latitudeDelta: 1,
                                    longitudeDelta: 1,
                                    }}
                                />
                            </TouchableOpacity>
                            <Text style={[styles.fontFamilyRegular, { fontSize:12, color:'red', marginBottom:5 }]}>Rynaldo Wahyudi</Text>
                            <Text style={[styles.fontFamilyRegular, { fontSize:12, color:'red', marginBottom:5 }]}>+61-123-456-789</Text>
                            <View style={{ marginBottom:25, height:25 }}>
                                <Text style={[styles.fontFamilyRegular, { fontSize:15 }]}>1x SeaBox Prawn</Text>
                                <Text style={[styles.fontFamilyRegular, { fontSize:15 }]}>1x Dimsim Seafood</Text>
                            </View>
                            <View style={{ flexDirection:'row' }}>
                                <Text style={[styles.fontFamilyRegular, { color:'blue', fontWeight:'700' }]}>Destination:</Text>
                                <Text style={[styles.fw400, styles.fontFamilyRegular, {paddingLeft:5, paddingRight:5}]}>333 Swann Road</Text>
                                {/* <Text>{key+1}</Text> */}
                            </View>
                            <View>
                                <View style={{ alignItems:'center', marginTop:10 }}>
                                    <TouchableOpacity onPress={()=>this.onClickLoadingTest()} style={{ padding:5, backgroundColor:'#009ce5', borderRadius:5 }}>
                                        <Text style={[styles.fontFamilyRegular, { color:'white' }]}>Received</Text>
                                    </TouchableOpacity>
                                </View>
                                {/* <ButtonApproveDriver navigation={this.props.navigation} orderId={1}/> */}
                            </View>
                        </View>
                    </View>
                </View>
            );
        } else {
            return(
                <View style={{ height:250 }}>
                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <View style={{ backgroundColor:'white', borderWidth:1, borderColor:'#ccc', 
                            alignSelf:'center', padding:20, shadowColor: "#000",
                            shadowOffset: {width: 0,height: 2,},
                            shadowOpacity: 0.25,shadowRadius: 3.84,elevation: 5, top:'75%'}}>
                            <ActivityIndicator size="large" color="black" />
                        </View>
                    </View>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    fontFamilyRegular:{
        fontFamily:'Asap-Regular'
    },
    fw400:{
        fontWeight:'400'
    },
    map: {
        ...StyleSheet.absoluteFillObject,
        height:150,
        zIndex:-1,
    },
});

export default withNavigation(TestingEachOrderList);