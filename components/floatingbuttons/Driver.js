import React, { Component } from 'react';
import { View, Image, Text, Dimensions, StyleSheet } from 'react-native';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';

class Driver extends React.Component{
    constructor() {
        super();
        this.state = {
          hideCallout: false,
        //   showMenu2: true,
        };
      }
    
      hideCallout = () => {
        if (this.state.hideCallout == false) {
          this.setState({ hideCallout: true });
        }
      };
    render(){
        return(
        <View style={{ position:'relative' }}>
        {
            !this.state.hideCallout
            ?
            (<View>
                <View style={{ backgroundColor:'#ff0018cc', width:125, borderRadius:10, padding:7.5, top:1.5 }}>
                    <Text style={{ color:'white',fontWeight:'600' }}>Are you driver? Click here...</Text>
                    <View style={{ position:'absolute', right:-10, top:-5, backgroundColor:'transparent', borderRadius:75, shadowColor: "#000",shadowOffset: {width: 0,height: 10,},shadowOpacity: 0.25,shadowRadius: 10,elevation: 5 }}>
                    <TouchableOpacity onPress={this.hideCallout} style={{ zIndex:9999, position:'relative', backgroundColor:'white', borderWidth:0.75, borderColor:'#2c2c2c', borderRadius:75, padding:3.5, width:25, height:25 }}>
                        <Text style={{ textAlign:'center', fontWeight:'800' }}>X</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.TriangleShapeCSS} />
            </View>
            )
            : null
        }
        </View>
        );
    }
}

const styles = StyleSheet.create({
    TriangleShapeCSS: {
        width: 0,
        height: 0,
        borderLeftWidth: 0,
        borderRightWidth: 15,
        borderTopWidth: 15,
        borderBottomWidth: 0,
        borderStyle: 'solid',
        left:100,
        top:0,
        backgroundColor: 'transparent',
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: '#ff0018cc',
        borderTopColor: '#ff0018cc',
      },
});

export default Driver;