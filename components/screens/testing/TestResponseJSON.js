import React, { Component } from 'react';

import {View, Text, AsyncStorage, RefreshControl} from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import ButtonApproveDriver from '../../approve/ButtonApproveDriver';

var statusBarHeight = getStatusBarHeight();
export default class TestResponseJSON extends Component {
    constructor(props) {
        super(props);
        //True to show the loader
        this.state = { 
            loaded: null,
            data: [],
            id:1,
            refreshing: false, 
        };
        this.componentDidMount = this.componentDidMount.bind(this);

    }

    componentDidMount(){
        var This = this;
        var reader = new FileReader();
        var url = 'http://seafoodau.herokuapp.com/api/v1/order/list/2';
        var req = new XMLHttpRequest();
        req.responseType = 'json';
        req.open('GET', url, true);
        req.onreadystatechange  = function(This) {
            var jsonResponse = JSON.stringify(req.response);
            var jsonData = JSON.parse(jsonResponse);
            
            // console.log(jsonData[1]["body"]);
            // AsyncStorage.setItem('testjson',jsonResponse);
        // do something with jsonResponse
            data = jsonData;
            // console.log(data);
            AsyncStorage.setItem('testjson',JSON.stringify(jsonData));
        };
        // jsonData = this.jsonData;
        req.send(null);
        this.fetchJSON();
    }

    ListViewItemSeparator = () => {
        return (
          //returning the listview item saparator view
          <View
            style={{
              height: 0.2,
              width: '100%',
              backgroundColor: '#808080',
            }}
          />
        );
      };

    fetchJSON = async () => {
        try {
            const json = await AsyncStorage.getItem('testjson');
            const jsonVal = JSON.parse(json);
            this.setState({
                data:jsonVal
            });
            console.log(this.state.data);
            console.log('true');
        } catch (error) {
            console.log(error);
        }
    }

    updateData(){
        var url = 'https://jsonplaceholder.typicode.com/posts';
        var req = new XMLHttpRequest();
        req.responseType = 'json';
        req.open('GET', url, true);
        req.onload  = function(This) {
            var jsonResponse = JSON.stringify(req.response);
            var jsonData = JSON.parse(jsonResponse);
            
            // console.log(jsonData[1]["body"]);
            // AsyncStorage.setItem('testjson',jsonResponse);
        // do something with jsonResponse
            data = jsonData;
            // console.log(data);
            AsyncStorage.setItem('testjson',JSON.stringify(jsonData));
        };
        // jsonData = this.jsonData;
        req.send(null);
        this.fetchJSON();
    }

    onRefresh() {
        //Clear old data of the list
        this.setState({ data: [] });
        //Call the Service to get the latest data
        this.updateData();
    }

    render(){
        return(
        <View style={{ marginTop:statusBarHeight }}>
            {/* <Text>{this.state.data.order}</Text> */}
            {
                this.state.data.map((item, key) =>
                (
                    <View style={{ marginBottom:10 }}>
                    <Text>{item.order[0]["id_transaction"]}</Text>
                    <Text>{item.order[0]["total_price"]}</Text>
                    <Text>{item.order[0]["recipient_address"]}</Text>
                    <Text>{item.order[0]["status"]}</Text>
                    <Text>{item.order[0]["fullname"]}</Text>
                    {
                        item.order_items.map((item2, key) =>(
                        <View style={{ flexDirection:'row', marginBottom:10 }}>
                            <Text style={{ flex:0.25 }}>{item2[0]["qty"]}</Text>
                            <Text style={{ flex:0.5 }}>{item2[0]["name"]}</Text>
                        </View>
                        ))
                    }
                    </View>
                ))
            }
            {/* <Text>{this.state.data["body"]}</Text> */}
                    {/* <FlatList   style={{ }}
                        data={this.state.data}
                        keyExtractor={(item, index) => index.toString()}
                        // ItemSeparatorComponent={this.ListViewItemSeparator}
                        renderItem={({item}) => (
                        <View style={{ borderWidth:1, borderColor:'#ccc', marginBottom:10 }}>
                            <Text
                            style={{}}
                            onPress={() => alert(item.order.id_transaction)}>
                                {item[0].order.fullname}
                            </Text>
                        </View>
                        )}
                        refreshControl={
                            <RefreshControl
                              //refresh control used for the Pull to Refresh
                              refreshing={this.state.refreshing}
                              onRefresh={this.onRefresh.bind(this)}
                            />
                        }
                    > */}
                {/* <Text>
                    {testJson[0]}
                </Text> */}
            {/* </FlatList> */}
        </View>
        )
    }
}