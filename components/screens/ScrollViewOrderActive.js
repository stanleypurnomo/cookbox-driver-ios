import React, { Component, useState } from 'react';
import { View, Text, StyleSheet, AsyncStorage, ActivityIndicator, RefreshControl, Switch } from 'react-native';
import { ScrollView, TouchableHighlight, TouchableOpacity } from 'react-native-gesture-handler';
import { withNavigation, SafeAreaView, NavigationActions, StackActions } from 'react-navigation';
import ButtonApproveDriver from '../approve/ButtonApproveDriver';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import TestingEachOrderList from './testing/TestingEachOrderList';
import SwitchOnOff from '../screens/SwitchOnOff';

class ScrollViewOrderActive extends React.Component{
    constructor(){
        super();
        this.state = {
            data:[],
            refreshScroll: false,
            refreshList: false,
        };
    }

    onChange(id){
        var req = new XMLHttpRequest();
        var url = 'http://seafoodau.herokuapp.com/api/v1/order/sending/'+id;
        req.responseType = 'json';
        req.open('GET', url, true);
        var ThisState = this;
        this.setState({
            refreshList: true,
        });
        // req.onload(this.setState({loading:true}));
        req.onload  = function(This) {
            console.log('refreshing '+ThisState.state.refreshing);
            var jsonResponse = req.response;
            console.log(jsonResponse.status);
            ThisState.setState({
                data:[]
            });
            // console.log(JSON.stringify(jsonResponse));
            if(jsonResponse.status === 'success'){
                setTimeout(function(){ThisState.setState({refreshing: false,})},1500);
                console.log(ThisState.state.refreshing+' result');
                console.log('refreshing success'+ThisState.state.refreshing);
                ThisState.onRefresh();
            }
            // ThisState.setState({refreshing: false,});
            setTimeout(function(){ThisState.onDoneAgain();},500);
            // var jsonData = JSON.parse(jsonResponse);
            
            // console.log(jsonData[1]["body"]);
            // AsyncStorage.setItem('testjson',jsonResponse);
        // do something with jsonResponse
            // data = jsonData;
            // console.log(data);
            // AsyncStorage.setItem('testjson',JSON.stringify(jsonData));
        };
        req.send(null)
        // jsonData = this.jsonData;
    }

    onProcess(id){
        var req = new XMLHttpRequest();
        var url = 'http://seafoodau.herokuapp.com/api/v1/order/process/'+id;
        req.responseType = 'json';
        req.open('GET', url, true);
        var ThisState = this;
        this.setState({
            refreshList: true,
        });
        // req.onload(this.setState({loading:true}));
        req.onload  = function(This) {
            console.log('refreshing '+ThisState.state.refreshing);
            var jsonResponse = req.response;
            console.log(jsonResponse.status);
            ThisState.setState({
                data:[]
            });
            // console.log(JSON.stringify(jsonResponse));
            if(jsonResponse.status === 'success'){
                setTimeout(function(){ThisState.setState({refreshing: false,})},1500);
                console.log(ThisState.state.refreshing+' result');
                console.log('refreshing success'+ThisState.state.refreshing);
                ThisState.onRefresh();
            }
            // ThisState.setState({refreshing: false,});
            setTimeout(function(){ThisState.onDoneAgain();},500);
            // var jsonData = JSON.parse(jsonResponse);
            
            // console.log(jsonData[1]["body"]);
            // AsyncStorage.setItem('testjson',jsonResponse);
        // do something with jsonResponse
            // data = jsonData;
            // console.log(data);
            // AsyncStorage.setItem('testjson',JSON.stringify(jsonData));
        };
        req.send(null)
        // jsonData = this.jsonData;
    }

    onDone(id){
        var req = new XMLHttpRequest();
        var url = 'http://seafoodau.herokuapp.com/api/v1/order/completed/'+id;
        req.responseType = 'json';
        req.open('GET', url, true);
        var ThisState = this;
        this.setState({
            refreshing: true,
        });
        // req.onload(this.setState({loading:true}));
        req.onload  = function(This) {
            console.log('refreshing '+ThisState.state.refreshing);
            var jsonResponse = req.response;
            console.log(jsonResponse.status);
            ThisState.setState({
                data:[]
            });
            // console.log(JSON.stringify(jsonResponse));
            if(jsonResponse.status === 'success'){
                setTimeout(function(){ThisState.setState({refreshing: false,})},1500);
                console.log(ThisState.state.refreshing+' result');
                console.log('refreshing success'+ThisState.state.refreshing);
                ThisState.onRefresh();
            }
            // ThisState.setState({refreshing: false,});
            setTimeout(function(){ThisState.onDoneAgain();},500);
            // var jsonData = JSON.parse(jsonResponse);
            
            // console.log(jsonData[1]["body"]);
            // AsyncStorage.setItem('testjson',jsonResponse);
        // do something with jsonResponse
            // data = jsonData;
            // console.log(data);
            // AsyncStorage.setItem('testjson',JSON.stringify(jsonData));
        };
        req.send(null)
        // jsonData = this.jsonData;
    }

    onDoneAgain(){
        this.onRefresh();
        // break;
    }

    onCancel(id){
        this.props.navigation.reset({
            index: 0,
            routes: [
                {   name: 'Order Out of Stock',
                    params:{id: id},
                }
            ]
        });
        // req.send(null)
    }

    componentDidMount(){
        this.getData();
        // alert(this.props.idDriver);
    }

    getData(){
        var thisVar = this;
        var url = 'http://seafoodau.herokuapp.com/api/v1/order/list/'+thisVar.props.idDriver;
        var req = new XMLHttpRequest();
        req.responseType = 'json';
        req.open('GET', url, true);
        // req.onload(this.setState({loading:true}));
        req.onload  = function(This) {
            var jsonResponse = JSON.stringify(req.response);
            var jsonData = JSON.parse(jsonResponse);
            
            // console.log(jsonData[1]["body"]);
            // AsyncStorage.setItem('testjson',jsonResponse);
        // do something with jsonResponse
            // data = JSON.stringify(jsonData);
            thisVar.setState({
                data: jsonData
            })
            // console.log(data);
            // AsyncStorage.setItem('testjson',JSON.stringify(jsonData));
            // alert(thisVar.props.idDriver);
            // alert(thisVar.state.data+' 1st');
        };
        // jsonData = this.jsonData;
        req.send(null);
        thisVar.setState({
            refreshList:true
        });
        setTimeout(function(){thisVar.onRefresh();},1500);
        // thisVar.onRefresh();
        // alert('Test');
    }

    onRefresh() {
        //Clear old data of the list
        this.setState({ data: [], refreshList:true });
        //Call the Service to get the latest data
        // this.updateData();
        var ThisState = this;
        setTimeout(function(){ThisState.setState({ refreshList:false });ThisState.refresh();},2500);
        // setTimeout(function(){ThisState.setState({ refreshing:false });},5000);
        // this.refreshAgain();
        // setTimeout(function(){alert('Refresh Done');},5000);
    }

    onRefreshScroll() {
        //Clear old data of the list
        this.setState({ data: [], refreshScroll:true });
        //Call the Service to get the latest data
        // this.updateData();
        var ThisState = this;
        setTimeout(function(){ThisState.setState({ refreshScroll:false });ThisState.refresh();},1000);
        // setTimeout(function(){ThisState.setState({ refreshing:false });},5000);
        // this.refreshAgain();
        // setTimeout(function(){alert('Refresh Done');},5000);
    }

    refresh(){
        var thisVar = this;
        thisVar.setState({
            data: []
        })
        var url = 'http://seafoodau.herokuapp.com/api/v1/order/list/'+thisVar.props.idDriver;
        var req = new XMLHttpRequest();
        req.responseType = 'json';
        req.open('GET', url, true);
        req.onload  = function(This) {
            var jsonResponse = JSON.stringify(req.response);
            var jsonData = JSON.parse(jsonResponse);
            thisVar.setState({
                data: jsonData
            })
            // console.log(jsonData[1]["body"]);
            // AsyncStorage.setItem('testjson',jsonResponse);
        // do something with jsonResponse
            // data = JSON.stringify(jsonData);
            // alert(thisVar.state.data+' 2nd');
            // alert(thisVar.props.idDriver+' 2nd');
            // AsyncStorage.setItem('testjson',JSON.stringify(jsonData));
        };
        // jsonData = this.jsonData;
        // this.fetchJSON();
        req.send(null);
        this.refreshAgain();
    }

    refreshAgain(){
        var thisVar = this;
        thisVar.setState({
            data: []
        })
        var url = 'http://seafoodau.herokuapp.com/api/v1/order/list/'+thisVar.props.idDriver;
        var req = new XMLHttpRequest();
        req.responseType = 'json';
        req.open('GET', url, true);
        req.onload  = function(This) {
            var jsonResponse = JSON.stringify(req.response);
            var jsonData = JSON.parse(jsonResponse);
            thisVar.setState({
                data: jsonData
            })
            // console.log(jsonData[1]["body"]);
            // AsyncStorage.setItem('testjson',jsonResponse);
        // do something with jsonResponse
            // data = JSON.stringify(jsonData);
            // alert(thisVar.state.data);
            // alert(thisVar.props.idDriver+' 2nd');
            // AsyncStorage.setItem('testjson',JSON.stringify(jsonData));
        };
        // jsonData = this.jsonData;
        // this.fetchJSON();
        req.send(null);
    }

    renderOrderList(){
        if(this.state.refreshList === false && this.state.data!=null){
            return(
                <View style={{ marginBottom:15 }}>
                {   
                    this.state.data.map(( item, key ) =>
                    (   
                        
                        // this.getStatusOrder(item.order[0]["id_transaction"]) === 'true'
                    // item.status == 'true'
                    // ?
                    <View key={key} ref={key} style={{ height:320, marginBottom:60 }}>
                        {/* <Text>{Object.keys(this.state.count).length}</Text> */}
                        {/* {this.getStatusOrder(item.order[0]["id_transaction"])} */}
                        <View style={{ marginBottom:15, borderColor:'#ccc', borderWidth:0.5, borderRadius:5, padding:15,
                                        backgroundColor:'white', shadowColor: "#000",
                                        shadowOffset: {width: 0,height: 2,},
                                        shadowOpacity: 0.25,shadowRadius: 2.25,
                                        elevation: 5, }}>
                            <View style={{ borderRadius:5, marginLeft:15, marginRight:15, position:'absolute', zIndex:1, top:15, left:10 }}>
                                <TouchableOpacity 
                                    onPress={() => alert('Pressed Track')} 
                                    style={{ zIndex:1, backgroundColor:'purple', borderRadius:5, borderTopRightRadius:5, width:'100%', marginTop:10, paddingLeft:15, paddingRight:15, paddingTop:5, paddingBottom:5 }}>
                                    <Text style={{ fontFamily:'Asap-Bold', color:'white' }}>Track</Text>
                                </TouchableOpacity>
                            </View>
                            <TouchableOpacity onPress={() => alert('Pressed Maps')} style={{ width:'100%', height:150, marginBottom:5 }}>
                                <View style={{position: 'absolute', top: 0, right: 0, bottom: 0, left: 0}} />
                                <MapView
                                    style={styles.map }
                                    provider={PROVIDER_GOOGLE}
                                    scrollEnabled={false}
                                    initialRegion={{
                                    latitude: -27.3810232,
                                    longitude: 152.4326816,
                                    latitudeDelta: 1,
                                    longitudeDelta: 1,
                                    }}
                                />
                            </TouchableOpacity>
                            <Text style={[styles.fontFamilyRegular, { fontSize:14, color:'red', marginBottom:5 }]}>{item.order[0]["fullname"]}</Text>
                            <Text style={[styles.fontFamilyRegular, { fontSize:14, color:'red', marginBottom:20 }]}>+61-123-456-789</Text>
                            <View style={{ marginBottom:40, height:25 }}>
                                {
                                    item.order_items.map((item2, key) =>(
                                    <View style={{ flexDirection:'row', marginBottom:10 }}>
                                        <Text style={[styles.fontFamilyRegular, { fontSize:15, flex:0.1 }]}>{item2[0]["qty"]}</Text>
                                        <Text style={[styles.fontFamilyRegular, { fontSize:15, flex:0.7 }]}>{item2[0]["name"]}</Text>
                                    </View>
                                    ))
                                }
                                {/* <Text style={[styles.fontFamilyRegular, { fontSize:15 }]}>1x SeaBox Prawn</Text> */}
                                {/* <Text style={[styles.fontFamilyRegular, { fontSize:15 }]}>1x Dimsim Seafood</Text> */}
                            </View>
                            <View style={{ flexDirection:'row' }}>
                                <Text style={[styles.fontFamilyRegular, { color:'blue', fontWeight:'700' }]}>Destination:</Text>
                                <Text style={[styles.fw400, styles.fontFamilyRegular, {paddingLeft:5, paddingRight:5}]}>{item.order[0]["recipient_address"]}</Text>
                                {/* <Text>{key+1}</Text> */}
                            </View>
                            <View>
                                {
                                    this.state.refresh
                                }
                                <ButtonApproveDriver navigation={this.props.navigation} orderId={item.order[0]["id_transaction"]} statusOrder={item.order[0]["status"]} onDone={this.onDone.bind(this, item.order[0]["id_transaction"])} onCancel={this.onCancel.bind(this, item.order[0]["id_transaction"])} onRefresh={this.onChange.bind(this, item.order[0]["id_transaction"])} onProcess={this.onProcess.bind(this, item.order[0]["id_transaction"])}
                                // whileDone={this.deleteOrder.bind(this, item.order[0]["id_transaction"])}
                                />
                            </View>
                        </View>
                    </View>
                    // :
                    // null
                    ))
                }
            </View>
            )
        } else if(this.state.refreshList === true) {
            return(
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <View style={{ backgroundColor:'white', borderWidth:1, borderColor:'#ccc', 
                        alignSelf:'center', padding:20, shadowColor: "#000",
                        shadowOffset: {width: 0,height: 2,},
                        shadowOpacity: 0.25,shadowRadius: 3.84,elevation: 5, top:'100%'}}>
                        <ActivityIndicator size="large" color="black" />
                    </View>
                </View>
            );
        } else if(this.state.data === null){
            return(
                // <View>
                //     <TestingEachOrderList navigation={this.props.navigation}/>
                //     <TestingEachOrderList navigation={this.props.navigation}/>
                //     <TestingEachOrderList navigation={this.props.navigation}/>
                //     <TestingEachOrderList navigation={this.props.navigation}/>
                // </View>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={[styles.fontFamilyRegular, { top:'750%', fontSize:15 }]}>Your order will be shown here</Text>
                </View>
            );
        } else {
            
        }
    }

    render(){
        return(
            <View style={{ flex:0.8 }}>
                <View style={{ flexDirection:'row', flex:0.05, marginTop:10 , marginBottom:10}}>
                    <View style={{ flex:0.5}}>
                        {
                            this.state.data != null
                            ?
                            <Text style={styles.fontFamilyRegular}>You have { Object.keys(this.state.data).length ? <Text style={{ color:'red' }}>{Object.keys(this.state.data).length}</Text> : <Text style={{ color:'red' }}>0</Text> } order</Text>
                            :
                            <Text style={styles.fontFamilyRegular}>You have <Text style={{ color:'red' }}>no</Text> order</Text>
                        }
                    </View>
                </View>
                <View style={{ flex:1, marginLeft:-5, marginRight:-5 }}>
                    <ScrollView style={{ width:'100%', marginBottom:30, height:'75%'}} 
                            refreshControl={ <RefreshControl refreshing={this.state.refreshScroll} onRefresh={this.onRefreshScroll.bind(this)}/> }
                    >
                        {/* <Text>{this.state.data.order[0]["id_transaction"]}</Text> */}
                        {this.renderOrderList()}
                    </ScrollView>
                </View>
            </View>
            // <ScrollView style={{ width:'100%', marginBottom:30, height:'75%'}} 
            //         refreshControl={ <RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefresh.bind(this)}/> }
            // >
            //     {/* <Text>{this.state.data.order[0]["id_transaction"]}</Text> */}
            //     {this.renderOrderList()}
            //     <Text>AAA</Text>
            // </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    fontFamilyRegular:{
        fontFamily:'Asap-Regular'
    },
    fw400:{
        fontWeight:'400'
    },
    map: {
        ...StyleSheet.absoluteFillObject,
        height:150,
        zIndex:-1,
    },
});

export default withNavigation(ScrollViewOrderActive);