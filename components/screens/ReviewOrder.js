import React, { Component } from 'react';
import { View, Image, Text, Dimensions, StyleSheet } from 'react-native';
import { withNavigation, SafeAreaView } from 'react-navigation';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faChevronUp, faChevronDown, faChevronCircleLeft, faChevronCircleRight, faTrash, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { ScrollView } from 'react-native-gesture-handler';

class ReviewOrder extends React.Component{
    render(){
        return(
            <SafeAreaView>
                <ScrollView style={{  }}>
                    <View style={{ width:'100%', paddingBottom:10 }}>
                        <Text>Review and place order</Text>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}