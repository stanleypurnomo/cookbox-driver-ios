import React, { Component } from 'react';
import { View, Image, Text, Dimensions, StyleSheet, Button, AsyncStorage } from 'react-native';
import { withNavigation, SafeAreaView } from 'react-navigation';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { Rating, AirbnbRating } from 'react-native-elements';
import { faChevronUp, faChevronDown, faChevronCircleLeft, faChevronCircleRight, faTrash, faTrashAlt, faUserCircle } from '@fortawesome/free-solid-svg-icons';
import { ScrollView, TouchableHighlight, TouchableOpacity } from 'react-native-gesture-handler';

class AchievementDriver extends React.Component{
    constructor(){
        super();
        this.state = {
            fullname:'',
            id:'',
            visible:false,
        }
    }
    componentDidMount(){
        this.fetchFullname();
        this.fetchId();
    }

    fetchFullname = async () => {
        try {
            const fullnameVal = await AsyncStorage.getItem('fullname');
            console.log(fullnameVal);
            this.setState({
                fullname:fullnameVal
            });
        } catch (error) {
            console.log(error);
        }
    }

    fetchId = async () => {
        try {
            const idVal = await AsyncStorage.getItem('id');
            console.log(idVal);
            this.setState({
                id:idVal
            });
        } catch (error) {
            console.log(error);
        }
    }

    render(){
        if(this.state.visible === true){
            return(
                <View style={{ flexDirection:'column', flex:1, paddingTop:25, paddingLeft:15, paddingRight:15 }}>
                    <Text style={[styles.fontFamilyRegular, { textAlign:'center', fontSize:18, fontWeight:'600', paddingBottom:50 }]}>ACHIEVEMENT</Text>
                    <View style={{ padding:15, backgroundColor:'white', shadowColor: "#000",
                                shadowOffset: {width: 0,height: 2,},
                                shadowOpacity: 0.25,shadowRadius: 2.25,
                                elevation: 5,}}>
                        <View style={{ position:'relative', top:-50, alignSelf:'center', maxHeight:40 }}>
                            <FontAwesomeIcon icon={faUserCircle} size="80x"/>
                        </View>
                        {/* <Image source={require('../images/logo-banner/')}/> */}
                        <Text style={[styles.fontFamilyRegular, { textAlign:'center', fontSize:18, fontWeight:'600' }]}>Name Driver</Text>
                        <Text style={[styles.fontFamilyRegular, { textAlign:'center', color:'grey', fontSize:15 }]}>{this.state.fullname}</Text>
                        <Rating imageSize={25} readonly startingValue='3.75' style={{ marginTop:10 }}/>
                        <View style={{ alignItems:'flex-start', paddingLeft:5, paddingRight:5, paddingTop:20, paddingBottom:20 }}>
                            <View style={{ flexDirection:'row' }}>
                                <View style={{ flex:0.6 }}>
                                    <Text style={[styles.driverDetailsFont, styles.fontFamilyRegular]}>Total order today</Text>
                                </View>
                                <View style={{ flex:0.0325 }}>
                                    <Text>:</Text>
                                </View>
                                <View style={{ flex:0.3 }}>
                                    <Text style={styles.fontFamilyRegular}>21</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection:'row' }}>
                                <View style={{ flex:0.6 }}>
                                        <Text style={[styles.driverDetailsFont, styles.fontFamilyRegular]}>Total order one week</Text>
                                </View>
                                <View style={{ flex:0.0325}}>
                                    <Text>:</Text>
                                </View>
                                <View style={{ flex:0.3 }}>
                                    <Text style={styles.fontFamilyRegular}>147</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection:'row' }}>
                                <View style={{ flex:0.6 }}>
                                    <Text style={[styles.driverDetailsFont, styles.fontFamilyRegular]}>Total order a month</Text>
                                </View>
                                <View style={{ flex:0.0325}}>
                                    <Text>:</Text>
                                </View>
                                <View style={{ flex:0.3 }}>
                                    <Text style={styles.fontFamilyRegular}>567</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection:'row' }}>
                                <View style={{ flex:0.6 }}>
                                    <Text style={[styles.driverDetailsFont, styles.fontFamilyRegular]}>Order canceled</Text>
                                </View>
                                <View style={{ flex:0.0325}}>
                                    <Text>:</Text>
                                </View>
                                <View style={{ flex:0.3 }}>
                                    <Text style={[styles.fontFamilyRegular, { fontWeight:'600', color:'#ff0018', lineHeight:18 }]}>21</Text>
                                </View>
                            </View>
                            <View style={{ flexDirection:'row' }}>
                                <View style={{ flex:0.6 }}>
                                    <Text style={[styles.driverDetailsFont, styles.fontFamilyRegular]}>Performance</Text>
                                </View>
                                <View style={{ flex:0.0325}}>
                                    <Text>:</Text>
                                </View>
                                <View style={{ flex:0.3 }}>
                                    <Text style={[styles.fontFamilyRegular, { fontWeight:'600', color:'#4FCA55', lineHeight:18 }]}>Good</Text>
                                </View>
                            </View>
                        </View>
                        <TouchableOpacity style={{ borderRadius:5, width:'100%', backgroundColor:'#4FCA55', padding:10, alignItems:'center' }}>
                            <Text style={[styles.fontFamilyRegular, { color:'white',fontSize:18, fontWeight:'600' }]}>Complaint</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            );
        } else {
            return(
                <View style={{ flexDirection:'column', flex:1, alignItems:'center', justifyContent:'center' }}>
                    <Text style={[styles.fontFamilyRegular, { fontSize:20 }]}>Coming soon</Text>
                </View>
            )
        }
    }
}

const styles = StyleSheet.create({
    driverDetailsFont:{
        fontSize:16,
    },
    fontFamilyRegular:{
        fontFamily:'Asap-Regular'
    },
    fw400:{
        fontWeight:'400'
    }
})

export default withNavigation(AchievementDriver);