import React, { Component } from 'react';
import { View, Image, Text, Dimensions, StyleSheet, Button, StatusBar, BackHandler, RefreshControl, Alert, AsyncStorage, ActivityIndicator } from 'react-native';
import { withNavigation, SafeAreaView } from 'react-navigation';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faChevronUp, faChevronDown, faChevronCircleLeft, faChevronCircleRight, faTrash, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { ScrollView, TouchableHighlight, TouchableOpacity } from 'react-native-gesture-handler';
import ActiveOrderList from '../screens/ActiveOrderListDriver';
import InactiveOrderList from '../screens/InactiveOrderListDriver';
import DropdownAlert from 'react-native-dropdownalert';

var isLogin = 'true';

class HomeAppDriver extends React.Component{
    static navigationOption = ({ navigation }) => {
        // console.log(navigation.getParam('id',null))
        return {
            title: navigation.getParam('name', null)
        }
    }
    constructor(props){
        super();
        this.state = {
            isActive: true,
            token: '',
            loading:false,
            id:null,
            status:null
        }
        // console.log(props.navigation.state.params.id+'aidiwaj');
        // console.log(props.routes.params+' ABC');
        // console.log(props);
    }

    componentDidMount(){
        this.getToken();
        this.getStatusDriver();
    }

    changeToInactive(){
        this.setState({loading:true});
        var thisState = this;
        // alert("Can't turn off");
        // setTimeout(function(){
        //     thisState.setState({isActive: true, loading: false});
        //     thisState.dropDownAlertRef.alertWithType('error', "Can't turn off");
        // },1500);
        setTimeout(function(){thisState.setState({isActive: false, loading: false});},1500);
    }

    testLoading(){
        this.setState({
            loading: true
        });
        var thisState = this;
        setTimeout(function(){thisState.setState({loading: false});},1500);
    }

    changeToActive(){
        this.setState({
            loading:true
        });
        var thisState = this;
        setTimeout(function(){thisState.setState({isActive: true, loading: false});},1500);
    }

    getStatusDriver(){
        var stat    = new XMLHttpRequest();
        var url     = 'http://seafoodau.herokuapp.com/api/v1/order/position/'+this.state.id;
        stat.responseType = 'json';
        stat.open('GET', url, true);
        var ThisState = this;
        // this.setState({
        //     loading: true,
        // });
        stat.onload  = function(This) {
            // console.log('refreshing '+ThisState.state.loading);
            var jsonResponse = stat.response;
            console.log(jsonResponse.status);
            if(jsonResponse.status === 'success'){
                ThisState.setState({
                    status: jsonResponse.position
                })
                // if(ThisState.state.status === 'on restaurant'){
                //     ThisState.
                // }
                // alert(ThisState.state.status);
            }
        };
        stat.send(null)
    }

    async getToken(){
        try{
            const token = await AsyncStorage.getItem('token');
            const message = await AsyncStorage.getItem('welcomeMessage');
            const fullname = await AsyncStorage.getItem('fullname');
            console.log(token);
            this.setState({
                token: token,
            });
            if(message === 'true'){
                this.dropDownAlertRef.alertWithType('success', "Welcome, "+fullname+" abc def ghi jkl.", "Have a nice day...");
                AsyncStorage.setItem('welcomeMessage','false');
            }
            console.log(this.state.token);
            // alert(this.state.token);
            this.getIDFromToken();
        } catch(error){
            console.log(error);
        }
    }

    getIDFromToken(){
        var request = {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Authorization': this.state.token
            },
        };

        fetch('http://seafoodau.herokuapp.com/api/v1/auth/data', request)
        .then((response) => response.json())
        .then((responseJson)=>
        {
            console.log(responseJson);
            if(responseJson.status == '200'){
                this.setState({
                    id:responseJson.data.id,
                })
                // alert(this.state.token+' ABC');
                console.log(this.state.id+' abcdef');
            } else {
                console.log('Error getting data');
                // alert("Wrong Username or Password");
                // this.setState({
                //     isLoading: false,
                // });
            }
        })
        .catch((error)=>{
            console.error(error);
        });
    }

    // getID(props) {
    //     let id = false;
    //     if(props.navigation.state.params) {
    //         id = props.navigation.state.params.id;
    //     }
    //     console.log(id+' aiwjdiajdi');
    // }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }
    
    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }
    
    handleBackButton = () => {
        if (this.props.isFocused) {
          Alert.alert(
            'Exit App',
            'Exiting the application?',
            [
              {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel'
              },
              {
                text: 'OK',
                onPress: () => BackHandler.exitApp()
              }
            ],
            {
              cancelable: false
            }
          );
          return true;
        }
    };

    wait(timeout) {
        return new Promise(resolve => {
          setTimeout(resolve, timeout);
        });
    }

    render(){
        // const { navigation } = this.props;
        // const id = navigation.getParam('id') || '';
        // const { id } = this.props.navigation.params.id;
        // const { id } = route.params.name;
        // const [refreshing, setRefreshing] = React.useState(false);

        // const onRefresh = React.useCallback(() => {
        //     setRefreshing(true);

        //     wait(2000).then(() => setRefreshing(false));
        // }, [refreshing]);
        if(this.state.loading==false){
            return(
                <View style={{ backgroundColor:'white', flexDirection:'column', flex:1, paddingTop:0, paddingLeft:15, paddingRight:15 }}>
                        {/* <StatusBar barStyle="dark-content" backgroundColor="black"/> */}
                    <ScrollView style={{ backgroundColor:'white', flex:0.1, height:50, paddingTop:0, position:'relative', height:10}}
                                refreshControl={ <RefreshControl refreshing={this.state.loading}/> }>
                        <View style={{ alignSelf:'center' }}>
                            <Image source={require('../images/logo-banner/Transparentbackgroundnew.png')} 
                                style={{ width:200, height:150, resizeMode:'contain', marginBottom:15 }}
                            />
                        </View>
                    </ScrollView>
                    <DropdownAlert ref={ref => this.dropDownAlertRef = ref} errorColor="#f54f29"/>
                    <View style={{ flex:2.5 }}>
                        {
                            this.state.isActive === true
                            ?
                                <ActiveOrderList navigation={this.props.navigation} getStatusOTW={this.state.status} driverID={this.state.id} statusDriver={true} checkStatus={this.testLoading.bind(this)} onSwitch={this.changeToInactive.bind(this)}/>
                            : this.state.isActive == false
                            ?
                                <InactiveOrderList navigation={this.props.navigation} statusDriver={false} onSwitch={this.changeToActive.bind(this)}/>
                            : null
                        }
                    </View>
                </View>
            );
        } else {
            return(
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <View style={{ backgroundColor:'white', borderWidth:1, borderColor:'#ccc', 
                        alignSelf:'center', padding:20, shadowColor: "#000",
                        shadowOffset: {width: 0,height: 2,},
                        shadowOpacity: 0.25,shadowRadius: 3.84,elevation: 5}}>
                        <ActivityIndicator size="large" color="black" />
                    </View>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    fontFamilyRegular:{
        fontFamily:'Asap-Regular'
    },
    fw400:{
        fontWeight:'400'
    }
});

export default withNavigation(HomeAppDriver);