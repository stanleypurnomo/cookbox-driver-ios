import React, { Component } from 'react';
import { View, Image, Text, Dimensions, StyleSheet, Linking } from 'react-native';
import { withNavigation, SafeAreaView } from 'react-navigation';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import DriverTab from '../../App';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { NavigationContainer } from '@react-navigation/native';

class InitScreen extends Component{
    render(){
        return(
            <DriverTab independent={true}/>
        )
    }
}

export default InitScreen;