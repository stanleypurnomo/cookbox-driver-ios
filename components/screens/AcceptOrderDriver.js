import React, { Component } from 'react';
import { View, Image, Text, Dimensions, StyleSheet, Platform } from 'react-native';
import { withNavigation, SafeAreaView } from 'react-navigation';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';

class AcceptOrderDriver extends Component{
    constructor() {
        super();
        this.state = {
            statusBarHeight: getStatusBarHeight(),
            tabBarVisible: false,
        };
    }
    componentDidMount(){
        this.props.navigation.dangerouslyGetParent().setOptions({tabBarVisible: false});
    }
    componentWillUnmount(){
        this.props.navigation.dangerouslyGetParent().setOptions({tabBarVisible: true});
    }
    static navigationOptions = {
        tabBarVisible: false
    }
    render(){
        return(
            // <SafeAreaView>
                // <View style={{ backgroundColor:'white' }}>
                <ScrollView style={{ height:hp('100%'), backgroundColor:'#2c2c2c' }}>
                    <View style={{ flexDirection:'row', marginTop:this.state.statusBarHeight }}>
                        <View style={{ flex:0.2 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{ paddingLeft:15, paddingRight:15, paddingTop:15 }}>
                                {/* <Text style={{ fontSize:28 }}>X</Text> */}
                                { Platform.OS === 'ios'
                                ?
                                    <FontAwesomeIcon icon={faTimesCircle} size="35x" color="white"/>
                                :
                                    <FontAwesomeIcon icon={faTimesCircle} size="35" color="white"/>
                                }
                            </TouchableOpacity>
                        </View>
                        <View style={{ flex:0.6, alignItems:'center', paddingTop:25 }}>
                            <Text style={{ color:'white' }}>FIXED PRICE</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection:'row' }}>
                        <View style={{ flex:1, alignItems:'center', paddingTop:10 }}>
                            <Text style={{ color:'white', fontSize:28, fontWeight:'500' }}>IDR 69,696.00</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection:'row' }}>
                        <View style={{ flex:1, alignItems:'center', paddingTop:15 }}>
                            <Text style={{ fontSize:18, color:'white' }}>COOKBOX</Text>
                        </View>
                    </View>
                    <View style={{ padding:15 }}>
                        <View style={{ padding:15, backgroundColor:'#ccc',borderTopStartRadius:10, borderTopEndRadius:10 }}>
                            <View style={{ flex:1, flexDirection:'row' }}>
                                <View style={{ flex:0.5 }}>
                                    <Text>0.8 KM</Text>
                                </View>
                                <View style={{ flex:0.5, alignItems:'flex-end' }}>
                                    <Text>CASH</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ padding:15, backgroundColor:'white' }}>
                            <Text style={{ fontSize:22, textAlign:'center', paddingBottom:10 }}>RESTAURANT A</Text>
                            <Text style={{ fontSize:16, textAlign:'center' }}>Brisbane, QLD, Australia</Text>
                        </View>
                        <View style={{ padding:15, backgroundColor:'#ccc' }}>
                            <Text style={{ fontSize:22, textAlign:'center', paddingBottom:10 }}>DESTINATION A</Text>
                            <Text style={{ fontSize:16, textAlign:'center' }}>Brisbane, QLD, Australia</Text>
                        </View>
                        <View style={{ padding:15, backgroundColor:'white', borderBottomStartRadius:10, borderBottomEndRadius:10, marginBottom:25 }}>
                            <View style={{ flex:1, alignSelf:'center', paddingBottom:20 }}>
                                <Text>Order Summary</Text>
                            </View>
                            <View style={{ flexDirection:'row', flex:1 }}>
                                <View style={{ flex:0.5 }}>
                                    <Text style={{ fontSize:18 }}>3 Items</Text>
                                </View>
                                <View style={{ flex:0.5, alignItems:'flex-end' }}>
                                    <Text style={{ fontSize:18 }}>IDR 69,696.00</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ flex:1 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('Go to Restaurant')} style={{ padding:15, backgroundColor:'#ff0018', alignItems:'center' }}>
                                <Text style={{ color:'white', fontWeight:'500', fontSize:21 }}>Accept Order</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
                // </View>
            // </SafeAreaView>
        );
    }
}

export default withNavigation(AcceptOrderDriver);