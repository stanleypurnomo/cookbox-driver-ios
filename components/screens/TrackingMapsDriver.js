import React, { Component } from 'react';
import { View } from 'react-native';
import { withNavigation } from 'react-navigation';
import TrackingMaps from './TrackingMaps';

class TrackingMapsDriver extends React.Component{
    render(){
        return(
            <View>
                <TrackingMaps/>
            </View>
        )
    }
}

export default withNavigation(TrackingMapsDriver);