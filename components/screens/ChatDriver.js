import React, { Component } from 'react';
import { View, Image, Text, Dimensions, StyleSheet, Button, StatusBar, BackHandler, KeyboardAvoidingView } from 'react-native';
import { withNavigation, SafeAreaView } from 'react-navigation';
import { WebView } from 'react-native-webview';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { getStatusBarHeight } from 'react-native-status-bar-height';

class ChatDriver extends Component{
    constructor() {
        super();
        this.state = {
          statusBarHeight: getStatusBarHeight(),
          token: '',
          id:null,
        };
    }

    async getToken(){
      try{
          const token = await AsyncStorage.getItem('token');
          console.log(token);
          this.setState({
              token: token,
          });
          console.log(this.state.token);
          this.getIDFromToken();
      } catch(error){
          console.log(error);
      }
    }

    getIDFromToken(){
        var request = {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Authorization': this.state.token
            },
        };

        fetch('http://seafoodau.herokuapp.com/api/v1/auth/data', request)
        .then((response) => response.json())
        .then((responseJson)=>
        {
            console.log(responseJson);
            if(responseJson.status == '200'){
                this.setState({
                    id:responseJson.data.id,
                })
                console.log(this.state.id+' abc');
            } else {
                console.log('Error getting data');
                // alert("Wrong Username or Password");
                // this.setState({
                //     isLoading: false,
                // });
            }
        })
        .catch((error)=>{
            console.error(error);
        });
    }

    render(){
      // const { params } = this.props.navigation.state;
      // const id = params ? params.id : null;
        return(
            // <SafeAreaView>
              // {/* <StatusBar barStyle="light-content" style={{ back }}/> */}
              <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : null}>
                <View style={{ width:'100%', height:hp('100%'), paddingBottom:50 }}>
                  {/* <View style={{ height:this.state.statusBarHeight, backgroundColor:'#990018' }}></View> */}
                  <WebView  useWebKit={true}
                            bounces={true} 
                            // javaScriptEnabled={true} 
                            // injectedJavaScript={`setInterval(function() { window.location.reload();}, 10000);`} 
                            // source={{ uri: 'http://rizkyserra.000webhostapp.com/' }} 
                            source={{ uri: 'https://seafoodau.herokuapp.com/support/react/index/'+this.state.id }} 
                            style={{ flex:1 }} 
                  />
                </View>
              </KeyboardAvoidingView>
            // </SafeAreaView>
        );
    }
}

export default withNavigation(ChatDriver);