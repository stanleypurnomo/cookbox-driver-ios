import React, { Component } from 'react';
import { View, Button } from 'react-native';

class TestButton extends React.Component{
    render(){
        return(
            <View>
                <Button title="Test" onPress={()=>alert('Test')}/>
            </View>
        )
    }
}

export default TestButton;