import React, { Component } from 'react';
import { View, Image, Text, Dimensions, StyleSheet, Linking } from 'react-native';
import { withNavigation, SafeAreaView } from 'react-navigation';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faTimesCircle, faLocationArrow, faPhoneAlt, faUserCircle, faCommentDots } from '@fortawesome/free-solid-svg-icons';

class OrderInProgressDriver extends Component{
    constructor() {
        const windowHeight = Dimensions.get('window').height;
        super();
        this.state = {
            statusBarHeight: getStatusBarHeight(),
            tabBarVisible: false,
            confirmed: false,
            windowHeight: windowHeight,
        };
    }
    componentDidMount(){
        this.props.navigation.dangerouslyGetParent().setOptions({tabBarVisible: false});
        // this.props.navigation.setOptions({gesturesEnabled:false});
    }
    componentWillUnmount(){
        if(this.state.confirmed === false){
            this.props.navigation.dangerouslyGetParent().setOptions({tabBarVisible: false});
        } else {
            this.props.navigation.dangerouslyGetParent().setOptions({tabBarVisible: true});
        }
    }
    static navigationOptions = {
        tabBarVisible: false
    }
    render(){
        return(
            // <SafeAreaView>
                // <View style={{ backgroundColor:'white' }}>
                <ScrollView style={{ backgroundColor:'#2c2c2c' }}>
                    <View style={{ flexDirection:'row' }}>
                        <View style={{ flex:0.1, alignItems:'center', top:15 }}>
                            <TouchableOpacity style={{  }}>
                                <View style={{ backgroundColor:'#1c1c1c', borderTopEndRadius:30, borderBottomEndRadius:30, paddingLeft:50, marginRight:20, paddingTop:20, paddingBottom:20, width:'100%' }}>
                                    {/* <FontAwesomeIcon icon={faLocationArrow} size="15x" color="white"/> */}
                                    { Platform.OS === 'ios'
                                    ?
                                        <FontAwesomeIcon icon={faLocationArrow} size="15x" color="white"/>
                                    :
                                        <FontAwesomeIcon icon={faLocationArrow} size="15" color="white"/>
                                    }
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={{ flex:0.8, alignItems:'center', paddingTop:20 }}>
                            <Text style={{ color:'white', fontSize:28, fontWeight:'500' }}>RESTAURANT A</Text>
                        </View>
                        <View style={{ flex:0.1, alignItems:'center', top:15 }}>
                            <TouchableOpacity>
                                <View style={{ backgroundColor:'#1c1c1c', borderTopStartRadius:30, borderBottomStartRadius:30, paddingLeft:30, paddingRight:50, paddingTop:20, paddingBottom:20 }}>
                                    { Platform.OS === 'ios'
                                    ?
                                        <FontAwesomeIcon icon={faPhoneAlt} size="15x" color="white"/>
                                    :
                                        <FontAwesomeIcon icon={faPhoneAlt} size="15" color="white"/>
                                    }
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ flexDirection:'row' }}>
                        <View style={{ flex:1, alignItems:'center', paddingTop:15 }}>
                            <Text style={{ fontSize:18, color:'white' }}>Brisbane, QLD, Australia</Text>
                        </View>
                    </View>
                    <View style={{ padding:15 }}>
                        <View style={{ padding:15, backgroundColor:'#ccc',borderTopStartRadius:10, borderTopEndRadius:10 }}>
                            <View style={{ flex:1, flexDirection:'row' }}>
                                <View style={{ flex:1, alignItems:'center' }}>
                                    <Text>Order Summary</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ padding:10, backgroundColor:'white', flexDirection:'row' }}>
                            <View style={{ flex:1 }}>
                                <View style={{ flex:0.5, flexDirection:'column' }}>
                                    <Text style={{ fontSize:16, fontWeight:'300', textAlign:'left', paddingBottom:10 }}>1 Seafood Box A</Text>
                                    <Text style={{ fontSize:16, fontWeight:'300', textAlign:'left' }}>( Small )</Text>
                                </View>
                            </View>
                            <Text style={{ fontSize:16, fontWeight:'300', textAlign:'right', flex:0.5 }}>IDR 50,000.00</Text>
                        </View>
                        <View style={{ padding:10, backgroundColor:'white', flexDirection:'row' }}>
                            <View style={{ flex:1 }}>
                                <View style={{ flex:0.5, flexDirection:'column' }}>
                                    <Text style={{ fontSize:16, fontWeight:'300', textAlign:'left', paddingBottom:10 }}>1 Seafood Box B</Text>
                                    <Text style={{ fontSize:16, fontWeight:'300', textAlign:'left' }}>( Small )</Text>
                                </View>
                            </View>
                            <Text style={{ fontSize:16, fontWeight:'300', textAlign:'right', flex:0.5 }}>IDR 50,000.00</Text>
                        </View>
                        <View style={{ padding:10, backgroundColor:'white', flexDirection:'row' }}>
                            <View style={{ flex:1 }}>
                                <View style={{ flex:0.5, flexDirection:'column' }}>
                                    <Text style={{ fontSize:16, fontWeight:'300', textAlign:'left', paddingBottom:10 }}>1 Seafood Box C</Text>
                                    <Text style={{ fontSize:16, fontWeight:'300', textAlign:'left' }}>( Small )</Text>
                                </View>
                            </View>
                            <Text style={{ fontSize:16, fontWeight:'300', textAlign:'right', flex:0.5 }}>IDR 50,000.00</Text>
                        </View>
                        {/* <View style={{ padding:15, backgroundColor:'#ccc' }}>
                            <Text style={{ fontSize:22, textAlign:'center', paddingBottom:10 }}>DESTINATION A</Text>
                            <Text style={{ fontSize:16, textAlign:'center' }}>Brisbane, QLD, Australia</Text>
                        </View> */}
                        <View style={{ padding:15, backgroundColor:'white', borderBottomStartRadius:10, borderBottomEndRadius:10, marginBottom:25 }}>
                            <View style={{ flex:1, alignSelf:'center', paddingBottom:20 }}>
                                <Text>Order Summary Total</Text>
                            </View>
                            <View style={{ flexDirection:'row', flex:1 }}>
                                <View style={{ flex:0.5 }}>
                                    <Text style={{ fontSize:18 }}>3 Items</Text>
                                </View>
                                <View style={{ flex:0.5, alignItems:'flex-end' }}>
                                    <Text style={{ fontSize:18 }}>IDR 69,696.00</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ flexDirection:'row', paddingBottom:10 }}>
                            <View style={{ flex:0.3, alignItems:'center' }}>
                                {/* <Text>AAA</Text> */}
                                <TouchableOpacity style={{ marginBottom:5 }}>
                                    { Platform.OS === 'ios'
                                    ?
                                        <FontAwesomeIcon icon={faUserCircle} size="62.5x" color="white"/>
                                    :
                                        <FontAwesomeIcon icon={faUserCircle} size="62.5" color="white"/>
                                    }
                                </TouchableOpacity>
                                <Text style={{ fontSize:14, color:'white', fontWeight:'600' }}>User One</Text>
                            </View>
                            <View style={{ flex:0.4, alignItems:'center' }}>
                                {/* <Text>BBB</Text> */}
                                <TouchableOpacity onPress={()=>{Linking.openURL('tel:0123456789');}} style={{ padding:21, borderRadius:50, backgroundColor:'#5c5c5c', marginBottom:5 }}>
                                    { Platform.OS === 'ios'
                                    ?
                                        <FontAwesomeIcon icon={faPhoneAlt} size="20x" color="white"/>
                                    :
                                        <FontAwesomeIcon icon={faPhoneAlt} size="20" color="white"/>
                                    }
                                </TouchableOpacity>
                                <Text style={{ fontSize:14, color:'white', fontWeight:'600' }}>Call</Text>
                            </View>
                            <View style={{ flex:0.3, alignItems:'center' }}>
                                <TouchableOpacity onPress={()=>{Linking.openURL('sms:0123456789?body=Hello world')}} style={{ padding:21, borderRadius:50, backgroundColor:'#5c5c5c', marginBottom:5 }}>
                                    { Platform.OS === 'ios'
                                    ?
                                        <FontAwesomeIcon icon={faCommentDots} size="20x" color="white"/>
                                    :
                                        <FontAwesomeIcon icon={faCommentDots} size="20" color="white"/>
                                    }
                                </TouchableOpacity>
                                <Text style={{ fontSize:14, color:'white', fontWeight:'600' }}>Chat</Text>
                            </View>
                        </View>
                        <View style={{ flex:1 }}>
                            <TouchableOpacity style={{ padding:15, backgroundColor:'#ff0018', alignItems:'center', borderRadius:5 }}>
                                <Text style={{ color:'white', fontWeight:'500', fontSize:21 }}>Order Placed</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
                // </View>
            // </SafeAreaView>
        );
    }
}

export default withNavigation(OrderInProgressDriver);