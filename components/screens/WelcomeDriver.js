import React, { Component } from 'react';
import { View, Image, Text, Dimensions, StyleSheet, Button, StatusBar, AsyncStorage } from 'react-native';
import { withNavigation, SafeAreaView } from 'react-navigation';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faChevronUp, faChevronDown, faChevronCircleLeft, faChevronCircleRight, faTrash, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { ScrollView, TouchableHighlight, TouchableOpacity } from 'react-native-gesture-handler';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { CommonActions } from '@react-navigation/native';
import DriverTab from '../../App';

var statusBarHeight = getStatusBarHeight();

class WelcomeDriver extends React.Component{
    render(){
        return(
            <View style={{ height:'100%', backgroundColor:'white' }}>
            <StatusBar barStyle="light-content"/>
            <View style={{ flexDirection:'column', flex:1 }}>
            <View style={{ flex:0.35 }}>
                <Image  source={require('../images/logo-banner/background1new.png')}
                        style={{ height:640, width:'100%', zIndex:1, resizeMode:'cover' }}/>
                {/* <View style={{ position:'absolute', zIndex:99 }}> */}
                {/* </View> */}
            </View>
            <View style={{ alignSelf:'center', position:'absolute', zIndex:9, top:statusBarHeight+20 }}>
                {/* <Text>AAA</Text> */}
                <View style={{ alignSelf:'center' }}>
                    <Image source={require('../images/logo-banner/Transparent_background_white.png')} 
                        style={{ width:200, height:150, resizeMode:'contain', marginBottom:25 }}
                    />
                </View>
            </View>
            <View style={{ flexDirection:'column', alignItems:'center', flex:0.5 }}>
                <Image source={require('../images/logo-banner/driver.png')}
                        style={{ width:200, height:200, marginBottom:50 }}/>
                <TouchableOpacity onPress={()=> this.props.navigation.navigate('Login')} style={{ marginBottom:25, borderRadius:5, backgroundColor:'#f54f29', width:200, paddingTop:10, paddingBottom:10, alignItems:'center' }}>
                    <Text style={{ fontSize:18, color:'white', fontFamily:'Asap-Bold' }}>Login</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate('Forgot Password')} style={{ borderColor:'#FF2D00', borderWidth:1, borderRadius:5, backgroundColor:'white', width:200, paddingTop:10, paddingBottom:10, alignItems:'center' }}>
                    <Text style={{ fontSize:18, color:'#ff0018', fontFamily:'Asap-Regular', fontWeight:'500' }}>Forgot Your Password?</Text>
                </TouchableOpacity>
            </View>
            </View>
        </View>
        );
    }
}

export default withNavigation(WelcomeDriver);