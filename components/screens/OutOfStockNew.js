import React, { Component } from 'react';
import { View, Button, Text, StatusBar, TouchableOpacity, StyleSheet, Keyboard, KeyboardAvoidingView, Platform, Dimensions } from 'react-native';
import { ScrollView, TextInput, TouchableWithoutFeedback } from 'react-native-gesture-handler';
import Textarea from 'react-native-textarea';
import { withNavigation } from 'react-navigation';
import TestButton from '../screens/TestButton';

const windowHeight = Dimensions.get('window').height;

const DismissKeyboard = ({ children }) => (
    <TouchableWithoutFeedback 
    onPress={() => Keyboard.dismiss()}>
        {children}
    </TouchableWithoutFeedback>
);
class OutOfStockNew extends React.Component{
    constructor(){
        super();
        this.state = {
            justifyContent: null
        };
    }

    componentWillMount(){
        this.setJustifyContent();
    }

    setJustifyContent(){
        var This = this;
        if(windowHeight>700){
            This.setState({
                justifyContent: 'flex-start'
            })
            // alert(this.state.justifyContent);
        } else {
            This.setState({
                justifyContent: 'center'
            })
            // alert(This.state.justifyContent);
        }
    }

    returnHome(){
        var req = new XMLHttpRequest();
        var url = 'http://seafoodau.herokuapp.com/api/v1/order/out_of_stock/'+id;
        req.responseType = 'json';
        req.open('POST', url, true);
        var ThisState = this;
        this.setState({
            refreshing: true,
        });
        // req.onload(this.setState({loading:true}));
        req.onload  = function() {
            var jsonResponse = req.response;
            console.log(jsonResponse.status);
            if(jsonResponse.status === 'success'){
                alert('Thanks');
                ThisState.props.navigation.reset({
                    index: 0,
                    routes: [
                        {   
                            name: 'Home',
                        }
                    ]
                });
            }
        };
    }
    render(){
        return(
            <View>
                <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : null}>
                    <View style={{ height:'100%', justifyContent:'center'}}>
                        <View style={{ flexDirection:'column', flex:0}}>
                            <View style={{ alignSelf:'center', position:'absolute', zIndex:9}}>
                            </View>
                        </View>
                        <View style={{ flexDirection:'row', marginBottom:5, paddingLeft:30, paddingRight:30 }}>
                            <View style={{ flex:0.2 }}>
                                <Text style={[styles.fontFamilyRegular, { fontSize:17 }]}>Status:</Text>
                            </View>
                            <View style={{ flex:0.2 }}>
                                <View style={{ paddingBottom:2.5, paddingTop:2.5, top:-2.5, backgroundColor:'#4FCA55', 
                                                alignItems:'center', borderRadius:5 }}>
                                    <Text style={[styles.fontFamilyRegular, { fontSize:16, color:'white', fontWeight:'600' }]}>Active</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ flexDirection:'row', marginBottom:20, paddingLeft:30, paddingRight:30 }}>
                            <View style={{ flex:1 }}>
                                <Text style={styles.fontFamilyRegular}>You have <Text style={{ color:'red' }}>4</Text> order</Text>
                            </View>
                        </View>
                        <View style={{ backgroundColor:'white', padding:15, marginLeft:30, marginRight:30, shadowColor: "#000",
                                shadowOffset: {width: 0,height: 2,},
                                shadowOpacity: 0.25,shadowRadius: 2.25,
                                elevation: 5, }}>
                        <DismissKeyboard>
                            <Text style={{ fontFamily:'Asap-Regular', textAlign:'center', marginBottom:30, color:'red' }}>ORDER CANCEL</Text>
                            <Text style={{ fontFamily:'Asap-Regular' }}>Rynaldo Wahyudi</Text>
                            <View style={{ marginBottom:15 }}>
                                <Text style={{ fontFamily:'Asap-Regular' }}>1 x SeaBox Prawn</Text>
                                <Text style={{ fontFamily:'Asap-Regular' }}>1 x Dimsim Seafood</Text>
                            </View>
                            <View style={{ marginBottom:10 }}>
                                <Text style={{ fontFamily:'Asap-Regular' }}>Destination: 333 Swann Road</Text>
                                <Text style={{ fontFamily:'Asap-Regular' }}>City: St Lucia</Text>
                                <Text style={{ fontFamily:'Asap-Regular' }}>State: Queensland</Text>
                                <Text style={{ fontFamily:'Asap-Regular' }}>Zip Code: 4067</Text>
                                <Text style={{ fontFamily:'Asap-Regular' }}>Phone: +61413226610</Text>
                            </View>
                            <View style={{ marginBottom:15, maxHeight:150 }}>
                                <Text style={{ marginBottom:5 }}>Note:</Text>
                                <ScrollView style={{ height:150 }}>
                                    <Textarea
                                        style={{ marginBottom:10, fontSize:16,fontFamily:'Asap-Regular', borderColor:'#ff0018', borderWidth:1, marginBottom:150, padding:10, borderRadius:5, height:100}}
                                        defaultValue=''
                                        placeholder={'Please fill the notes'}
                                        placeholderTextColor={'#c7c7c7'}
                                    />
                                </ScrollView>
                            </View>
                        </DismissKeyboard>
                        <View style={{ alignSelf:'center' }}>
                            {/* <TestButton/> */}
                            <TouchableOpacity onPress={()=>this.returnHome()} style={{ paddingLeft:15, paddingRight:15, paddingBottom:2.5, paddingTop:2.5, backgroundColor:'#4FCA55', alignItems:'center', borderRadius:5 }}>
                                <Text style={[{fontFamily:'Asap-Regular', fontSize:16, color:'white', fontWeight:'600' }]}>Done</Text>
                            </TouchableOpacity>
                        </View>
                        </View>
                    </View>
                </KeyboardAvoidingView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    fontFamilyRegular:{
        fontFamily:'Asap-Regular'
    },
    orderDetailsFont:{
        fontSize:16,
    },
})

export default withNavigation(OutOfStockNew);