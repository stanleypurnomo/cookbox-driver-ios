import React, { Component } from 'react';
import { View, Image, Text, Dimensions, StyleSheet, Button, AsyncStorage } from 'react-native';
import { withNavigation, SafeAreaView } from 'react-navigation';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { Rating, AirbnbRating } from 'react-native-elements';
import { faChevronUp, faChevronDown, faChevronCircleLeft, faChevronCircleRight, faTrash, faTrashAlt, faUserCircle } from '@fortawesome/free-solid-svg-icons';
import { ScrollView, TouchableHighlight, TouchableOpacity } from 'react-native-gesture-handler';

class ProfileDriver extends React.Component{
    constructor(){
        super();
        this.state = {
            fullname:''
        }
    }
    componentDidMount(){
        this.fetchFullname();
    }

    fetchFullname = async () => {
        try {
            const fullnameVal = await AsyncStorage.getItem('fullname');
            console.log(fullnameVal);
            this.setState({
                fullname:fullnameVal
            });
        } catch (error) {
            console.log(error);
        }
    }
    render(){
        return(
            <ScrollView style={{ flexDirection:'column', flex:1, paddingTop:25, paddingLeft:15, paddingRight:15, height:'100%' }}>
                <Text style={[styles.fontFamilyRegular, { textAlign:'center', fontSize:18, fontWeight:'600', paddingBottom:50 }]}>PROFILE</Text>
                <View style={{ padding:15, backgroundColor:'white', shadowColor: "#000",
                            shadowOffset: {width: 0,height: 2,},
                            shadowOpacity: 0.25,shadowRadius: 2.25,
                            elevation: 5,}}>
                    <View style={{ position:'relative', top:-50, alignSelf:'center', maxHeight:40 }}>
                        <FontAwesomeIcon icon={faUserCircle} size="80x"/>
                    </View>
                    {/* <Image source={require('../images/logo-banner/')}/> */}
                    <Text style={[styles.fontFamilyRegular, { textAlign:'center', fontSize:18, fontWeight:'600' }]}>Name Driver</Text>
                    <Text style={[styles.fontFamilyRegular, { textAlign:'center', color:'grey', fontSize:15 }]}>{this.state.fullname}</Text>
                    <View style={{ alignItems:'flex-start', padding:20 }}>
                        <Text style={[styles.driverDetailsFont,styles.fontFamilyRegular]}>Street Address:</Text>
                        <Text style={[styles.driverDetailsFont,styles.fontFamilyRegular]}>City:</Text>
                        <Text style={[styles.driverDetailsFont,styles.fontFamilyRegular]}>State:</Text>
                        <Text style={[styles.driverDetailsFont,styles.fontFamilyRegular]}>Zip Code :</Text>
                        <Text style={[styles.driverDetailsFont,styles.fontFamilyRegular]}>Join Date :</Text>
                    </View>
                    {this.props.isLogin}
                </View>
                <View style={{ paddingTop:15 }}>
                    <TouchableOpacity onPress={()=> this.props.navigation.navigate('Chat CS')} style={{ alignSelf:'center' }}>
                        <Text style={[styles.fontFamilyRegular,{fontSize:15}]}>Help</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ paddingTop:15 }}>
                    <TouchableOpacity style={{ alignSelf:'center' }}>
                        <Text style={[styles.fontFamilyRegular,{fontSize:15}]}>Change Account Setting</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    driverDetailsFont:{
        fontSize:16,
    },
    fontFamilyRegular:{
        fontFamily:'Asap-Regular'
    },
    fw400:{
        fontWeight:'400'
    }
})

export default withNavigation(ProfileDriver);