import React, { Component } from 'react';
import { StyleSheet, Text, View, StatusBar } from 'react-native';
import { WebView } from 'react-native-webview';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { SafeAreaView } from 'react-navigation';
// import WKWebView from 'react-native-wkwebview-reborn';
// StatusBar.setHidden(false);

// console.log(getStatusBarHeight());
// const statusBarHeight = parseInt(getStatusBarHeight());

class WebViewComponents extends Component {
  constructor() {
    super();
    this.state = {
      statusBarHeight: getStatusBarHeight(),
    };
  }
    render() {
      // return <View></View>;
      return(
        // <SafeAreaView>
          // {/* <StatusBar barStyle="light-content" style={{ back }}/> */}
          <View style={{ width:'100%', height:hp('100%'), paddingBottom:50 }}>
            <View style={{ height:this.state.statusBarHeight, backgroundColor:'#990018' }}></View>
            <WebView  useWebKit={true}
                      bounces={true} 
                      // javaScriptEnabled={true} 
                      // injectedJavaScript={`setInterval(function() { window.location.reload();}, 10000);`} 
                      // source={{ uri: 'http://rizkyserra.000webhostapp.com/' }} 
                      source={{ uri: 'https://maps.google.com' }} 
                      style={{ flex:1 }} 
            />
          </View>
        // </SafeAreaView>
      );
    }
  }

  export default WebViewComponents;