import React, { Component } from 'react';
import { View, Image, Text, Dimensions, StyleSheet, Button, StatusBar } from 'react-native';
import { withNavigation, SafeAreaView } from 'react-navigation';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faChevronUp, faChevronDown, faChevronCircleLeft, faChevronCircleRight, faTrash, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { ScrollView, TouchableHighlight, TouchableOpacity } from 'react-native-gesture-handler';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';

class TrackOrderDriver extends React.Component{
    render(){
        return(
            <View style={{ backgroundColor:'#ffffff', flexDirection:'column', flex:1, paddingTop:25, paddingLeft:15, paddingRight:15, }}>
                <StatusBar barStyle="dark-content"/>
                <View style={{ alignSelf:'center' }}>
                    <Image source={require('../images/logo-banner/Transparentbackgroundnew.png')} 
                        style={{ width:200, height:150, resizeMode:'contain', marginBottom:25 }}
                    />
                </View>
                <View style={{ flexDirection:'row', flex:0.15 }}>
                    <View style={{ flex:0.175 }}>
                        <Text style={[styles.fontFamilyRegular, { fontSize:17 }]}>Status:</Text>
                    </View>
                    <View style={{ flex:0.2 }}>
                        <View style={{ paddingBottom:2.5, paddingTop:2.5, top:-2.5, backgroundColor:'#4FCA55', 
                                        alignItems:'center', borderRadius:5 }}>
                            <Text style={[styles.fontFamilyRegular, { fontSize:16, color:'white', fontWeight:'600' }]}>Active</Text>
                        </View>
                    </View>
                </View>
                <View style={{ flexDirection:'row', flex:0.15 }}>
                    <View style={{ flex:1 }}>
                        <Text style={styles.fontFamilyRegular}>You have <Text style={{ color:'red' }}>4</Text> order</Text>
                    </View>
                </View>
                <View style={{ flexDirection:'column', padding:15, backgroundColor:'white', borderWidth:1, borderColor:'#ccc', marginTop:10 }}>
                    {/* <Text>AAA</Text> */}
                    <View style={{ width:'100%' }}>
                        <View style={{ borderRadius:5, marginLeft:15, marginRight:15, position:'absolute' }}>
                            <TouchableOpacity style={{ zIndex:1, backgroundColor:'purple', borderRadius:5, borderTopRightRadius:5, width:'100%', marginTop:10, paddingLeft:15, paddingRight:15, paddingTop:5, paddingBottom:5 }}>
                                <Text style={{ fontFamily:'Asap-Bold', color:'white' }}>Track</Text>
                            </TouchableOpacity>
                        </View>
                        <MapView
                            style={styles.map }
                            provider={PROVIDER_GOOGLE}
                            scrollEnabled={false}
                            initialRegion={{
                            latitude: 37.78825,
                            longitude: -122.4324,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421,
                            }}
                        />
                    </View>
                    <View style={{ flexDirection:'column', marginTop:15, marginBottom:15 }}>
                        <Text style={[styles.fontFamilyRegular, { fontSize:12, color:'red', marginBottom:5 }]}>Rynaldo Wahyudi</Text>
                        <View style={{ height:25 }}>
                            <Text style={[styles.fontFamilyRegular, { fontSize:15 }]}>1x SeaBox Prawn</Text>
                            <Text style={[styles.fontFamilyRegular, { fontSize:15 }]}>1x Dimsim Seafood</Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    fontFamilyRegular:{
        fontFamily:'Asap-Regular'
    },
    fw400:{
        fontWeight:'400'
    },
    map: {
        // ...StyleSheet.absoluteFillObject,
        height:150,
        zIndex:-1,
    },
});

export default withNavigation(TrackOrderDriver);