import React, { Component } from 'react';
import { View, Image, Text, Dimensions, StyleSheet } from 'react-native';
import { withNavigation, SafeAreaView } from 'react-navigation';
import { Header } from 'react-native-elements';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faChevronUp, faChevronDown, faChevronCircleLeft, faChevronCircleRight, faTrash, faTrashAlt } from '@fortawesome/free-solid-svg-icons'
import LinearGradient from 'react-native-linear-gradient';

var screenWidth = Dimensions.get('window').width;

class DiscoverRestaurants extends React.Component{
    constructor() {
        super();
        this.state = {
          showMenu1: true,
          showMenu2: true,
        };
      }
    
      ShowHideMenu1Component = () => {
        if (this.state.showMenu1 == true) {
          this.setState({ showMenu1: false });
        } else {
          this.setState({ showMenu1: true });
        }
      };
      ShowHideMenu2Component = () => {
        if (this.state.showMenu2 == true) {
          this.setState({ showMenu2: false });
        } else {
          this.setState({ showMenu2: true });
        }
      };
    render(){
        return(
            <SafeAreaView style={{ }}>
                <ScrollView style={{ paddingTop:15, paddingLeft:15, paddingRight:15, height:'100%'}}>
                    <View style={{ width:'100%', paddingBottom:10 }}>
                        <Text style={{ fontSize:20 }}>Browse by cuisine</Text>
                    </View>
                    <View style={{ marginLeft:-15, marginRight:-15 }}>
                        <View style={{ position:'absolute', top:55, left:5, zIndex:999, backgroundColor:'black', borderRadius:50, borderColor:'white' }}>
                            <TouchableOpacity onPress={() => { this.scroll.scrollTo({ x: 0 }) }}>
                                <FontAwesomeIcon icon={faChevronCircleLeft} size="30x" color="white" backgroundColor="white"/>
                            </TouchableOpacity>
                        </View>
                        <View style={{ position:'absolute', top:55, right:5, zIndex:999, backgroundColor:'black', borderRadius:50, borderWidth:0, borderColor:'white' }}>
                            <TouchableOpacity onPress={() => { this.scroll.scrollTo({ x: screenWidth }) }}>
                                <FontAwesomeIcon icon={faChevronCircleRight} size="30x" color="white" backgroundColor="white"/>
                            </TouchableOpacity>
                        </View>
                        <ScrollView horizontal={true} pagingEnabled={false} ref={(node) => this.scroll = node} showsHorizontalScrollIndicator={false} style={{ position:'relative', height:150, width:'100%' }}>
                            <View style={{ width:125, marginLeft:10, marginRight:10 }}>
                                <Image 
                                    style={{ width:125, height:125, borderRadius:50, resizeMode:'contain', alignSelf:'center' }}
                                    source={{uri:'http://rizkyserra.000webhostapp.com/assets/img/restaurants/125x125/1.jpg'}}
                                />
                                <Text style={{ textAlign:'center' }}>Singapore</Text>
                            </View>
                            <View style={{ width:125, marginLeft:10, marginRight:10 }}>
                                <Image 
                                    style={{ width:125, height:125, borderRadius:75, resizeMode:'contain', alignSelf:'center' }}
                                    source={{uri:'http://rizkyserra.000webhostapp.com/assets/img/restaurants/125x125/2.jpg'}}
                                />
                                <Text style={{ textAlign:'center' }}>Hongkong</Text>
                            </View>
                            <View style={{ width:125, marginLeft:10, marginRight:10 }}>
                                <Image 
                                    style={{ width:125, height:125, borderRadius:75, resizeMode:'contain', alignSelf:'center' }}
                                    source={{uri:'http://rizkyserra.000webhostapp.com/assets/img/restaurants/125x125/cuisine-2.jpg'}}
                                />
                                <Text style={{ textAlign:'center' }}>Thailand</Text>
                            </View>
                            <View style={{ width:125, marginLeft:10, marginRight:10 }}>
                                <Image 
                                    style={{ width:125, height:125, borderRadius:75, resizeMode:'contain', alignSelf:'center' }}
                                    source={{uri:'http://rizkyserra.000webhostapp.com/assets/img/restaurants/125x125/cuisine-3.jpg'}}
                                />
                                <Text style={{ textAlign:'center' }}>DimSim</Text>
                            </View>
                            <View style={{ width:125, marginLeft:10, marginRight:10 }}>
                                <Image 
                                    style={{ width:125, height:125, borderRadius:75, resizeMode:'contain', alignSelf:'center' }}
                                    source={{uri:'http://rizkyserra.000webhostapp.com/assets/img/restaurants/125x125/3.jpg'}}
                                />
                                <Text style={{ textAlign:'center' }}>Seafood</Text>
                            </View>
                        </ScrollView>
                    </View>
                    <View style={{ flexDirection:'column', flex:1, backgroundColor:'#fff', marginLeft:-15, marginRight:-15, paddingLeft:15, paddingRight:15 }}>
                        <View style={{ flexDirection:'row', flex:1, marginTop:15 }}>
                            {/* <Text>aaa</Text> */}
                            <Image style={{ width:'100%',height:175 }} source={{ uri:'http://rizkyserra.000webhostapp.com/assets/img/banner.jpg' }}/>
                            <View style={{ position:'absolute', width:'100%', height:175, backgroundColor:'rgba(0, 0, 0, 0.5)', padding:24}}>
                                <Text style={{ color:'white', fontWeight:'bold', fontSize:18, marginBottom:10 }}>Get $10 off your first order!</Text>
                                <Text style={{ color:'white', fontWeight:'400', fontSize:14 }}>Spend $15 or more and get $10 off your first delivery order.</Text>
                                <View style={{ flexDirection:'row', flex:1, marginTop:15, height:50 }}>
                                    <View style={screenWidth <= 375 ? styles.widthDealSm : styles.widthDeal}>
                                        <TouchableOpacity style={{ paddingLeft:12, paddingLeft:12, paddingTop:15, paddingBottom:15, borderWidth:2, borderColor:'white', width:'100%'}}>
                                            <Text style={{ fontSize:16, fontWeight:'600', color:'white' }}>Get Deal</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View style={{ flexDirection:'column', flex:1, paddingTop:15, paddingBottom:15 }}>
                            <View style={{ flexDirection:'row', flex:1, paddingBottom:10 }}>
                                <View style={{ flex:0.5 }}>
                                    <Text style={{ fontSize:25 }}>Menu 1</Text>
                                </View>
                                <View style={{ flex:0.5 }}>
                                    <TouchableOpacity onPress={this.ShowHideMenu1Component} style={{ alignSelf:'flex-end' }}>
                                    {this.state.showMenu1 ? (
                                        <FontAwesomeIcon icon={ faChevronUp } size="25x" style={{ marginTop:2.5 }} />
                                        ) :
                                        <FontAwesomeIcon icon={ faChevronDown } size="25x" style={{ marginTop:2.5 }} />
                                    }
                                    </TouchableOpacity>
                                </View>
                            </View>
                            {this.state.showMenu1 ? (
                                <View>
                                    <View style={{ flexDirection:'column', flex:1, marginBottom:18, borderRadius:5, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.5, shadowRadius: 3 }}>
                                        <View style={{ backgroundColor:'white', padding: 10, borderRadius:5 }}>
                                            <Image style={{ width:'100%', height:200, resizeMode:'contain', marginBottom:15 }} source={{ uri:'http://rizkyserra.000webhostapp.com/assets/img/dish/150x151/dish-4.jpg' }} />
                                            <Text style={{ fontSize:16, fontWeight:'600', marginBottom:10 }}>Menu SeaBox - Seabox Essential A</Text>
                                            <Text style={{ marginBottom:15 }}>600-700 Cal.</Text>
                                            <View style={{ flexDirection:'row', flex:1, marginBottom:10 }}>
                                                <View style={{ flex:0.225 }}>
                                                    <TouchableOpacity>
                                                        {/* <Text>AAA</Text> */}
                                                        <LinearGradient colors={['#f20000', '#a20000']}
                                                            start={{ x: 0, y: 1 }}
                                                            end={{ x: 1, y: 0 }}
                                                            style={{ paddingTop:8, paddingBottom:8, borderRadius:3 }}>
                                                            <Text style={{ color:'white', fontWeight:'600', fontSize:16, textAlign:'center', letterSpacing:2 }}>Select</Text>
                                                        </LinearGradient>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                            <Text style={{ lineHeight:24, marginBottom:10 }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor...</Text>
                                            <Text style={{ color:'#13aa37', fontWeight:'600', fontSize:16 }}>$7.99+</Text>
                                        </View>
                                    </View>
                                    <View style={{ flexDirection:'column', flex:1, marginBottom:18, borderRadius:5, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.5, shadowRadius: 3 }}>
                                        <View style={{ backgroundColor:'white', padding: 10, borderRadius:5 }}>
                                            <Image style={{ width:'100%', height:200, resizeMode:'contain', marginBottom:15 }} source={{ uri:'http://rizkyserra.000webhostapp.com/assets/img/dish/150x151/dish-5.jpg' }} />
                                            <Text style={{ fontSize:16, fontWeight:'600', marginBottom:10 }}>Menu SeaBox - Seabox Essential B</Text>
                                            <Text style={{ marginBottom:15 }}>600-700 Cal.</Text>
                                            <View style={{ flexDirection:'row', flex:1, marginBottom:10 }}>
                                                <View style={{ flex:0.225 }}>
                                                    <TouchableOpacity>
                                                        {/* <Text>AAA</Text> */}
                                                        <LinearGradient colors={['#f20000', '#a20000']}
                                                            start={{ x: 0, y: 1 }}
                                                            end={{ x: 1, y: 0 }}
                                                            style={{ paddingTop:8, paddingBottom:8, borderRadius:3 }}>
                                                            <Text style={{ color:'white', fontWeight:'600', fontSize:16, textAlign:'center', letterSpacing:2 }}>Select</Text>
                                                        </LinearGradient>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                            <Text style={{ lineHeight:24, marginBottom:10 }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor...</Text>
                                            <Text style={{ color:'#13aa37', fontWeight:'600', fontSize:16 }}>$7.99+</Text>
                                        </View>
                                    </View>
                                    <View style={{ flexDirection:'column', flex:1, marginBottom:18, borderRadius:5, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.5, shadowRadius: 3 }}>
                                        <View style={{ backgroundColor:'white', padding: 10, borderRadius:5 }}>
                                            <Image style={{ width:'100%', height:200, resizeMode:'contain', marginBottom:15 }} source={{ uri:'http://rizkyserra.000webhostapp.com/assets/img/dish/150x151/dish-4.jpg' }} />
                                            <Text style={{ fontSize:16, fontWeight:'600', marginBottom:10 }}>Menu SeaBox - Seabox Essential C</Text>
                                            <Text style={{ marginBottom:15 }}>600-700 Cal.</Text>
                                            <View style={{ flexDirection:'row', flex:1, marginBottom:10 }}>
                                                <View style={{ flex:0.225 }}>
                                                    <TouchableOpacity>
                                                        {/* <Text>AAA</Text> */}
                                                        <LinearGradient colors={['#f20000', '#a20000']}
                                                            start={{ x: 0, y: 1 }}
                                                            end={{ x: 1, y: 0 }}
                                                            style={{ paddingTop:8, paddingBottom:8, borderRadius:3 }}>
                                                            <Text style={{ color:'white', fontWeight:'600', fontSize:16, textAlign:'center', letterSpacing:2 }}>Select</Text>
                                                        </LinearGradient>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                            <Text style={{ lineHeight:24, marginBottom:10 }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor...</Text>
                                            <Text style={{ color:'#13aa37', fontWeight:'600', fontSize:16 }}>$7.99+</Text>
                                        </View>
                                    </View>
                                    <View style={{ flexDirection:'column', flex:1, marginBottom:18, borderRadius:5, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.5, shadowRadius: 3 }}>
                                        <View style={{ backgroundColor:'white', padding: 10, borderRadius:5 }}>
                                            <Image style={{ width:'100%', height:200, resizeMode:'contain', marginBottom:15 }} source={{ uri:'http://rizkyserra.000webhostapp.com/assets/img/dish/150x151/dish-5.jpg' }} />
                                            <Text style={{ fontSize:16, fontWeight:'600', marginBottom:10 }}>Menu SeaBox - Seabox Essential Prawn</Text>
                                            <Text style={{ marginBottom:15 }}>600-700 Cal.</Text>
                                            <View style={{ flexDirection:'row', flex:1, marginBottom:10 }}>
                                                <View style={{ flex:0.225 }}>
                                                    <TouchableOpacity>
                                                        {/* <Text>AAA</Text> */}
                                                        <LinearGradient colors={['#f20000', '#a20000']}
                                                            start={{ x: 0, y: 1 }}
                                                            end={{ x: 1, y: 0 }}
                                                            style={{ paddingTop:8, paddingBottom:8, borderRadius:3 }}>
                                                            <Text style={{ color:'white', fontWeight:'600', fontSize:16, textAlign:'center', letterSpacing:2 }}>Select</Text>
                                                        </LinearGradient>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                            <Text style={{ lineHeight:24, marginBottom:10 }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor...</Text>
                                            <Text style={{ color:'#13aa37', fontWeight:'600', fontSize:16 }}>$7.99+</Text>
                                        </View>
                                    </View>
                                </View>
                                ) : null
                            }
                        </View>
                        <View style={{ flexDirection:'column', flex:1, paddingTop:15, paddingBottom:0 }}>
                            <View style={{ flexDirection:'row', flex:1, paddingBottom:10 }}>
                                <View style={{ flex:0.5 }}>
                                    <Text style={{ fontSize:25 }}>Menu 2</Text>
                                </View>
                                <View style={{ flex:0.5 }}>
                                    <TouchableOpacity onPress={this.ShowHideMenu2Component} style={{ alignSelf:'flex-end' }}>
                                    {this.state.showMenu2 ? (
                                        <FontAwesomeIcon icon={ faChevronUp } size="25x" style={{ marginTop:2.5 }} />
                                        ) :
                                        <FontAwesomeIcon icon={ faChevronDown } size="25x" style={{ marginTop:2.5 }} />
                                    }
                                    </TouchableOpacity>
                                </View>
                            </View>
                            {this.state.showMenu2 ? (
                                <View>
                                    <View style={{ flexDirection:'column', flex:1, marginBottom:18, borderRadius:5, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.5, shadowRadius: 3 }}>
                                        <View style={{ backgroundColor:'white', padding: 10, borderRadius:5 }}>
                                            <Image style={{ width:'100%', height:200, resizeMode:'contain', marginBottom:15 }} source={{ uri:'http://rizkyserra.000webhostapp.com/assets/img/dish/150x151/dish-4.jpg' }} />
                                            <Text style={{ fontSize:16, fontWeight:'600', marginBottom:10 }}>Menu SeaBox - Seabox Essential A</Text>
                                            <Text style={{ marginBottom:15 }}>600-700 Cal.</Text>
                                            <View style={{ flexDirection:'row', flex:1, marginBottom:10 }}>
                                                <View style={{ flex:0.225 }}>
                                                    <TouchableOpacity>
                                                        {/* <Text>AAA</Text> */}
                                                        <LinearGradient colors={['#f20000', '#a20000']}
                                                            start={{ x: 0, y: 1 }}
                                                            end={{ x: 1, y: 0 }}
                                                            style={{ paddingTop:8, paddingBottom:8, borderRadius:3 }}>
                                                            <Text style={{ color:'white', fontWeight:'600', fontSize:16, textAlign:'center', letterSpacing:2 }}>Select</Text>
                                                        </LinearGradient>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                            <Text style={{ lineHeight:24, marginBottom:10 }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor...</Text>
                                            <Text style={{ color:'#13aa37', fontWeight:'600', fontSize:16 }}>$7.99+</Text>
                                        </View>
                                    </View>
                                    <View style={{ flexDirection:'column', flex:1, marginBottom:18, borderRadius:5, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.5, shadowRadius: 3 }}>
                                        <View style={{ backgroundColor:'white', padding: 10, borderRadius:5 }}>
                                            <Image style={{ width:'100%', height:200, resizeMode:'contain', marginBottom:15 }} source={{ uri:'http://rizkyserra.000webhostapp.com/assets/img/dish/150x151/dish-5.jpg' }} />
                                            <Text style={{ fontSize:16, fontWeight:'600', marginBottom:10 }}>Menu SeaBox - Seabox Essential B</Text>
                                            <Text style={{ marginBottom:15 }}>600-700 Cal.</Text>
                                            <View style={{ flexDirection:'row', flex:1, marginBottom:10 }}>
                                                <View style={{ flex:0.225 }}>
                                                    <TouchableOpacity>
                                                        {/* <Text>AAA</Text> */}
                                                        <LinearGradient colors={['#f20000', '#a20000']}
                                                            start={{ x: 0, y: 1 }}
                                                            end={{ x: 1, y: 0 }}
                                                            style={{ paddingTop:8, paddingBottom:8, borderRadius:3 }}>
                                                            <Text style={{ color:'white', fontWeight:'600', fontSize:16, textAlign:'center', letterSpacing:2 }}>Select</Text>
                                                        </LinearGradient>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                            <Text style={{ lineHeight:24, marginBottom:10 }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor...</Text>
                                            <Text style={{ color:'#13aa37', fontWeight:'600', fontSize:16 }}>$7.99+</Text>
                                        </View>
                                    </View>
                                    <View style={{ flexDirection:'column', flex:1, marginBottom:18, borderRadius:5, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.5, shadowRadius: 3 }}>
                                        <View style={{ backgroundColor:'white', padding: 10, borderRadius:5 }}>
                                            <Image style={{ width:'100%', height:200, resizeMode:'contain', marginBottom:15 }} source={{ uri:'http://rizkyserra.000webhostapp.com/assets/img/dish/150x151/dish-4.jpg' }} />
                                            <Text style={{ fontSize:16, fontWeight:'600', marginBottom:10 }}>Menu SeaBox - Seabox Essential C</Text>
                                            <Text style={{ marginBottom:15 }}>600-700 Cal.</Text>
                                            <View style={{ flexDirection:'row', flex:1, marginBottom:10 }}>
                                                <View style={{ flex:0.225 }}>
                                                    <TouchableOpacity>
                                                        {/* <Text>AAA</Text> */}
                                                        <LinearGradient colors={['#f20000', '#a20000']}
                                                            start={{ x: 0, y: 1 }}
                                                            end={{ x: 1, y: 0 }}
                                                            style={{ paddingTop:8, paddingBottom:8, borderRadius:3 }}>
                                                            <Text style={{ color:'white', fontWeight:'600', fontSize:16, textAlign:'center', letterSpacing:2 }}>Select</Text>
                                                        </LinearGradient>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                            <Text style={{ lineHeight:24, marginBottom:10 }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor...</Text>
                                            <Text style={{ color:'#13aa37', fontWeight:'600', fontSize:16 }}>$7.99+</Text>
                                        </View>
                                    </View>
                                    <View style={{ flexDirection:'column', flex:1, marginBottom:18, borderRadius:5, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.5, shadowRadius: 3 }}>
                                        <View style={{ backgroundColor:'white', padding: 10, borderRadius:5 }}>
                                            <Image style={{ width:'100%', height:200, resizeMode:'contain', marginBottom:15 }} source={{ uri:'http://rizkyserra.000webhostapp.com/assets/img/dish/150x151/dish-5.jpg' }} />
                                            <Text style={{ fontSize:16, fontWeight:'600', marginBottom:10 }}>Menu SeaBox - Seabox Essential Prawn</Text>
                                            <Text style={{ marginBottom:15 }}>600-700 Cal.</Text>
                                            <View style={{ flexDirection:'row', flex:1, marginBottom:10 }}>
                                                <View style={{ flex:0.225 }}>
                                                    <TouchableOpacity>
                                                        {/* <Text>AAA</Text> */}
                                                        <LinearGradient colors={['#f20000', '#a20000']}
                                                            start={{ x: 0, y: 1 }}
                                                            end={{ x: 1, y: 0 }}
                                                            style={{ paddingTop:8, paddingBottom:8, borderRadius:3 }}>
                                                            <Text style={{ color:'white', fontWeight:'600', fontSize:16, textAlign:'center', letterSpacing:2 }}>Select</Text>
                                                        </LinearGradient>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                            <Text style={{ lineHeight:24, marginBottom:10 }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor...</Text>
                                            <Text style={{ color:'#13aa37', fontWeight:'600', fontSize:16 }}>$7.99+</Text>
                                        </View>
                                    </View>
                                </View>
                                ) : null
                            }
                        </View>
                        <View style={{ marginBottom:30, flexDirection:'column', flex:1, borderRadius:5, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.5, shadowRadius: 3 }}>
                            <View style={{ backgroundColor:'white', borderRadius:5 }}>
                                <Text style={{ fontSize:15, fontWeight:'900', padding:15 }}>Your Order</Text>
                                <View style={{ height:0.75, flex:1.5, backgroundColor:'#ccc', paddingLeft:-15, paddingRight:-15 }}></View>
                                <ScrollView style={{ height:200 }}>
                                    <View style={{ flexDirection:'row', flex:1, padding:15 }}>
                                        <View style={{ flex:0.1 }}>
                                            <Text>1</Text>
                                        </View>
                                        <View style={{ flex:0.5 }}>
                                            <Text>Seafood Essential A</Text>
                                        </View>
                                        <View style={{ flex:0.2, alignItems:'flex-end' }}>
                                            <FontAwesomeIcon icon={faTrashAlt}/>
                                        </View>
                                        <View style={{ flex:0.2, alignItems:'flex-end' }}>
                                            <Text>$2.25</Text>
                                        </View>
                                    </View>
                                    <View style={{ height:0.75, flex:1.5, backgroundColor:'#ccc', paddingLeft:-15, paddingRight:-15 }}></View>
                                    <View style={{ flexDirection:'row', flex:1, padding:15 }}>
                                        <View style={{ flex:0.1 }}>
                                            <Text>1</Text>
                                        </View>
                                        <View style={{ flex:0.5 }}>
                                            <Text>Seafood Essential B</Text>
                                        </View>
                                        <View style={{ flex:0.2, alignItems:'flex-end' }}>
                                            <FontAwesomeIcon icon={faTrashAlt}/>
                                        </View>
                                        <View style={{ flex:0.2, alignItems:'flex-end' }}>
                                            <Text>$2.25</Text>
                                        </View>
                                    </View>
                                    <View style={{ height:0.75, flex:1.5, backgroundColor:'#ccc', paddingLeft:-15, paddingRight:-15 }}></View>
                                    <View style={{ flexDirection:'row', flex:1, padding:15 }}>
                                        <View style={{ flex:0.1 }}>
                                            <Text>1</Text>
                                        </View>
                                        <View style={{ flex:0.5 }}>
                                            <Text>Seafood Essential C</Text>
                                        </View>
                                        <View style={{ flex:0.2, alignItems:'flex-end' }}>
                                            <FontAwesomeIcon icon={faTrashAlt}/>
                                        </View>
                                        <View style={{ flex:0.2, alignItems:'flex-end' }}>
                                            <Text>$2.25</Text>
                                        </View>
                                    </View>
                                    <View style={{ height:0.75, flex:1.5, backgroundColor:'#ccc', paddingLeft:-15, paddingRight:-15 }}></View>
                                    <View style={{ flexDirection:'row', flex:1, padding:15 }}>
                                        <View style={{ flex:0.1 }}>
                                            <Text>1</Text>
                                        </View>
                                        <View style={{ flex:0.5 }}>
                                            <Text>Seafood Prawn</Text>
                                        </View>
                                        <View style={{ flex:0.2, alignItems:'flex-end' }}>
                                            <FontAwesomeIcon icon={faTrashAlt}/>
                                        </View>
                                        <View style={{ flex:0.2, alignItems:'flex-end' }}>
                                            <Text>$2.25</Text>
                                        </View>
                                    </View>
                                    <View style={{ height:0.75, flex:1.5, backgroundColor:'#ccc', paddingLeft:-15, paddingRight:-15 }}></View>
                                    <View style={{ flexDirection:'row', flex:1, padding:15 }}>
                                        <View style={{ flex:0.5 }}>
                                            <Text style={{  }}>Items subtotal</Text>
                                        </View>
                                        <View style={{ flex:0.5, alignItems:'flex-end' }}>
                                            <Text>$9.00</Text>
                                        </View>
                                    </View>
                                </ScrollView>
                                <View style={{ height:0.75, flex:1.5, backgroundColor:'#ccc', paddingLeft:-15, paddingRight:-15 }}></View>
                                <View style={{ padding:15, alignItems:'center', flex:1, flexDirection:'row' }}>
                                    <View style={{ flex:1 }}>
                                        <TouchableOpacity style={{ padding:15, backgroundColor:'#28a745', borderRadius:5, width:'100%', alignItems:'center'}}>
                                            <Text style={{ color:'white', fontWeight:'700', fontSize:13 }}>Proceed to Checkout</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    widthDeal:{
        width:95,
        height:50
    },
    widthDealSm:{
        width:95,
        height:50
    },
});

export default withNavigation(DiscoverRestaurants);