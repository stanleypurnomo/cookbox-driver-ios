import React, { Component } from 'react';
import { View, Image, Text } from 'react-native';
import { withNavigation } from 'react-navigation';
import { Header } from 'react-native-elements';
import { TouchableOpacity } from 'react-native-gesture-handler';

class Footer extends React.Component{
    render(){
        return(
            <View style={{ flexDirection:'column', flex:1, paddingTop:15, paddingBottom:50, backgroundColor:'black' }}>
              <Text style={{ fontSize:22, alignSelf:'center', paddingBottom:30, color:'white' }}>Need help</Text>
              <View style={{ flexDirection:'row', flex:1, paddingLeft:15, paddingRight:15 }}>
                <View style={{ flex:0.4 }}>
                  <Text style={{ color:'#c5c5c5', alignSelf:'center', fontSize:18 }}>Messages Us</Text>
                </View>
                <View style={{ flex:0.6 }}>
                  <TouchableOpacity style={{ paddingBottom:25 }}>
                    <Text style={{ alignSelf:'flex-start', fontSize:18, color:'white' }}>+(347) 123 456 789</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={{ flexDirection:'row', flex:1, paddingLeft:15, paddingRight:15 }}>
                <View style={{ flex:0.4 }}>
                  <Text style={{ alignSelf:'center', fontSize:18, color:'#c5c5c5' }}>Email us</Text>
                </View>
                <View style={{ flex:0.6 }}>
                  <TouchableOpacity style={{ paddingBottom:25 }}>
                    <Text style={{ alignSelf:'flex-start', fontSize:18, color:'white' }}>admin@cookbox.com.au</Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={{ flexDirection:'row', flex:1, paddingLeft:15, paddingRight:15 }}>
                <View style={{ flex:1 }}>
                  <TouchableOpacity
                    onPress={() => navigation.navigate('Privacy Policy')}>
                    <Text style={{ alignSelf:'center', fontSize:18, color:'#c5c5c5' }}>
                      Privacy Policy
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
              {/* <Button
                title="Login first"
                onPress={() => navigation.navigate('Login')}
              /> */}
            </View>
        );
    }
}

export default Footer;