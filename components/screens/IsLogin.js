import React, { Component } from 'react';
import {View, Text, AsyncStorage, ActivityIndicator} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import { withNavigation } from 'react-navigation';
import { CommonActions } from '@react-navigation/native';

class IsLogin extends React.Component{
    constructor(){
      super();
      this.state = {
        isLoading: false,
      };
      // this.logout = this.logout.bind(this);
      // this.state.isLogin = this.state.isLogin.bind(this);
    }
    logout = () => {
      this.setState({
        isLoading: true
      })
      var navigating  = this.props.navigation;
      AsyncStorage.setItem('isLogin','false');
      setTimeout(function(){navigating.dispatch( CommonActions.reset({ index: 0, routes: [ { name: 'Welcome' }, ], }) ); alert('You are successfully logout. Please login again...');}, 1000)
      // this.props.navigation.dispatch(
      //   CommonActions.reset({
      //     index: 0,
      //     routes: [
      //       { name: 'Welcome' },
      //       // {
      //       //   name: 'Profile',
      //       //   params: { user: 'jane' },
      //       // },
      //     ],
      //   })
      // );
    }
    render(){
      return(
        <View>
            {
              this.state.isLoading === true
              ?
              <View style={{ position:'absolute', top:'-100%', zIndex:999, alignSelf:'center' }}>
                  <View style={{ backgroundColor:'white', borderWidth:1, borderColor:'#ccc', 
                  alignSelf:'center', padding:20, shadowColor: "#000",
                  shadowOffset: {width: 0,height: 2,},
                  shadowOpacity: 0.25,shadowRadius: 3.84,elevation: 5, }}>
                      <ActivityIndicator size="large" color="black" />
                  </View>
                  {/* <Text>Is Loading: true</Text> */}
                  {/* show loading spinner */}
              </View>
              :
              null
            }
          <TouchableOpacity onPress={this.logout} style={{ borderRadius:5, width:'100%', backgroundColor:'#4FCA55', padding:10, alignItems:'center' }}>
            <Text style={{ fontFamily:'Asap-Regular', color:'white',fontSize:18, fontWeight:'600' }}>Log out</Text>
          </TouchableOpacity>
          {/* <Button title="Click here Log out" onPress={this.logout}></Button>
          <Text>{String(this.state.isLogin)}</Text> */}
        </View>
      )
    }
  }
export default withNavigation(IsLogin);