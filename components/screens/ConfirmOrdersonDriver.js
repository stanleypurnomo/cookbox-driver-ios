import React, { Component } from 'react';
import { View, Image, Text, Dimensions, StyleSheet } from 'react-native';
import { withNavigation, SafeAreaView } from 'react-navigation';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';

class ConfirmOrdersonDriver extends React.Component{
    render(){
        return(
            <SafeAreaView>
                <ScrollView style={{ paddingTop:15, paddingLeft:15, paddingRight:15, height:'100%' }}>
                    <View style={{ padding:15, marginBottom:15, flexDirection:'column', flex:1, backgroundColor:'white', borderRadius:5, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.5, shadowRadius: 3 }}>
                        <TouchableOpacity onPress={() =>  this.props.navigation.navigate('Accept Order')}>
                            <View style={{ flexDirection:'row', flex:1 }}>
                                <View style={{ flex:0.4 }}>
                                    <Text style={{ fontWeight:'600', marginBottom:10 }}>Order #1</Text>
                                </View>
                                <View style={{ flex:0.6, alignItems:'flex-end' }}>
                                    <View style={{ padding:5, backgroundColor:'blue', borderRadius:5 }}>
                                        <Text style={{ color:'white', fontWeight:'700' }}>Waiting</Text>
                                    </View>
                                </View>
                            </View>
                            <Text style={{ marginBottom:5 }}>From Restaurants:</Text>
                            <Text style={{ marginBottom:5 }}>My Seafood Restaurant</Text>
                            <Text style={{ marginBottom:5 }}>To Delivery Address:</Text>
                            <Text style={{ marginBottom:5 }}>John Doe - My Delivery Address</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ padding:15, marginBottom:15, flexDirection:'column', flex:1, backgroundColor:'white', borderRadius:5, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.5, shadowRadius: 3 }}>
                        <TouchableOpacity>
                            <View style={{ flexDirection:'row', flex:1 }}>
                                <View style={{ flex:0.4 }}>
                                    <Text style={{ fontWeight:'600', marginBottom:10 }}>Order #1</Text>
                                </View>
                                <View style={{ flex:0.6, alignItems:'flex-end' }}>
                                    <View style={{ padding:5, backgroundColor:'green', borderRadius:5 }}>
                                        <Text style={{ color:'white', fontWeight:'700' }}>Confirmed</Text>
                                    </View>
                                </View>
                            </View>
                            <Text style={{ marginBottom:5 }}>From Restaurants:</Text>
                            <Text style={{ marginBottom:5 }}>My Seafood Restaurant</Text>
                            <Text style={{ marginBottom:5 }}>To Delivery Address:</Text>
                            <Text style={{ marginBottom:5 }}>John Doe - My Delivery Address</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ padding:15, marginBottom:15, flexDirection:'column', flex:1, backgroundColor:'white', borderRadius:5, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.5, shadowRadius: 3 }}>
                        <TouchableOpacity>
                            <View style={{ flexDirection:'row', flex:1 }}>
                                <View style={{ flex:0.4 }}>
                                    <Text style={{ fontWeight:'600', marginBottom:10 }}>Order #1</Text>
                                </View>
                                <View style={{ flex:0.6, alignItems:'flex-end' }}>
                                    <View style={{ padding:5, backgroundColor:'brown', borderRadius:5 }}>
                                        <Text style={{ color:'white', fontWeight:'700' }}>At restaurant</Text>
                                    </View>
                                </View>
                            </View>
                            <Text style={{ marginBottom:5 }}>From Restaurants:</Text>
                            <Text style={{ marginBottom:5 }}>My Seafood Restaurant</Text>
                            <Text style={{ marginBottom:5 }}>To Delivery Address:</Text>
                            <Text style={{ marginBottom:5 }}>John Doe - My Delivery Address</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ padding:15, marginBottom:15, flexDirection:'column', flex:1, backgroundColor:'white', borderRadius:5, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.5, shadowRadius: 3 }}>
                        <TouchableOpacity>
                            <View style={{ flexDirection:'row', flex:1 }}>
                                <View style={{ flex:0.4 }}>
                                    <Text style={{ fontWeight:'600', marginBottom:10 }}>Order #1</Text>
                                </View>
                                <View style={{ flex:0.6, alignItems:'flex-end' }}>
                                    <View style={{ padding:5, backgroundColor:'cyan', borderRadius:5 }}>
                                        <Text style={{ color:'#2c2c2c', fontWeight:'700' }}>Out of delivery</Text>
                                    </View>
                                </View>
                            </View>
                            <Text style={{ marginBottom:5 }}>From Restaurants:</Text>
                            <Text style={{ marginBottom:5 }}>My Seafood Restaurant</Text>
                            <Text style={{ marginBottom:5 }}>To Delivery Address:</Text>
                            <Text style={{ marginBottom:5 }}>John Doe - My Delivery Address</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ padding:15, marginBottom:15, flexDirection:'column', flex:1, backgroundColor:'white', borderRadius:5, shadowColor: '#000', shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.5, shadowRadius: 3 }}>
                        <TouchableOpacity>
                            <View style={{ flexDirection:'row', flex:1 }}>
                                <View style={{ flex:0.4 }}>
                                    <Text style={{ fontWeight:'600', marginBottom:10 }}>Order #1</Text>
                                </View>
                                <View style={{ flex:0.6, alignItems:'flex-end' }}>
                                    <View style={{ padding:5, backgroundColor:'lightgreen', borderRadius:5 }}>
                                        <Text style={{ color:'#2c2c2c', fontWeight:'700' }}>Delivered</Text>
                                    </View>
                                </View>
                            </View>
                            <Text style={{ marginBottom:5 }}>From Restaurants:</Text>
                            <Text style={{ marginBottom:5 }}>My Seafood Restaurant</Text>
                            <Text style={{ marginBottom:5 }}>To Delivery Address:</Text>
                            <Text style={{ marginBottom:5 }}>John Doe - My Delivery Address</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </SafeAreaView>
        );
    }
}

export default withNavigation(ConfirmOrdersonDriver);