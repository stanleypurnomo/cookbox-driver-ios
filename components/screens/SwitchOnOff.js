import React from 'react';
import { Switch, Text, View, StyleSheet } from 'react-native';
export default class App extends React.Component {
    state = {
        switchValue:this.props.getStatus
    }
    toggleSwitch = (value) => {
        this.setState({
          switchValue: value
        });
    }
  render() {
    return (
      <View style={{ top:5 }}>
        {/* <Text>{this.state.switchValue?'Switch is ON':'Switch is OFF'}</Text> */}
        <Switch
        //   style={{marginTop:0}}
          thumbColor={this.state.switchValue == true ? "white" : "white"}
          trackColor={{ false: "transparent", true: "#f54f29" }}
          ios_backgroundColor="#2c2c2c"
          onValueChange = {this.props.onSwitch}
          value = {this.state.switchValue}/>
      </View>
    );  
  } 
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    top:5
  }
});