import React, { Component } from 'react';

import { View, Text, StyleSheet, AsyncStorage, ActivityIndicator, Alert } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { withNavigation } from 'react-navigation';
// import SkeletonContent from "react-native-skeleton-content";

const LoadingSkeleton = () => (
    <SkeletonContent
        containerStyle={{flex: 1, width: 300}}
        isLoading={true}
        layout={[
        { width: 220, height: 20, marginBottom: 6 },
        { width: 180, height: 20, marginBottom: 6 },
        ]}
        >
            <Text>Loading</Text>
            {/* <View></Views> */}
    </SkeletonContent>
);

class ButtonApproveDriver extends Component{
    constructor(){
        super();
        this.state = {
            storageName: '',
            loading:'0',
            loadingTrue:'1',
        };
        // this.changeToCanceled = this.changeToCanceled.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
        // this.changeToReadySend = this.changeToReadySend.bind(this);
        // this.changeToReceived = this.changeToReceived.bind(this);
        this.loading5Sec = this.loading5Sec.bind(this);
        // this.render = this.render.bind(this);
    }

    componentDidMount(){
        // console.log(storageName);
        var storageName = 'statusOrderID-'+this.props.orderId;
        this.setState({
            storageName: storageName.toString()
        });
        // alert(this.state.storageName);
        var i = null;
        // for(i=1;i<=3;i++){
        //     AsyncStorage.setItem('statusOrderID-'+i, 'null');
        // }
        // var thisState = this;
        // setTimeout(function(){thisState.setState({beingMade: true, readyToSend: false, received: false, canceled: false,})}, 1000)
        // AsyncStorage.setItem(storageName,'beingSend');
        this.loading5Sec();
        // this.setState({
        //     loading:true
        // })
        // alert(this.state.loading);
        // this.checkStatusAgain();
    }

    loading5Sec = () => {
        this.setState({
            storageName: '5x'
        })
        // alert(this.state.storageName);
        // this.setState({ loading: '5' }, () => {
        //     this.setState({ loading: this.state.loading})
        //     alert(this.state.loading+'yes');
        // }); 
     }

    // loading5Sec(){
    //     this.setState({
    //         loading:true
    //     })
    //     alert(this.state.loading);
        // var thisState = this;
        // setTimeout(function(){thisState.setState({loading:false})}, 2500)
    // }

    // fetchStatus = async () => {
    //     try {
    //         const getStatus = await AsyncStorage.getItem('statusOrderID-'+this.props.orderId);
    //         // const jsonVal = JSON.parse(json);
    //         var thisState = this;
    //         setTimeout(function(){thisState.setState({statusID:getStatus})}, 2500)
    //         this.setState({
    //             statusID:null
    //         });
    //         setTimeout(function(){thisState.setState({statusID:getStatus})}, 2500)
    //         console.log(this.state.statusID);
    //         console.log('true');
    //     } catch (error) {
    //         console.log(error);
    //     }
    // }

    checkStatusAgain(){
        // try{
            // var storageNameStr = this.state.storageName.toString();
            // const getStatus = await AsyncStorage.getItem('statusOrderID-'+this.props.orderId);
            // console.log(storageNameStr);
            this.setState({
                statusID:null
            })
            var getStatusVal = this.props.statusOrder;
            AsyncStorage.setItem('statusOrderID-'+this.props.orderId, getStatusVal);
            this.fetchStatus();
            // var getStatus = this.props.statusOrder;
            // console.log(getStatus);
            // var thisState = this;
            // setTimeout(function(){thisState.setState({statusID:getStatus})}, 2500)
            // if(getStatus == 'null' || !getStatus){
            //     setTimeout(function(){thisState.setState({statusID:1})}, 1000);
            // }
            // if(getStatus == 3){
            //     setTimeout(function(){thisState.setState({statusID:3})}, 2500)
            // } else if(getStatus == 1){
            //     setTimeout(function(){thisState.setState({statusID:1})}, 2500)
            // } else if(getStatus == 4){
            //     setTimeout(function(){thisState.setState({statusID:4})}, 2500)
            // } else if(getStatus == 2){
            //     setTimeout(function(){thisState.setState({statusID:2})}, 2500)
            // } else if(getStatus == 5){
            //     setTimeout(function(){thisState.setState({statusID:5})}, 2500)
            // }
        // } 
        // catch (error) {
        //     console.log(error);
        // }
    };

    changeToReadySend(){
        var req = new XMLHttpRequest();
        var url = 'http://seafoodau.herokuapp.com/api/v1/order/process/'+this.props.orderId;
        req.responseType = 'json';
        req.open('GET', url, true);
        req.onload  = function(This) {
            var jsonResponse = req.response;
            if(jsonResponse.status === 'success'){
                alert('Success Process');
            }
        };
        this.setState({
            statusID: 3
        });
        req.send(null)
        AsyncStorage.setItem(this.state.storageName, 'readySend');
    }
    changeToReceived(){
        var req = new XMLHttpRequest();
        var url = 'http://seafoodau.herokuapp.com/api/v1/order/sending/'+this.props.orderId;
        req.responseType = 'json';
        req.open('GET', url, true);
        req.onload  = function(This) {
            var jsonResponse = req.response;
            if(jsonResponse.status === 'success'){
                alert('Success Sending');
            }
        };
        this.setState({
            statusID: 4
        });
        req.send(null)
        AsyncStorage.setItem(this.state.storageName, 'received');
        // this.parent.hide();
    }
    changeToCanceled(){
        this.setState({
            statusID: 2
        })
        console.log(this.state.storageName);
    }
    changeToDone(){
        var req = new XMLHttpRequest();
        var url = 'http://seafoodau.herokuapp.com/api/v1/order/completed/'+this.props.orderId;
        req.responseType = 'json';
        req.open('GET', url, true);
        req.onload  = function(This) {
            var jsonResponse = req.response;
            if(jsonResponse.status === 'success'){
                alert('Success Sending');
            }
        };
        this.setState({
            statusID: 5,
            // beingMade: false,
            // readyToSend: false,
            // received: true,
            // canceled: false
            // loading: true
        })
        setTimeout(() => {
            this.setState({loading: true},
            function(){
               console.log(this.state.loading, 'true');
            });
        }, 1000)
        var thisProps = this.props;
        var thisState = this.state;
        // alert('Please wait...');
        setTimeout(function(){
            thisProps.navigation.reset({
                index: 0,
                routes: [
                { name: 'Home',},
                ],
            });
            // AsyncStorage.setItem(thisState.storageName, 'done');
            alert('Your request has done');
        }, 1000);
    }
    cancelOrder(){
        this.changeToCanceled();
        this.props.navigation.navigate('Order Out of Stock');
    }
    render(){
        const { onDone } = this.props;
        const { onCancel } = this.props;
        const { onRefresh } = this.props;
        const { onProcess } = this.props;
        if(this.props.statusOrder){
            return(
            <View>
                {
                    this.props.statusOrder == 3
                    ?
                    <View style={{ flexDirection:'row', paddingLeft:5, paddingRight:5, marginTop:10 }}>
                        {/* <View style={{ flex:1, alignItems:'center' }}> */}
                            <View style={{ flex:0.5, alignItems:'center' }}>
                                <TouchableOpacity onPress={onProcess} style={{ padding:5, backgroundColor:'#4FCA55', borderRadius:5 }}>
                                    <Text style={[styles.fontFamilyRegular, { color:'white' }]}>Being made</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ flex:0.5, alignItems:'center' }}>
                                <TouchableOpacity onPress={onCancel} style={{ padding:5, backgroundColor:'#f54f29', borderRadius:5 }}>
                                    <Text style={[ styles.fontFamilyRegular,{ color:'white', fontWeight:'500' }]}>Out of stock</Text>
                                </TouchableOpacity>
                            </View>
                        {/* </View> */}
                    </View>
                    :
                    this.props.statusOrder == 1
                    ?
                    <View style={{ alignItems:'center', marginTop:10 }}>
                        {/* <TouchableOpacity onPress={()=>this.props.navigation.navigate('Track Order')} style={{ padding:5, backgroundColor:'#4FCA55', borderRadius:5 }}> */}
                        <TouchableOpacity onPress={onRefresh} style={{ padding:5, backgroundColor:'#4FCA55', borderRadius:5 }}>
                            <Text style={[styles.fontFamilyRegular, { color:'white' }]}>Ready to Send</Text>
                        </TouchableOpacity>
                    </View>
                    :
                    this.props.statusOrder == 4
                    ?
                    <View style={{ alignItems:'center', marginTop:10 }}>
                        <TouchableOpacity onPress={onDone} style={{ padding:5, backgroundColor:'#009ce5', borderRadius:5 }}>
                            <Text style={[styles.fontFamilyRegular, { color:'white' }]}>Received</Text>
                        </TouchableOpacity>
                    </View>
                    :
                    this.props.statusOrder == 5
                    ?
                    <View style={{ marginTop:10 }}>
                        <Text style={{ textAlign:'center' }}>Cancelled Order</Text>
                    </View>
                    :
                    this.props.statusOrder == 2
                    ?
                    null
                    // <View style={{ alignItems:'center', marginTop:10 }}>
                    //     <TouchableOpacity onPress={onDone} style={{ padding:5, backgroundColor:'#009ce5', borderRadius:5 }}>
                    //         <Text style={[styles.fontFamilyRegular, { color:'white' }]}>Received</Text>
                    //     </TouchableOpacity>
                    // </View>
                    // <View style={{ marginTop:10 }}>
                    //     <Text style={{ color:'transparent' }}>Test</Text>
                    // </View>
                    :
                    null
                    // <View style={{ alignItems:'center', marginTop:10 }}>
                    //     <View style={{ padding:5, backgroundColor:'white', borderRadius:5, shadowColor: "#000",shadowOffset: {width: 0,height: 2,},shadowOpacity: 0.25,shadowRadius: 3.84,elevation: 5, }}>
                    //         <Text style={[styles.fontFamilyRegular, { color:'black' }]}>Loading...</Text>
                    //     </View>
                    // </View>
            }
            </View>
            )
        } else {
            return(
                <View>
                    {/* {
                    this.state.loadingButton == true
                        ? */}
                        {/* <ActivityIndicator size="large" color="black" style={{ marginTop:10 }} /> */}
                        {/* : */}
                    <ActivityIndicator size="small" color="black" style={{ marginTop:10 }} />
                    {/* } */}
                </View>
            )
        }
    }
}

const styles = StyleSheet.create({
    fontFamilyRegular:{
        fontFamily:'Asap-Regular'
    },
    fw400:{
        fontWeight:'400'
    }
});

export default withNavigation(ButtonApproveDriver);