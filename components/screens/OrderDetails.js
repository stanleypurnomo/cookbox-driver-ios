import React, { Component } from 'react';
import { View, Image, Text, SafeAreaView } from 'react-native';
import { withNavigation } from 'react-navigation';
import { Header, withTheme } from 'react-native-elements';
import { TouchableOpacity, ScrollView } from 'react-native-gesture-handler';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import CountDown from 'react-native-countdown-component';
import MapView from 'react-native-maps';
import TrackingMaps from './TrackingMaps';
import Footer from './Footer';
import { PROVIDER_GOOGLE } from 'react-native-maps';

const orderProgress = '2';
var sec = new Date().getSeconds();
var date1 = new Date().getTime(); 
var date2 = new Date("05/24/2020 00:00:00").getTime(); 
var msDiff = date2 - date1;
var countdownValue = Math.floor((msDiff)/1000);

class OrderDetails extends React.Component{
    render(){
        return(
            <ScrollView style={{ paddingLeft:15, paddingRight:15, paddingTop:15, paddingBottom:15 }}>
                <View style={{ backgroundColor:'white', marginBottom:15, borderRadius:5, borderWidth:0.5, padding:20, borderColor:'#ccc', shadowColor: "#000",shadowOffset: {width: 0,height: 2,},shadowOpacity: 0.25,shadowRadius: 3.84,elevation: 5, }}>
                    <View style={{ flexDirection:'row', flex:1, paddingBottom:20 }}>
                        <View style={{ flex:0.5 }}>
                            <Text style={{ fontSize:18 }}>Great Seafood</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection:'row', flex:1, paddingBottom:5 }}>
                        <View style={{ flex:0.5 }}>
                            <Text style={{ fontSize:13, color:'#6b6b83' }}>Estimated Delivery time</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection:'row', flex:1, paddingBottom:20 }}>
                        <View style={{ flex:1 }}>
                            <Text style={{ fontSize:28, fontWeight:'bold', color:'black' }}>9:00pm-9:10pm</Text>
                        </View>
                    </View>
                    { orderProgress=== '0'  //order sent state progress bar
                        ?
                        <View style={{ flexDirection:'row', flex:1, paddingBottom:50, paddingLeft:5, paddingRight:5 }}>
                            <View style={{ flex:0.33, alignSelf:'center' }}>
                                <View style={{ position:'absolute', left:-2, top:-1.25, backgroundColor:'black', borderRadius:50, borderWidth:1, zIndex:99, width:12, height:12 }}></View>
                                <Text style={{ position:'absolute', top:15, left:-15, color:'#ff0018', fontWeight:'500' }}>Order sent</Text>
                                <View style={{ backgroundColor:'#e3e3e3', height:10, borderRadius:5 }}></View>
                            </View>
                            <View style={{ flex:0.33, alignSelf:'center' }}>
                                <View style={{ position:'absolute', left:-2, top:-1.25, backgroundColor:'black', borderRadius:50, borderWidth:1, zIndex:99, width:12, height:12 }}></View>
                                <Text style={{ position:'absolute', top:15, left:-30, color:'#ff0018', fontWeight:'500' }}>In the works</Text>
                                <View style={{ backgroundColor:'#e3e3e3', height:10, borderRadius:5 }}></View>
                            </View>
                            <View style={{ flex:0.33, alignSelf:'center' }}>
                                <View style={{ position:'absolute', left:-2, top:-1.25, backgroundColor:'black', borderRadius:50, borderWidth:1, zIndex:99, width:12, height:12 }}></View>
                                <Text style={{ position:'absolute', top:15, left:-20, color:'#ff0018', fontWeight:'500', width:60, textAlign:'center' }}>Out of delivery</Text>
                                <View style={{ backgroundColor:'#e3e3e3', height:10, borderRadius:5 }}></View>
                                <View style={{ position:'absolute', right:0, top:-1.25, backgroundColor:'black', borderRadius:50, borderWidth:1, zIndex:99, width:12, height:12 }}></View>
                                <Text style={{ position:'absolute', top:15, right:-20, color:'#ff0018', fontWeight:'500', textAlign:'center' }}>Delivered</Text>
                            </View>
                        </View>
                        :
                        <View></View>
                    }
                    { orderProgress=== '1'  //in the works state progress bar
                        ?
                        <View style={{ flexDirection:'row', flex:1, paddingBottom:50, paddingLeft:5, paddingRight:5 }}>
                            <View style={{ flex:0.33, alignSelf:'center' }}>
                                <View style={{ position:'absolute', left:-2, top:-1.25, backgroundColor:'black', borderRadius:50, borderWidth:1, zIndex:99, width:12, height:12 }}></View>
                                <Text style={{ position:'absolute', top:15, left:-15, color:'#ff0018', fontWeight:'500' }}>Order sent</Text>
                                <View style={{ backgroundColor:'#00a000', height:10, borderRadius:5 }}></View>
                            </View>
                            <View style={{ flex:0.33, alignSelf:'center' }}>
                                <View style={{ position:'absolute', left:-2, top:-1.25, backgroundColor:'black', borderRadius:50, borderWidth:1, zIndex:99, width:12, height:12 }}></View>
                                <Text style={{ position:'absolute', top:15, left:-30, color:'#ff0018', fontWeight:'500' }}>In the works</Text>
                                <View style={{ backgroundColor:'#e3e3e3', height:10, borderRadius:5 }}></View>
                            </View>
                            <View style={{ flex:0.33, alignSelf:'center' }}>
                                <View style={{ position:'absolute', left:-2, top:-1.25, backgroundColor:'black', borderRadius:50, borderWidth:1, zIndex:99, width:12, height:12 }}></View>
                                <Text style={{ position:'absolute', top:15, left:-20, color:'#ff0018', fontWeight:'500', width:60, textAlign:'center' }}>Out of delivery</Text>
                                <View style={{ backgroundColor:'#e3e3e3', height:10, borderRadius:5 }}></View>
                                <View style={{ position:'absolute', right:0, top:-1.25, backgroundColor:'black', borderRadius:50, borderWidth:1, zIndex:99, width:12, height:12 }}></View>
                                <Text style={{ position:'absolute', top:15, right:-20, color:'#ff0018', fontWeight:'500', textAlign:'center' }}>Delivered</Text>
                            </View>
                        </View>
                        :
                        <View></View>
                    }
                    { orderProgress=== '2'  //out of delivery state progress bar
                        ?
                        <View style={{ flexDirection:'row', flex:1, paddingBottom:50, paddingLeft:5, paddingRight:5}}>
                            <View style={{ flex:0.33, alignSelf:'center' }}>
                                <View style={{ position:'absolute', left:-2, top:-1.25, backgroundColor:'black', borderRadius:50, borderWidth:1, zIndex:99, width:12, height:12 }}></View>
                                <Text style={{ position:'absolute', top:15, left:-15, color:'#ff0018', fontWeight:'500' }}>Order sent</Text>
                                <View style={{ backgroundColor:'#00a000', height:10, borderRadius:5 }}></View>
                            </View>
                            <View style={{ flex:0.33, alignSelf:'center' }}>
                                <View style={{ position:'absolute', left:-2, top:-1.25, backgroundColor:'black', borderRadius:50, borderWidth:1, zIndex:99, width:12, height:12 }}></View>
                                <Text style={{ position:'absolute', top:15, left:-30, color:'#ff0018', fontWeight:'500' }}>In the works</Text>
                                <View style={{ backgroundColor:'#00a000', height:10, borderRadius:5 }}></View>
                            </View>
                            <View style={{ flex:0.33, alignSelf:'center' }}>
                                <View style={{ position:'absolute', left:-2, top:-1.25, backgroundColor:'black', borderRadius:50, borderWidth:1, zIndex:99, width:12, height:12 }}></View>
                                <Text style={{ position:'absolute', top:15, left:-20, color:'#ff0018', fontWeight:'500', width:60, textAlign:'center' }}>Out of delivery</Text>
                                <View style={{ backgroundColor:'#e3e3e3', height:10, borderRadius:5 }}></View>
                                <View style={{ position:'absolute', right:0, top:-1.25, backgroundColor:'black', borderRadius:50, borderWidth:1, zIndex:99, width:12, height:12 }}></View>
                                <Text style={{ position:'absolute', top:15, right:-20, color:'#ff0018', fontWeight:'500', textAlign:'center' }}>Delivered</Text>
                            </View>
                        </View>
                        :
                        <View></View>
                    }
                    { orderProgress=== '3'  //delivered state progress bar
                        ?
                        <View style={{ flexDirection:'row', flex:1, paddingBottom:50, paddingLeft:5, paddingRight:5 }}>
                            <View style={{ flex:0.33, alignSelf:'center' }}>
                                <View style={{ position:'absolute', left:-2, top:-1.25, backgroundColor:'black', borderRadius:50, borderWidth:1, zIndex:99, width:12, height:12 }}></View>
                                <Text style={{ position:'absolute', top:15, left:-15, color:'#ff0018', fontWeight:'500' }}>Order sent</Text>
                                <View style={{ backgroundColor:'#00a000', height:10, borderRadius:5 }}></View>
                            </View>
                            <View style={{ flex:0.33, alignSelf:'center' }}>
                                <View style={{ position:'absolute', left:-2, top:-1.25, backgroundColor:'black', borderRadius:50, borderWidth:1, zIndex:99, width:12, height:12 }}></View>
                                <Text style={{ position:'absolute', top:15, left:-30, color:'#ff0018', fontWeight:'500' }}>In the works</Text>
                                <View style={{ backgroundColor:'#00a000', height:10, borderRadius:5 }}></View>
                            </View>
                            <View style={{ flex:0.33, alignSelf:'center' }}>
                                <View style={{ position:'absolute', left:-2, top:-1.25, backgroundColor:'black', borderRadius:50, borderWidth:1, zIndex:99, width:12, height:12 }}></View>
                                <Text style={{ position:'absolute', top:15, left:-20, color:'#ff0018', fontWeight:'500', width:60, textAlign:'center' }}>Out of delivery</Text>
                                <View style={{ backgroundColor:'#00a000', height:10, borderRadius:5 }}></View>
                                <View style={{ position:'absolute', right:0, top:-1.25, backgroundColor:'black', borderRadius:50, borderWidth:1, zIndex:99, width:12, height:12 }}></View>
                                <Text style={{ position:'absolute', top:15, right:-20, color:'#ff0018', fontWeight:'500', textAlign:'center' }}>Delivered</Text>
                            </View>
                        </View>
                        :
                        <View></View>
                    }
                    <View style={{ flexDirection:'row', flex:1, paddingBottom:10 }}>
                        <Text>Full Page View</Text>
                    </View>
                    <View style={{ flexDirection:'row', flex:1 }}>
                        {/* <Image style={{ width:wp('82.5%'), height:200 }} source={require('../images/dummy/maps_dummy.png')}/> */}
                        {/* <MapView
                            initialRegion={{
                            latitude: 37.78825,
                            longitude: -122.4324,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421,
                            }}
                            provider={PROVIDER_GOOGLE}
                            style={{ width:wp('80%'), height:300 }}
                        /> */}
                        <TrackingMaps/>
                    </View>
                </View>
                <View style={{ backgroundColor:'white', marginBottom:50, borderRadius:5, borderWidth:0.5, padding:20, borderColor:'#ccc', shadowColor: "#000",shadowOffset: {width: 0,height: 2,},shadowOpacity: 0.25,shadowRadius: 3.84,elevation: 5, }}>
                    <View style={{ flexDirection:'row', flex:1, paddingBottom:20 }}>
                        <View style={{ flex:0.5 }}>
                            <Text style={{ fontSize:18 }}>Great Seafood</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection:'row', flex:1, paddingBottom:5 }}>
                        <View style={{ flex:0.5 }}>
                            <Text style={{ fontSize:13, color:'#6b6b83' }}>Estimated Delivery time</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection:'row', flex:1, paddingBottom:5, marginBottom:50 }}>
                        <View style={{ flex:1, height:50 }}>
                            <CountDown
                                size={30}
                                until={countdownValue}
                                running={true}
                                // onFinish={() => alert('Finished')}
                                digitStyle={{backgroundColor: '#ff0018', marginRight:20, width:80}}
                                digitTxtStyle={{color: 'white', top:-10}}
                                timeLabelStyle={{color: 'white', fontWeight: 'bold', marginLeft:-15, top:-30, fontSize:15}}
                                separatorStyle={{color: '#1CC625', marginRight:20}}
                                timeToShow={['H', 'M', 'S']}
                                timeLabels={{h:'Hours', m: 'Minutes', s: 'Seconds'}}
                                style={{ paddingTop:10, paddingBottom:10, alignSelf:'flex-start', height:100 }}
                            />
                            {/* <Text>{this.state.date}</Text> */}
                            {/* <Text>{countdownValue}</Text> */}
                        </View>
                    </View>
                    <View style={{ flexDirection:'row', flex:1 }}>
                        <View style={{ width:wp('80%'), height:0.75, backgroundColor:'#acacac', marginBottom:15 }}></View>
                    </View>
                    <View style={{ flexDirection:'row', flex:1, paddingBottom:25 }}>
                        <View style={{ flexDirection:'column', flex:1 }}>
                            <Text style={{ color:'#000000de', paddingBottom:20, fontWeight:'600', fontSize:18 }}>Delivery (ASAP) to:</Text>
                            <Text style={{ fontSize:15, color:'#6b6b83', paddingBottom:5 }}>Mr. Rynaldo Wahyudi</Text>
                            <Text style={{ fontSize:15, color:'#6b6b83', paddingBottom:5 }}>Home</Text>
                            <Text style={{ fontSize:15, color:'#6b6b83', paddingBottom:5 }}>314 79th St</Text>
                            <Text style={{ fontSize:15, color:'#6b6b83', paddingBottom:5 }}>Rite Aid, Brisbane, AU, 11209</Text>
                            <Text style={{ fontSize:15, color:'#6b6b83', paddingBottom:5 }}>(347) 123456789</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection:'row', flex:1, marginBottom:15 }}>
                        <View style={{ flexDirection:'column', flex:1 }}>
                            <Text style={{ color:'#000000de', paddingBottom:15, fontWeight:'600', fontSize:18 }}>Delivery instructions</Text>
                            <Text style={{ fontSize:15, color:'#6b6b83', paddingBottom:15, lineHeight:24 }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua consectetur adipiscing elit.</Text>
                            <Image style={{ flexDirection:'row', width:'100%', height:150, resizeMode:'contain' }} source={{ uri:'http://rizkyserra.000webhostapp.com/assets/img/details/banner.jpg' }}/>
                        </View>
                    </View>
                    <View style={{ flexDirection:'row', flex:1 }}>
                        <View style={{ width:'100%', height:0.75, backgroundColor:'#acacac', marginBottom:15 }}></View>
                    </View>
                    <View style={{ flexDirection:'row', flex:1 }}>
                        <View style={{ flexDirection:'column', flex:1 }}>
                            <View style={{ flexDirection:'row', flex:1, paddingBottom:10 }}>
                                <View style={{ flex:0.5 }}>
                                    <Text style={{ fontSize:18 }}>Your Order</Text>
                                </View>
                                <View style={{ flex:0.5, alignItems:'flex-end' }}>
                                    <TouchableOpacity>
                                        <Text style={{ lineHeight:21, color:'#ff0018' }}>Print receipt</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={{ flexDirection:'column', flex:1, paddingBottom:10 }}>
                                <Text style={{ fontSize:15, color:'#6b6b83', paddingBottom:5 }}>Apr 15, 2020 8:38pm</Text>
                                <Text style={{ fontSize:15, color:'#2c2c2c', paddingBottom:5 }}>Order #123456789012345</Text>
                            </View>
                            <View style={{ flexDirection:'column', flex:1 }}>
                                <View style={{ flexDirection:'row', flex:1, paddingBottom:15 }}>
                                    <View style={{ flex:0.2 }}>
                                        <Image  style={{ width:'100%',height:70, resizeMode:'contain' }}
                                            source={{ uri:'http://rizkyserra.000webhostapp.com/assets/img/details/1.jpg' }}
                                        />
                                    </View>
                                    <View style={{ flex:0.2 }}>
                                        <Text style={{ textAlign:'center' }}>2</Text>
                                    </View>
                                    <View style={{ flex:0.4 }}>
                                        <Text>SeaBox Essential A</Text>
                                    </View>
                                    <View style={{ flex:0.2 }}>
                                        <Text style={{ textAlign:'right' }}>$20.99</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection:'row', flex:1, paddingBottom:15 }}>
                                    <View style={{ flex:0.2 }}>
                                        <Image  style={{ width:'100%',height:70, resizeMode:'contain' }}
                                            source={{ uri:'http://rizkyserra.000webhostapp.com/assets/img/details/1.jpg' }}
                                        />
                                    </View>
                                    <View style={{ flex:0.2 }}>
                                        <Text style={{ textAlign:'center' }}>2</Text>
                                    </View>
                                    <View style={{ flex:0.4 }}>
                                        <Text>SeaBox Essential A</Text>
                                    </View>
                                    <View style={{ flex:0.2 }}>
                                        <Text style={{ textAlign:'right' }}>$20.99</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection:'row', flex:1, paddingBottom:15 }}>
                                    <View style={{ flex:0.2 }}>
                                        <Image  style={{ width:'100%',height:70, resizeMode:'contain' }}
                                            source={{ uri:'http://rizkyserra.000webhostapp.com/assets/img/details/1.jpg' }}
                                        />
                                    </View>
                                    <View style={{ flex:0.2 }}>
                                        <Text style={{ textAlign:'center' }}>2</Text>
                                    </View>
                                    <View style={{ flex:0.4 }}>
                                        <Text>SeaBox Essential A</Text>
                                    </View>
                                    <View style={{ flex:0.2 }}>
                                        <Text style={{ textAlign:'right' }}>$20.99</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection:'row', flex:1, paddingBottom:15 }}>
                                    <View style={{ flex:0.2 }}>
                                        <Image  style={{ width:'100%',height:70, resizeMode:'contain' }}
                                            source={{ uri:'http://rizkyserra.000webhostapp.com/assets/img/details/1.jpg' }}
                                        />
                                    </View>
                                    <View style={{ flex:0.2 }}>
                                        <Text style={{ textAlign:'center' }}>2</Text>
                                    </View>
                                    <View style={{ flex:0.4 }}>
                                        <Text>SeaBox Essential A</Text>
                                    </View>
                                    <View style={{ flex:0.2 }}>
                                        <Text style={{ textAlign:'right' }}>$20.99</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection:'row', flex:1, paddingBottom:15 }}>
                                    <View style={{ flex:0.2 }}>
                                        <Image  style={{ width:'100%',height:70, resizeMode:'contain' }}
                                            source={{ uri:'http://rizkyserra.000webhostapp.com/assets/img/details/1.jpg' }}
                                        />
                                    </View>
                                    <View style={{ flex:0.2 }}>
                                        <Text style={{ textAlign:'center' }}>2</Text>
                                    </View>
                                    <View style={{ flex:0.4 }}>
                                        <Text>SeaBox Essential A</Text>
                                    </View>
                                    <View style={{ flex:0.2 }}>
                                        <Text style={{ textAlign:'right' }}>$20.99</Text>
                                    </View>
                                </View>
                                <View style={{ width:'100%', height:0.75, backgroundColor:'#acacac', marginBottom:15 }}></View>
                            </View>
                            <View style={{ flexDirection:'column', flex:1 }}>
                                <View style={{ flexDirection:'row', flex:1, paddingBottom:15 }}>
                                    <Text style={{ fontSize:18, fontWeight:'600' }}>Payment Method</Text>
                                </View>
                                <View style={{ flexDirection:'row', flex:1, paddingBottom:25 }}>
                                    <View style={{ flex:0.2, flexDirection:'row', alignItems:'flex-start' }}>
                                        <Image style={{ width:'75%', height:50, resizeMode:'stretch' }} source={require('../images/logo-banner/outline_payment_black_18dp.png')}/>
                                    </View>
                                    <View style={{ flex:0.3, alignSelf:'center' }}>
                                        <Text style={{ }}>Paypal</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection:'row', flex:1, paddingBottom:5 }}>
                                    <View style={{ flex:0.5 }}>
                                        <Text>Item subtotal:</Text>
                                    </View>
                                    <View style={{ flex:0.5, alignItems:'flex-end' }}>
                                        <Text>$30.5</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection:'row', flex:1, paddingBottom:5 }}>
                                    <View style={{ flex:0.5 }}>
                                        <Text>Delivery fee:</Text>
                                    </View>
                                    <View style={{ flex:0.5, alignItems:'flex-end' }}>
                                        <Text>$30.5</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection:'row', flex:1, paddingBottom:5 }}>
                                    <View style={{ flex:0.5 }}>
                                        <Text>Tax and fees:</Text>
                                    </View>
                                    <View style={{ flex:0.5, alignItems:'flex-end' }}>
                                        <Text>$30.5</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection:'row', flex:1, paddingBottom:5 }}>
                                    <View style={{ flex:0.5 }}>
                                        <Text>Driver tip:</Text>
                                    </View>
                                    <View style={{ flex:0.5, alignItems:'flex-end' }}>
                                        <Text>$30.5</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection:'row', paddingTop:10, paddingBottom:5 }}>
                                    <View style={{ width:'100%', height:0.75, backgroundColor:'#acacac', marginBottom:15 }}></View>
                                </View>
                                <View style={{ flexDirection:'row', flex:1 }}>
                                    <View style={{ flex:0.5 }}>
                                        <Text style={{ fontSize:18, fontWeight:'600' }}>Total:</Text>
                                    </View>
                                    <View style={{ flex:0.5 }}>
                                        <Text style={{ fontSize:18, fontWeight:'600', textAlign:'right' }}>$33.36</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection:'row', flex:1, alignSelf:'center', paddingTop:15 }}>
                                    <TouchableOpacity style={{ alignSelf:'center', width:'100%', borderRadius:10, borderWidth:0.75, borderColor:'#acacac', paddingLeft:15, paddingRight:15, paddingTop:10, paddingBottom:10 }}>
                                        <Text style={{ color:'#ff0018', fontWeight:'600' }}>Need Help?</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={{ marginLeft:-15, marginRight:-15 }}>
                    <Footer/>
                </View>
            </ScrollView>
        );
    }
}

export default withNavigation(OrderDetails);