import React, { Component } from 'react';
import { View, Text, StatusBar, TouchableOpacity, StyleSheet, KeyboardAvoidingView, Platform } from 'react-native';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import Textarea from 'react-native-textarea';
import { withNavigation } from 'react-navigation';

class OutOfStock extends React.Component{
    returnHome(){
        alert('Thanks');
        this.props.navigation.reset({
            index: 0,
            routes: [
                {   name: 'Home',
                    // params:{id: id},
                }
            ]
        });
    }
    render(){
        return(
            <View style={{ flexDirection:'row' }}>
                    <StatusBar barStyle="dark-content"/>
                    <Text>{this.props.id}</Text>
                    <ScrollView style={{ paddingTop:20, paddingLeft:30, paddingRight:30, width:'100%' }}>
                    <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : null}>
                        <View>
                        <View style={{ flexDirection:'row', flex:0.05, marginBottom:15 }}>
                            <View style={{ flex:0.175 }}>
                                <Text style={[styles.fontFamilyRegular, { fontSize:17 }]}>Status:</Text>
                            </View>
                            <View style={{ flex:0.2 }}>
                                <View style={{ paddingBottom:2.5, paddingTop:2.5, top:-2.5, backgroundColor:'#4FCA55', 
                                                alignItems:'center', borderRadius:5 }}>
                                    <Text style={[styles.fontFamilyRegular, { fontSize:16, color:'white', fontWeight:'600' }]}>Active</Text>
                                </View>
                            </View>
                        </View>
                        <View style={{ flexDirection:'row', flex:0.05, marginBottom:20 }}>
                            <View style={{ flex:1 }}>
                                <Text style={styles.fontFamilyRegular}>You have <Text style={{ color:'red' }}>4</Text> order</Text>
                            </View>
                        </View>
                        <View style={{ marginBottom:15, borderColor:'#ccc', borderWidth:0.5, borderRadius:5, padding:15,
                        backgroundColor:'white', shadowColor: "#000",
                        shadowOffset: {width: 0,height: 2,},
                        shadowOpacity: 0.25,shadowRadius: 2.25,
                        elevation: 5,}}>
                        <Text style={[styles.fontFamilyRegular, { fontSize:16, textAlign:'center', color:'red', marginBottom:15 }]}>ORDER CANCEL</Text>
                        <Text style={[styles.fontFamilyRegular, { fontSize:12, color:'red', marginBottom:5 }]}>Rynaldo Wahyudi</Text>
                        <View style={{ marginBottom:25, height:25 }}>
                            <Text style={[styles.fontFamilyRegular, { fontSize:15 }]}>1x SeaBox Prawn</Text>
                            <Text style={[styles.fontFamilyRegular, { fontSize:15 }]}>1x Dimsim Seafood</Text>
                        </View>
                        <View style={{ flexDirection:'row' }}>
                            <View style={{ flex:0.35}}>
                                    <Text style={[styles.fontFamilyRegular, { color:'blue', fontWeight:'700', fontSize:16 }]}>Destination</Text>
                            </View>
                            <View style={{ flex:0.0325}}>
                                <Text>:</Text>
                            </View>
                            <View style={{ flex:0.6 }}>
                                <Text style={[styles.orderDetailsFont, styles.fontFamilyRegular]}>333 Swann Road</Text>
                            </View>
                        </View>
                        <View style={{ flexDirection:'row' }}>
                            <View style={{ flex:0.35 }}>
                                    <Text style={[styles.fontFamilyRegular, { color:'blue', fontWeight:'700', fontSize:16 }]}>City</Text>
                            </View>
                            <View style={{ flex:0.0325}}>
                                <Text>:</Text>
                            </View>
                            <View style={{ flex:0.6 }}>
                                <Text style={[styles.orderDetailsFont, styles.fontFamilyRegular]}>St Lucia</Text>
                            </View>
                        </View>
                        <View style={{ flexDirection:'row' }}>
                            <View style={{ flex:0.35 }}>
                                    <Text style={[styles.fontFamilyRegular, { color:'blue', fontWeight:'700', fontSize:16 }]}>State</Text>
                            </View>
                            <View style={{ flex:0.0325}}>
                                <Text>:</Text>
                            </View>
                            <View style={{ flex:0.6 }}>
                                <Text style={[styles.orderDetailsFont, styles.fontFamilyRegular]}>Queensland</Text>
                            </View>
                        </View>
                        <View style={{ flexDirection:'row' }}>
                            <View style={{ flex:0.35 }}>
                                    <Text style={[styles.fontFamilyRegular, { color:'blue', fontWeight:'700', fontSize:16 }]}>Zip Code</Text>
                            </View>
                            <View style={{ flex:0.0325}}>
                                <Text>:</Text>
                            </View>
                            <View style={{ flex:0.6 }}>
                                <Text style={[styles.orderDetailsFont, styles.fontFamilyRegular]}>4067</Text>
                            </View>
                        </View>
                        <View style={{ flexDirection:'row' }}>
                            <View style={{ flex:0.35 }}>
                                    <Text style={[styles.fontFamilyRegular, { color:'blue', fontWeight:'700', fontSize:16 }]}>Phone</Text>
                            </View>
                            <View style={{ flex:0.0325}}>
                                <Text>:</Text>
                            </View>
                            <View style={{ flex:0.6 }}>
                                <Text style={[styles.orderDetailsFont, styles.fontFamilyRegular]}>+61413226610</Text>
                            </View>
                        </View>
                        <View style={{ marginTop:10, marginBottom:10 }}>
                            <View style={{ flexDirection:'row' }}>
                            <Text style={[styles.fontFamilyRegular, { flex:0.35, marginBottom:5 ,color:'blue', fontWeight:'700', fontSize:16 }]}>Note</Text>
                            <Text>:</Text>
                            </View>
                
                            <Textarea
                                containerStyle={styles.textareaContainer}
                                style={{ fontSize:16,fontFamily:'Asap-Regular', borderColor:'#ff0018', borderWidth:1, height:'100%', padding:10, borderRadius:5}}
                                // onChangeText=
                                defaultValue=''
                                // maxLength={120}
                                placeholder={'Please fill the notes'}
                                placeholderTextColor={'#c7c7c7'}
                                // underlineColorAndroid={'transparent'}
                            />
                        </View>
                        <View style={{ flexDirection:'row', alignSelf:'center', flex:0.5 }}>
                            <View style={{ flex:0.3 }}>
                            <TouchableOpacity onPress={()=>this.returnHome()} style={{ paddingBottom:2.5, paddingTop:2.5, top:-2.5, backgroundColor:'#4FCA55', alignItems:'center', borderRadius:5 }}>
                                <Text style={[styles.fontFamilyRegular, { fontSize:16, color:'white', fontWeight:'600' }]}>Done</Text>
                            </TouchableOpacity>
                            </View>
                        </View>
                        </View>
                        </View>
                        </KeyboardAvoidingView>
                    </ScrollView>
                </View>
        )
    }
}

    const styles = StyleSheet.create({
        fontFamilyRegular:{
            fontFamily:'Asap-Regular'
        },
        orderDetailsFont:{
            fontSize:16,
        },
    })

export default withNavigation(OutOfStock);