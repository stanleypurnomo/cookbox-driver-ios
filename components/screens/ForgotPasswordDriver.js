import React, { Component } from 'react';
import { View, Image, Text, Dimensions, StyleSheet, Linking, StatusBar, 
        KeyboardAvoidingView, Keyboard, TouchableWithoutFeedback, 
        AsyncStorage, ActivityIndicator, Alert } from 'react-native';
import { withNavigation, SafeAreaView } from 'react-navigation';
import { TouchableOpacity, ScrollView, TextInput } from 'react-native-gesture-handler';
import { getStatusBarHeight } from 'react-native-status-bar-height';
import { CommonActions } from '@react-navigation/native';
import { KeyboardAccessoryView, KeyboardAccessoryNavigation } from 'react-native-keyboard-accessory';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import DoneButton from 'react-native-keyboard-done-button';
// import { asin } from 'react-native-reanimated';

var statusBarHeight = getStatusBarHeight();
var isCorrected = null;

const DismissKeyboard = ({ children }) => (
    <TouchableWithoutFeedback 
    onPress={() => Keyboard.dismiss()}>
        {children}
    </TouchableWithoutFeedback>
);
class ForgotPasswordDriver extends Component{
    constructor(){
        super();
        this.state = {
            username:'',
            password:'',
            isLogin: false,
            isLoading: false,
            token:'',
            email:'',
            wrongEmail:'',
        };
        const createTwoButtonAlert = () =>
            Alert.alert(
                "Alert Title",
                "My Alert Msg",
                [
                    {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                    },
                    { text: "OK", onPress: () => console.log("OK Pressed") }
                ],
            { cancelable: false }
        );
        this.postEmail = this.postEmail.bind(this);
        // this.validate = this.validate.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
        // this.state.isLogin = this.state.isLogin.bind(this);
    }       
      componentDidMount(){
        if(this.state.isLogin === true){
            this.props.navigation.navigate('Home');
        }
      }

      fetchAuthData = async () =>{
        try {
            const tokenVal = await AsyncStorage.getItem('token');
            console.log(tokenVal);
            this.setState({
                token:tokenVal
            });
        } catch (error) {
            console.log(error);
        }
        var request = {
            method: 'GET',
            headers: {
                'Content-type': 'application/json',
                'Authorization': this.state.token
            },
        };
        fetch('http://seafoodau.herokuapp.com/api/v1/auth/data', request)
        .then((response) => response.json())
        .then((responseJson)=>
        {
            console.log(responseJson);
            if(responseJson.status == '200'){
                console.log(responseJson.data.fullname);
                AsyncStorage.setItem('username',responseJson.data.username);
                AsyncStorage.setItem('fullname',responseJson.data.fullname);
                AsyncStorage.setItem('id',responseJson.data.id);
            } else {
                console.log('Error getting data');
                // alert("Wrong Username or Password");
                // this.setState({
                //     isLoading: false,
                // });
            }
        })
        .catch((error)=>{
            console.error(error);
        });
      }

    validateEmail = email => {
        var re = /^(([^<>()\[\]\\.,;:\s@”]+(\.[^<>()\[\]\\.,;:\s@”]+)*)|(“.+”))@((\[[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}\.[0–9]{1,3}])|(([a-zA-Z\-0–9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };

    onSubmitEditingEmail(){
        if(isCorrected == true){
            this.setState({
                checkEmail: true,
            })
        } else {
            this.setState({
                checkEmail: false,
            })
        }
        Keyboard.dismiss;
    }

    validate = (text) => {
        console.log(text);
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(text) === false) {
          console.log("Email is Not Correct");
          this.setState({ email: text.toLowerCase() })
          return false;
        }
        else {
          this.setState({ email: text.toLowerCase() })
          console.log("Email is Correct");
        }
      }

    validateAfterEditing(){
        var emailInput = this.state.email;
        if(emailInput != ''){
            let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            if (reg.test(emailInput) === false) {
                console.log("Email not Corrected");
                // this.setState({ email: text })
                // return false;
                isCorrected = false;
                console.log(isCorrected);
            }
            else {
            // this.setState({ email: text })
                console.log("Email Corrected");
                isCorrected = true;
                console.log(isCorrected);
            }
        }    
    }

      postEmail = () =>{
        const {email} = this.state;
        // alert(username+','+password);
        // this.props.navigation.navigate('Home');
        //  API Login Function
        var params = {
            email: email,
        };
        
        var formData = new FormData();
        
        for (var k in params) {
            formData.append(k, params[k]);
        }

        console.log(formData);

        var request = {
            method: 'POST',
            headers: {
                'Content-type': 'application/x-www-form-urlencoded',
            },
            body: formData
        };

        var thisState = this

        this.setState({
            isLoading: true
        });
        
        if(isCorrected==true){
            // setTimeout(function(){alert('Reset confirmation email has been sent to '+email.toLowerCase());thisState.setState({isLoading: false,})},1000)
            setTimeout(function(){Alert.alert( "Email Confirmation Send", "Reset confirmation email has been sent to "+email.toLowerCase(), [ { text: "Cancel", onPress: () => console.log("Cancel Pressed"), style: "cancel" }, { text: "OK", onPress: () => console.log("OK Pressed") } ], { cancelable: false }); thisState.setState({isLoading: false,})},1000)
            this.setState({
                wrongEmail: false
            })
        } else{
            setTimeout(function(){thisState.setState({wrongEmail:true,isLoading:false,})}, 1000);
        }
        // fetch('http://seafoodau.herokuapp.com/api/v1/auth/login', request)
        // .then((response) => response.json())
        // .then((responseJson)=>
        // {
        //     console.log(responseJson);
        //     if(responseJson.status == '200'){
        //         AsyncStorage.setItem('token',responseJson.token);
        //         this.fetchAuthData();
        //         alert("Login successful");
        //         this.props.navigation.dispatch(
        //             CommonActions.reset({
        //                 index: 0,
        //                 routes: [
        //                 { name: 'Home' },
        //                 // {
        //                 //   name: 'Profile',
        //                 //   params: { user: 'jane' },
        //                 // },
        //                 ],
        //             })
        //         );
        //         AsyncStorage.setItem('isLogin','true');
        //     } else {
        //         alert("Wrong Username or Password");
        //         this.setState({
        //             isLoading: false,
        //         });
        //     }
        // })
        // .catch((error)=>{
        //     console.error(error);
        // });

        // fetch('http://seafoodau.herokuapp.com/api/v1/auth/login',{
        //     method:'post',
        //     header:{
        //         'Accept': '*/*',
        //         'Content-type': 'application/json',
        //         'Connection': 'keep-alive',
        //         'User-Agent': 'PostmanRuntime/7.25.0'

        //     },
        //     body:formData,
        // })
        // .then((response) => response.json())
        //     .then((responseJson)=>{
        //         console.log(responseJson.msg);
        //         if(responseJson == "ok"){
        //             alert("Login successful");
        //             this.props.navigation.dispatch(
        //                 CommonActions.reset({
        //                   index: 0,
        //                   routes: [
        //                     { name: 'Home' },
        //                     // {
        //                     //   name: 'Profile',
        //                     //   params: { user: 'jane' },
        //                     // },
        //                   ],
        //                 })
        //             );
        //             AsyncStorage.setItem('isLogin','true');
        //         } else {
        //             alert("Wrong Username or Password");
        //         }
        //     })
        //     .catch((error)=>{
        //         console.error(error);
        //     });
        Keyboard.dismiss();


    //   this.state.isLogin = true;
      // if(this.state.isLogin===true){
      //     this.setState({
      //       isLogin: false,
      //     });
      //     console.log(this.state.isLogin);
      // }
    //   if(this.state.isLogin === true){
    //       this.props.navigation.navigate('Home');
    //     }
      }
      render(){
        return(
        <DismissKeyboard>
        <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : null}>
            {
                this.state.isLoading === true
                ?
                <View style={{ position:'absolute', top:'50%', zIndex:999, alignSelf:'center' }}>
                    <View style={{ backgroundColor:'white', borderWidth:1, borderColor:'#ccc', 
                    alignSelf:'center', padding:20, shadowColor: "#000",
                    shadowOffset: {width: 0,height: 2,},
                    shadowOpacity: 0.25,shadowRadius: 3.84,elevation: 5, }}>
                        <ActivityIndicator size="large" color="black" />
                    </View>
                    {/* <Text>Is Loading: true</Text> */}
                    {/* show loading spinner */}
                </View>
                :
                null
            }
          <SafeAreaView style={{ height:'100%', backgroundColor:'white' }}>
            <StatusBar barStyle="dark-content"/>
            <View style={{ flexDirection:'column', flex:1}}>
                <View style={{ flex:0.5 }}>
                    <Image  source={require('../../components/images/logo-banner/background1new.png')}
                        style={{ height:640, width:'100%', zIndex:1, resizeMode:'cover' }}/>
                </View>
                <View style={{ alignSelf:'center', position:'absolute', zIndex:9, top:statusBarHeight+20 }}>
                {/* <Text>AAA</Text> */}
                    <View style={{ alignSelf:'center' }}>
                        <Image source={require('../images/logo-banner/Transparent_background_white.png')} 
                            style={{ width:200, height:150, resizeMode:'contain', marginBottom:25 }}
                        />
                    </View>
                {/* <Image source={require('../../components/images/logo-banner/White.png')}
                        style={{ zIndex:9, height:175, width:175, resizeMode:'contain' }}/> */}
                </View>
            </View>  
            <View style={{ flexDirection:'column', alignItems:'center', flex:1, backgroundColor:'white', paddingTop:15, paddingBottom:15 }}>
               <View style={{ flexDirection:'column', marginBottom:20, paddingLeft:5, paddingRight:5, width:'80%', flex:0.5 }}>
                    {/* <View style={{ flex:1, alignItems:'center' }}> */}
                        <Text style={{ lineHeight:36, fontFamily:'Asap-Regular', fontSize:18, textAlign:'center' }}>Enter Your Email</Text>
                    {/* </View> */}
                    {/* <View style={{ paddingLeft:15, paddingRight:15, flex:1, alignItems:'flex-start' }}> */}
                            {/* <DoneButton
                            title="Done!"   //not required, default value = `Done`
                            style={{ backgroundColor: 'red' }}  //not required
                            doneStyle={{ color: 'green' }}  //not requireds
                            /> */}
                            <View style={{ flex:0.5 }}>
                                <TextInput style={{ textAlign:'center', fontFamily:'Asap-Regular', padding:7.5, borderColor:'#FF2D00', borderRadius:5, borderWidth:1, height:36 }}
                                autoCompleteType="email"
                                returnKeyType={ 'next' } autoCapitalize='none' onSubmitEditing={Keyboard.dismiss} autoCorrect={true} onBlur={this.validateAfterEditing()} onChangeText={(text) => this.validate(text)}/>
                                { this.state.wrongEmail === true
                                    ?
                                    <Text>Email Wrong</Text>
                                    // : this.state.wrongEmail === false
                                    // ?
                                    // <Text>Email Corrected</Text>
                                    :
                                    null
                                }
                            </View>
                    {/* </View> */}
                </View>
                <View style={{ flex:0.5 }}>
                    <TouchableOpacity onPress={this.postEmail} style={{ borderRadius:5, width:200, backgroundColor:'#FF2D00', padding:10, alignItems:'center' }}>
                        <Text style={{ color:'white',fontSize:18, fontWeight:'600' }}>Go</Text>
                    </TouchableOpacity>
                </View>
            </View>    
            {/* <Button title="Click here Log out" onPress={this.logout}></Button>
            <Text>{String(this.state.isLogin)}</Text> */}
          </SafeAreaView>
        </KeyboardAvoidingView>
        </DismissKeyboard>
        )
    }
}

export default withNavigation(ForgotPasswordDriver);