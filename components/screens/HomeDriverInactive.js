import React, { Component } from 'react';
import { View, Image, Text, Dimensions, StyleSheet, Button, StatusBar } from 'react-native';
import { withNavigation, SafeAreaView } from 'react-navigation';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faChevronUp, faChevronDown, faChevronCircleLeft, faChevronCircleRight, faTrash, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { ScrollView, TouchableHighlight, TouchableOpacity } from 'react-native-gesture-handler';

var isLogin = 'true';

class HomeDriverInactive extends React.Component{
    render(){
        return(
            <View style={{ backgroundColor:'white', flexDirection:'column', flex:1, paddingTop:25, paddingLeft:15, paddingRight:15, }}>
                <StatusBar barStyle="dark-content"/>
                <View style={{ alignSelf:'center' }}>
                    <Image source={require('../images/logo-banner/Transparentbackgroundnew.png')} 
                        style={{ width:200, height:150, resizeMode:'contain', marginBottom:25 }}
                    />
                </View>
                <View style={{ flexDirection:'row', flex:0.075, marginBottom:5 }}>
                    <View style={{ flex:0.175 }}>
                        <Text style={[styles.fontFamilyRegular, { fontSize:17, lineHeight:25 }]}>Status:</Text>
                    </View>
                    <View style={{ flex:0.2 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={{ paddingBottom:2.5, paddingTop:2.5, backgroundColor:'#ff0018', 
                                        alignItems:'center', borderRadius:5 }}>
                            <Text style={[styles.fontFamilyRegular, { fontSize:16, color:'white', fontWeight:'600' }]}>Off</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ flexDirection:'row', flex:0.075 }}>
                    <View style={{ flex:1 }}>
                        <Text style={styles.fontFamilyRegular}>You will be back to work on ...</Text>
                    </View>
                </View>
                <View style={{ flex:0.725, marginLeft:-5, marginRight:-5 }}>
                    <ScrollView style={{ width:'100%'}}>
                        <View style={{ marginBottom:15, borderColor:'#ccc', borderWidth:0.5, borderRadius:5, padding:15,
                            backgroundColor:'white', shadowColor: "#000",
                            shadowOffset: {width: 0,height: 2,},
                            shadowOpacity: 0.25,shadowRadius: 2.25,
                            elevation: 5, }}>
                            <Text style={[styles.fontFamilyRegular, { fontSize:12, color:'red', marginBottom:5 }]}>Sunday</Text>
                            <View style={{ marginBottom:5 }}>
                                <Text style={[styles.fontFamilyRegular, { fontSize:15 }]}>06.00 AM - 19.00 PM</Text>
                            </View>
                        </View>
                        <View style={{ marginBottom:15, borderColor:'#ccc', borderWidth:0.5, borderRadius:5, padding:15,
                            backgroundColor:'white', shadowColor: "#000",
                            shadowOffset: {width: 0,height: 2,},
                            shadowOpacity: 0.25,shadowRadius: 2.25,
                            elevation: 5, }}>
                            <Text style={[styles.fontFamilyRegular, { fontSize:12, color:'red', marginBottom:5 }]}>Monday</Text>
                            <View style={{ marginBottom:5 }}>
                                <Text style={[styles.fontFamilyRegular, { fontSize:15 }]}>06.00 AM - 19.00 PM</Text>
                            </View>
                        </View>
                        <View style={{ marginBottom:15, borderColor:'#ccc', borderWidth:0.5, borderRadius:5, padding:15,
                            backgroundColor:'white', shadowColor: "#000",
                            shadowOffset: {width: 0,height: 2,},
                            shadowOpacity: 0.25,shadowRadius: 2.25,
                            elevation: 5, }}>
                            <Text style={[styles.fontFamilyRegular, { fontSize:12, color:'red', marginBottom:5 }]}>Tuesday</Text>
                            <View style={{ marginBottom:5 }}>
                                <Text style={[styles.fontFamilyRegular, { fontSize:15 }]}>06.00 AM - 19.00 PM</Text>
                            </View>
                        </View>
                        <View style={{ marginBottom:15, borderColor:'#ccc', borderWidth:0.5, borderRadius:5, padding:15,
                            backgroundColor:'white', shadowColor: "#000",
                            shadowOffset: {width: 0,height: 2,},
                            shadowOpacity: 0.25,shadowRadius: 2.25,
                            elevation: 5, }}>
                            <Text style={[styles.fontFamilyRegular, { fontSize:12, color:'red', marginBottom:5 }]}>Wednesday</Text>
                            <View style={{ marginBottom:5 }}>
                                <Text style={[styles.fontFamilyRegular, { fontSize:15 }]}>06.00 AM - 19.00 PM</Text>
                            </View>
                        </View>
                        <View style={{ marginBottom:15, borderColor:'#ccc', borderWidth:0.5, borderRadius:5, padding:15,
                            backgroundColor:'white', shadowColor: "#000",
                            shadowOffset: {width: 0,height: 2,},
                            shadowOpacity: 0.25,shadowRadius: 2.25,
                            elevation: 5, }}>
                            <Text style={[styles.fontFamilyRegular, { fontSize:12, color:'red', marginBottom:5 }]}>Thursday</Text>
                            <View style={{ marginBottom:5 }}>
                                <Text style={[styles.fontFamilyRegular, { fontSize:15 }]}>06.00 AM - 19.00 PM</Text>
                            </View>
                        </View>  
                    </ScrollView>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    fontFamilyRegular:{
        fontFamily:'Asap-Regular'
    },
    fw400:{
        fontWeight:'400'
    }
});

export default withNavigation(HomeDriverInactive);